package com.atlassian.pocketknife.spi.emailreply.matcher;

import javax.annotation.Nonnull;
import java.util.List;

/**
 * Implementations of this interface can detect if a block of text
 * is the start of a quoted email
 */
public interface QuotedEmailMatcher {
    /**
     * @param textBlock contains up to 5 lines of text
     * @return true if the block is the <b>START</b> of
     * a quoted email, otherwise return false
     */
    boolean isQuotedEmail(@Nonnull List<String> textBlock);
}
