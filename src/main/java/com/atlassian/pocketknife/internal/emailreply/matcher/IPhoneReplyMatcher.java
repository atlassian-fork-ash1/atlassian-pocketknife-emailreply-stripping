package com.atlassian.pocketknife.internal.emailreply.matcher;

import com.atlassian.pocketknife.spi.emailreply.matcher.QuotedEmailMatcher;
import com.atlassian.pocketknife.internal.emailreply.matcher.basic.BeginForwardedMessageBlockMatcher;
import com.atlassian.pocketknife.internal.emailreply.matcher.basic.DefaultIphoneSignAndFwMessageBlockMatcher;
import com.atlassian.pocketknife.internal.emailreply.matcher.basic.DefaultIphoneSignatureBlockMatcher;
import com.atlassian.pocketknife.internal.emailreply.matcher.basic.DelegatingQuotedEmailMatcher;
import com.atlassian.pocketknife.internal.emailreply.matcher.basic.JapaneseMessageOfSmbBlockMatcher;
import com.atlassian.pocketknife.internal.emailreply.matcher.basic.QuotedBlockMatcher;
import com.google.common.collect.Lists;

import java.util.List;

/**
 * Detector for emails sent by iPhone Mail client
 */
public class IPhoneReplyMatcher extends DelegatingQuotedEmailMatcher
{
    @Override
    protected List<QuotedEmailMatcher> createDelegators()
    {
        return Lists.<QuotedEmailMatcher>newArrayList(
                new DefaultIphoneSignatureBlockMatcher(),
                new JapaneseMessageOfSmbBlockMatcher(),
                new DefaultIphoneSignAndFwMessageBlockMatcher(),
                new BeginForwardedMessageBlockMatcher(),
                new QuotedBlockMatcher());
    }
}
