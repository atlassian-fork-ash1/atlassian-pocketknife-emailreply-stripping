package com.atlassian.pocketknife.internal.emailreply.matcher.outlook.web;

import com.atlassian.pocketknife.internal.emailreply.matcher.ProxyQuotedEmailMatcher;
import com.atlassian.pocketknife.spi.emailreply.matcher.QuotedEmailMatcher;
import com.google.common.collect.ImmutableList;

import java.util.List;

public class Matcher extends ProxyQuotedEmailMatcher {
    private final List<QuotedEmailMatcher> delegators =
            ImmutableList.of(
                    new EnglishMatcher(),
                    new FrenchMatcher(),
                    new GermanMatcher(),
                    new JapaneseMatcher(),
                    new SpanishMatcher()
            );

    @Override
    protected List<QuotedEmailMatcher> getDelegators() {
        return delegators;
    }
}
