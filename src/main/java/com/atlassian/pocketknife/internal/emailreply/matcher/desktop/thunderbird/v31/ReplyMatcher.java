package com.atlassian.pocketknife.internal.emailreply.matcher.desktop.thunderbird.v31;

import com.atlassian.pocketknife.spi.emailreply.matcher.QuotedEmailMatcher;
import com.atlassian.pocketknife.internal.emailreply.matcher.util.RegexUtils;
import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;

import java.util.List;
import java.util.regex.Pattern;

/**
 * <p>Sample reply quoted text beginning marker: <br/>
 * On 6/11/15 10:52 AM, Chuong Nguyen wrote:  <br/>
 * > reply 11. <br/>
 * > reply 12. <br/>
 * </p>
 * <p>
 * <p> Matching logic: <br/>
 * 1. Text block begins with a attribution line. <br/>
 * 2. Attribution line is followed by all quoted lines. <br/>
 * </p>
 */
public class ReplyMatcher implements QuotedEmailMatcher
{

    private final String DATE_REGEX = "[0-9]+\\/[0-9]+\\/[0-9]+";
    private final String TIME_REGEX = "[0-9]+:[0-9]+[\\s]+(PM|AM)";
    private final String MILITARY_TIME_REGEX = "[0-9]+:[0-9]+";
    private final String FULL_NAME_REGEX = ".*";

    private final String ATTRIBUTION_LINE_ENGLISH_AND_JAPANESE_REGEX_SKELETON = "^On[\\s]+${0}[\\s]+${1},${2}wrote:$";
    private final String ATTRIBUTION_LINE_GERMAN_REGEX_SKELETON = "^Am[\\s]+${0}[\\s]+um[\\s]+${1}[\\s]+schrieb[\\s]+${2}:$";
    private final String ATTRIBUTION_LINE_SPANISH_REGEX_SKELETON = "^El[\\s]+${0}[\\s]+a[\\s]+las[\\s]+${1},${2}escribiÃ³:$";
    private final String ATTRIBUTION_LINE_FRENCH_REGEX_SKELETON = "^Le[\\s]+${0}[\\s]+${1},${2}a[\\s]+Ã©crit[\\s]*:$";

    private final String QUOTED_LINE_REGEX = "^>.*$";

    /**
     * package private for testing.
     */
    final Pattern ATTRIBUTION_LINE_ENGLISH_AND_JAPANESE_PATTERN = Pattern.compile(
            makeRegex(ATTRIBUTION_LINE_ENGLISH_AND_JAPANESE_REGEX_SKELETON), Pattern.CASE_INSENSITIVE);
    final Pattern ATTRIBUTION_LINE_GERMAN_PATTERN = Pattern.compile(
            makeRegex(ATTRIBUTION_LINE_GERMAN_REGEX_SKELETON), Pattern.CASE_INSENSITIVE);
    final Pattern ATTRIBUTION_LINE_SPANISH_PATTERN = Pattern.compile(
            makeRegex(ATTRIBUTION_LINE_SPANISH_REGEX_SKELETON), Pattern.CASE_INSENSITIVE);
    final Pattern ATTRIBUTION_LINE_FRENCH_PATTERN = Pattern.compile(
            makeRegex(ATTRIBUTION_LINE_FRENCH_REGEX_SKELETON), Pattern.CASE_INSENSITIVE);
    final Pattern MILITARY_TIME_ATTRIBUTION_LINE_ENGLISH_AND_JAPANESE_PATTERN = Pattern.compile(
            makeMilitaryTimeRegex(ATTRIBUTION_LINE_ENGLISH_AND_JAPANESE_REGEX_SKELETON), Pattern.CASE_INSENSITIVE);
    final Pattern MILITARY_TIME_ATTRIBUTION_LINE_GERMAN_PATTERN = Pattern.compile(
            makeMilitaryTimeRegex(ATTRIBUTION_LINE_GERMAN_REGEX_SKELETON), Pattern.CASE_INSENSITIVE);
    final Pattern MILITARY_TIME_ATTRIBUTION_LINE_SPANISH_PATTERN = Pattern.compile(
            makeMilitaryTimeRegex(ATTRIBUTION_LINE_SPANISH_REGEX_SKELETON), Pattern.CASE_INSENSITIVE);
    final Pattern MILITARY_TIME_ATTRIBUTION_LINE_FRENCH_PATTERN = Pattern.compile(
            makeMilitaryTimeRegex(ATTRIBUTION_LINE_FRENCH_REGEX_SKELETON), Pattern.CASE_INSENSITIVE);
    final Pattern QUOTED_LINE_PATTERN = Pattern.compile(QUOTED_LINE_REGEX, Pattern.CASE_INSENSITIVE);
    final List<Pattern> ATTRIBUTION_LINE_PATTERNS = Lists.newArrayList(
            ATTRIBUTION_LINE_ENGLISH_AND_JAPANESE_PATTERN,
            ATTRIBUTION_LINE_GERMAN_PATTERN,
            ATTRIBUTION_LINE_SPANISH_PATTERN,
            ATTRIBUTION_LINE_FRENCH_PATTERN,
            MILITARY_TIME_ATTRIBUTION_LINE_ENGLISH_AND_JAPANESE_PATTERN,
            MILITARY_TIME_ATTRIBUTION_LINE_FRENCH_PATTERN,
            MILITARY_TIME_ATTRIBUTION_LINE_GERMAN_PATTERN,
            MILITARY_TIME_ATTRIBUTION_LINE_SPANISH_PATTERN);

    @Override
    public boolean isQuotedEmail(List<String> textBlock)
    {
        if (textBlock == null || textBlock.size() == 0)
        {
            return false;
        }

        String attributionLine = StringUtils.EMPTY;
        boolean attributionLineMatched = false;
        int readLineIndex = -1;

        // match attributionLine
        for (int i = 0; i < textBlock.size(); i++)
        {
            attributionLine += textBlock.get(i);
            readLineIndex = i;
            attributionLineMatched |= isAttributionLine(attributionLine);
            if (attributionLineMatched)
            {
                break;
            }
        }
        if (!attributionLineMatched)
        {
            return false;
        }

        // match quoted lines
        boolean allQuotedLineMatched = true;
        if (readLineIndex < textBlock.size())
        {
            for (int i = readLineIndex + 1; i < textBlock.size(); i++)
            {
                allQuotedLineMatched &= isQuotedLine(textBlock.get(i));
                if (!allQuotedLineMatched)
                {
                    break;
                }
            }
        }
        return allQuotedLineMatched;
    }

    boolean isAttributionLine(String line)
    {
        for (Pattern pattern : ATTRIBUTION_LINE_PATTERNS)
        {
            if (RegexUtils.match(pattern, line))
            {
                return true;
            }
        }
        return false;
    }

    boolean isQuotedLine(String line)
    {
        return RegexUtils.match(QUOTED_LINE_PATTERN, line);
    }

    String makeRegex(String skeleton)
    {
        return RegexUtils.buildRegexFromSkeleton(skeleton, DATE_REGEX, TIME_REGEX, FULL_NAME_REGEX);
    }

    String makeMilitaryTimeRegex(String skeleton)
    {
        return RegexUtils.buildRegexFromSkeleton(skeleton, DATE_REGEX, MILITARY_TIME_REGEX, FULL_NAME_REGEX);
    }
}
