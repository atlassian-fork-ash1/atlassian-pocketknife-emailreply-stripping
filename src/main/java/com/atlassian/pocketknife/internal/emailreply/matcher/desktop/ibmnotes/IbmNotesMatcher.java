package com.atlassian.pocketknife.internal.emailreply.matcher.desktop.ibmnotes;

import com.atlassian.pocketknife.internal.emailreply.matcher.ProxyQuotedEmailMatcher;
import com.atlassian.pocketknife.spi.emailreply.matcher.QuotedEmailMatcher;
import com.atlassian.pocketknife.internal.emailreply.matcher.desktop.ibmnotes.v9.Matcher;
import com.google.common.collect.Lists;

import java.util.List;

public class IbmNotesMatcher extends ProxyQuotedEmailMatcher
{
    private final List<QuotedEmailMatcher> delegators = Lists.<QuotedEmailMatcher>newArrayList(new Matcher());

    @Override
    protected List<QuotedEmailMatcher> getDelegators()
    {
        return delegators;
    }
}
