package com.atlassian.pocketknife.internal.emailreply.util;

import org.apache.commons.lang.StringUtils;

import java.util.List;

import static org.apache.commons.lang.StringUtils.substring;

/**
 * Various static utility methods to access different lines from
 * a list of text lines
 */
public class TextBlockUtil {
    /**
     * Return the first 2 lines from the block by concatenating them into
     * a single String separated by newline char. Never returns null.
     * Returned result is always trimmed
     *
     * @param textBlock
     * @return
     */
    public static String getFirst2Lines(final List<String> textBlock) {
        if (textBlock == null || textBlock.isEmpty()) {
            return StringUtils.EMPTY;
        } else if (textBlock.size() == 1) {
            return textBlock.get(0).trim();
        } else {
            final StringBuilder sb = new StringBuilder(textBlock.get(0));
            if (StringUtils.isNotBlank(textBlock.get(1))) {
                sb.append("\n");
                sb.append(textBlock.get(1));
            }
            return sb.toString().trim();
        }
    }

    /**
     * The same as above but limits each line to specified length
     */
    public static String getFirstTwoLines(final List<String> textBlock, int limit) {
        if (textBlock == null || textBlock.isEmpty()) {
            return StringUtils.EMPTY;
        } else if (textBlock.size() == 1) {
            return textBlock.get(0).trim().substring(0, limit);
        } else {
            final StringBuilder sb = new StringBuilder(substring(textBlock.get(0),0, limit));
            String secondLine = substring(textBlock.get(1),0, limit);
            if (StringUtils.isNotBlank(secondLine)) {
                sb.append("\n");
                sb.append(secondLine);
            }
            return sb.toString().trim();
        }
    }

    /**
     * Get specific line from the block. If the line do not exist, return empty String
     * instead, never returns null
     *
     * @param textBlock
     * @param lineIndex
     * @return
     */
    public static String getLineOrEmptyString(final List<String> textBlock, int lineIndex) {
        if (textBlock == null || textBlock.isEmpty() || textBlock.size() <= lineIndex || lineIndex < 0) {
            return StringUtils.EMPTY;
        } else {
            return textBlock.get(lineIndex);
        }
    }

    /**
     * Return 2 lines from the block by concatenating them into
     * a single String separated by newline char. Never returns null.
     * Returned result is always trimmed
     *
     * @param textBlock
     * @return
     */
    public static String get2LinesOrEmptyString(final List<String> textBlock, final int startLineIndex) {
        if (textBlock == null || textBlock.isEmpty() || textBlock.size() <= startLineIndex || startLineIndex < 0) {
            return StringUtils.EMPTY;
        }

        final String one = textBlock.get(startLineIndex);

        final int nextLineIndex = startLineIndex + 1;

        if (textBlock.size() <= nextLineIndex) {
            return one.trim();
        }

        return new StringBuilder(one).append("\n").append(textBlock.get(nextLineIndex)).toString().trim();
    }
}
