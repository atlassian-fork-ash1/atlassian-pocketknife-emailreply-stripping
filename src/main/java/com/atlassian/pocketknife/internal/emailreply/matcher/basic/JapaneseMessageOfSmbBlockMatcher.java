package com.atlassian.pocketknife.internal.emailreply.matcher.basic;

import com.atlassian.pocketknife.internal.emailreply.matcher.RegexList;
import com.atlassian.pocketknife.internal.emailreply.util.LineMatchingUtil;
import com.atlassian.pocketknife.internal.emailreply.util.TextBlockUtil;

import javax.annotation.Nonnull;
import java.util.List;

/**
 * Detect quoted email by this pattern:
 * <pre>
 *     iPhoneから送信 (sent from my iPhone)
 *     date, someone のメッセージ (message of someone on date)
 *     > quoted text
 * </pre>
 * Only supports Japanese!!!!
 */
public class JapaneseMessageOfSmbBlockMatcher extends StatelessQuotedEmailMatcher
{
    @Override
    public boolean isQuotedEmail(@Nonnull final List<String> textBlock)
    {
        return RegexList.DEFAULT_IPHONE_SIGNATURE_PATTERN.matcher(TextBlockUtil.getLineOrEmptyString(textBlock, 0)).find()
                && LineMatchingUtil.isBlankOrStartWithQuoteLineIndicatorOrMatchPattern(TextBlockUtil.getLineOrEmptyString(textBlock, 1), RegexList.JAPANESE_MESSAGE_OF_SMB_PATTERN)
                && LineMatchingUtil.isBlankOrStartWithQuoteLineIndicatorOrMatchPattern(TextBlockUtil.getLineOrEmptyString(textBlock, 2), RegexList.JAPANESE_MESSAGE_OF_SMB_PATTERN)
                && LineMatchingUtil.isBlankOrStartWithQuoteLineIndicatorOrMatchPattern(TextBlockUtil.getLineOrEmptyString(textBlock, 3), RegexList.JAPANESE_MESSAGE_OF_SMB_PATTERN);
    }
}
