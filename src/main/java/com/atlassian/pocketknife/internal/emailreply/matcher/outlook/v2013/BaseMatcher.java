package com.atlassian.pocketknife.internal.emailreply.matcher.outlook.v2013;

import com.atlassian.pocketknife.internal.emailreply.matcher.util.RegexUtils;
import com.google.common.collect.Lists;

import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;

/**
 * <p>Sample quoted text beginning marker: <br/>
 * From:  chuong nguyen [chuongnn.atlassian.cus1@gmail.com] <br/>
 * Sent:  Monday, June 15, 2015 at 23:35 <br/>
 * To:  'chuong nguyen' <br/>
 * Cc:  'chuong nguyen' <br/>
 * Subject:  outlook 2011 hello english empire 1 <br/>
 * </p>
 * <p> Matching logic: <br/>
 * 1. Text block has at least 2 lines. <br/>
 * 2. Each line in first two lines matches one of pattern "From|Date|To|Cc|Subject:..." <br/>
 * </p>
 */
public abstract class BaseMatcher
        extends com.atlassian.pocketknife.internal.emailreply.matcher.outlook.BaseMatcher
{
    protected class MarkerDetector implements QuotedTextMarkerDetector
    {
        private final String NAME_WITH_EMAIL_REGEX = "[\\s]*[^\\[]+\\[[^@]+@[^\\]]+\\][\\s]*";
        private final String NAME_WITHOUT_EMAIL_REGEX = "[\\s]*\\'[^\\']+\\'";

        /**
         * package private for testing.
         */
        final Pattern FROM_PATTERN;
        final Pattern TO_PATTERN;
        final Pattern CC_PATTERN;
        final Pattern DATE_PATTERN;
        final Pattern DATE_AT_TOP_PATTERN;
        final Pattern SUBJECT_PATTERN;
        final Pattern SUBJECT_AT_TOP_PATTERN;
        final List<Pattern> FIRST_LINE_PATTERNS;
        final List<Pattern> SECOND_LINE_PATTERNS;

        public MarkerDetector(String FROM_REGEX_SKELETON,
                String TO_REGEX_SKELETON,
                String CC_REGEX_SKELETON,
                String DATE_REGEX,
                String DATE_AT_TOP_REGEX,
                String SUBJECT_REGEX,
                String SUBJECT_AT_TOP_REGEX)
        {
            FROM_PATTERN = Pattern.compile(
                    makeRegex(FROM_REGEX_SKELETON, NAME_WITH_EMAIL_REGEX), Pattern.CASE_INSENSITIVE);
            TO_PATTERN = Pattern.compile(
                    makeRegex(TO_REGEX_SKELETON, NAME_WITHOUT_EMAIL_REGEX), Pattern.CASE_INSENSITIVE);
            CC_PATTERN = Pattern.compile(
                    makeRegex(CC_REGEX_SKELETON, NAME_WITHOUT_EMAIL_REGEX), Pattern.CASE_INSENSITIVE);
            DATE_PATTERN = Pattern.compile(DATE_REGEX, Pattern.CASE_INSENSITIVE);
            DATE_AT_TOP_PATTERN = Pattern.compile(DATE_AT_TOP_REGEX, Pattern.CASE_INSENSITIVE);
            SUBJECT_PATTERN = Pattern.compile(SUBJECT_REGEX, Pattern.CASE_INSENSITIVE);
            SUBJECT_AT_TOP_PATTERN = Pattern.compile(SUBJECT_AT_TOP_REGEX, Pattern.CASE_INSENSITIVE);

            FIRST_LINE_PATTERNS = Collections.unmodifiableList(
                    Lists.newArrayList(
                            FROM_PATTERN, TO_PATTERN, CC_PATTERN, DATE_AT_TOP_PATTERN, SUBJECT_AT_TOP_PATTERN));
            SECOND_LINE_PATTERNS = Collections.unmodifiableList(
                    Lists.newArrayList(
                            FROM_PATTERN, TO_PATTERN, CC_PATTERN, DATE_PATTERN, SUBJECT_PATTERN));
        }

        @Override
        public List<Pattern> getFirstLinePatterns()
        {
            return FIRST_LINE_PATTERNS;
        }

        @Override
        public List<Pattern> getSecondLinePatterns()
        {
            return SECOND_LINE_PATTERNS;
        }

        private String makeRegex(String skeleton, String nameRegex)
        {
            return RegexUtils.buildRegexFromSkeleton(
                    skeleton, nameRegex);
        }
    }

    protected abstract MarkerDetector getMarkerDetector();
}