package com.atlassian.pocketknife.internal.emailreply.matcher.outlook;

import com.atlassian.pocketknife.internal.emailreply.matcher.ProxyQuotedEmailMatcher;
import com.atlassian.pocketknife.spi.emailreply.matcher.QuotedEmailMatcher;
import com.google.common.collect.Lists;

import java.util.List;

public class OutlookDesktopMatcher extends ProxyQuotedEmailMatcher
{
    private final List<QuotedEmailMatcher> delegators = Lists.<QuotedEmailMatcher>newArrayList(
            new com.atlassian.pocketknife.internal.emailreply.matcher.outlook.v2011.Matcher(),
            new com.atlassian.pocketknife.internal.emailreply.matcher.outlook.v2013.Matcher()
    );

    @Override
    protected List<QuotedEmailMatcher> getDelegators()
    {
        return delegators;
    }
}
