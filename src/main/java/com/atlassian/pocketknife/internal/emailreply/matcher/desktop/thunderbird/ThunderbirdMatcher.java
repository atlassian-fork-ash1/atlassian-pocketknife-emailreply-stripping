package com.atlassian.pocketknife.internal.emailreply.matcher.desktop.thunderbird;

import com.atlassian.pocketknife.spi.emailreply.matcher.QuotedEmailMatcher;
import com.atlassian.pocketknife.internal.emailreply.matcher.ProxyQuotedEmailMatcher;
import com.atlassian.pocketknife.internal.emailreply.matcher.desktop.thunderbird.v31.ForwardMatcher;
import com.atlassian.pocketknife.internal.emailreply.matcher.desktop.thunderbird.v31.ReplyMatcher;
import com.google.common.collect.Lists;

import java.util.List;

public class ThunderbirdMatcher extends ProxyQuotedEmailMatcher
{
    private final List<QuotedEmailMatcher> delegators = Lists.newArrayList(new ReplyMatcher(), new ForwardMatcher());

    @Override
    protected List<QuotedEmailMatcher> getDelegators()
    {
        return delegators;
    }
}
