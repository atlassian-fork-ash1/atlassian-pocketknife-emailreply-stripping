package com.atlassian.pocketknife.internal.emailreply.matcher.mobile.ipad;

import com.atlassian.pocketknife.spi.emailreply.matcher.QuotedEmailMatcher;
import com.atlassian.pocketknife.internal.emailreply.matcher.basic.DelegatingQuotedEmailMatcher;
import com.atlassian.pocketknife.internal.emailreply.matcher.basic.QuotedBlockMatcher;
import com.google.common.collect.Lists;

import java.util.List;

/**
 * Aggregate class for all patterns used by iPad Mail
 */
public class IPadReplyMatcher extends DelegatingQuotedEmailMatcher
{
    @Override
    protected List<QuotedEmailMatcher> createDelegators()
    {
        return Lists.<QuotedEmailMatcher>newArrayList(
                new IpadReplySignAndQuotedTextBlockMatcher(),
                new IpadJapReplyWithSignBlockMatcher(),
                new IpadJapReplyWithoutSignBlockMatcher(),
                new QuotedBlockMatcher(),
                new IpadForwardWithSignBlockMatcher(),
                new IpadForwardWithoutSignBlockMatcher()
        );
    }
}
