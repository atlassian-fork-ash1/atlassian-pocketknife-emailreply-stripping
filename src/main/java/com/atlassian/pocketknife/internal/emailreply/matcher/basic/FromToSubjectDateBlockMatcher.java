package com.atlassian.pocketknife.internal.emailreply.matcher.basic;

import com.atlassian.pocketknife.internal.emailreply.matcher.RegexList;
import com.atlassian.pocketknife.internal.emailreply.util.LineMatchingUtil;
import com.atlassian.pocketknife.internal.emailreply.util.TextBlockUtil;

import javax.annotation.Nonnull;
import java.util.List;

/**
 * Detect quoted email using the following pattern:
 * <pre>
 *     From: Joe Doe
 *     To: John
 *     Subject: hello
 *     Date: Wed, Jun 10, 2015
 *     CC: Jorge
 * </pre>
 * If the text block contains 4 out of 5 lines above in any order, then it is the
 * start of a quoted email.
 * Currently supports multiple languages
 */
public class FromToSubjectDateBlockMatcher extends StatelessQuotedEmailMatcher
{
    @Override
    public boolean isQuotedEmail(@Nonnull List<String> textBlock)
    {
        if (textBlock.size() < 4)
        {
            return false;
        }

        if (!RegexList.FROM_TO_SUBJECT_DATE_CC_PREFIX_PATTERN.matcher(TextBlockUtil.getLineOrEmptyString(textBlock, 0)).find()) {
            return false;
        }

        int matchCount = 0;
        for (final String line : textBlock)
        {
            if (RegexList.FROM_TO_SUBJECT_DATE_CC_PREFIX_PATTERN.matcher(line).find())
            {
                matchCount++;
            }
        }
        return matchCount >= 4;
    }
}
