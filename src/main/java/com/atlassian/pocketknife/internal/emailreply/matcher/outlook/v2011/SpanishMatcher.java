package com.atlassian.pocketknife.internal.emailreply.matcher.outlook.v2011;

public class SpanishMatcher extends BaseMatcher
{
    private final MarkerDetector markerDetector;

    public SpanishMatcher()
    {
        String FROM_REGEX_SKELETON = "^De:${0}$";
        String REPLY_TO_REGEX_SKELETON = "^Responder[\\s]+a:${0}$";
        String TO_REGEX_SKELETON = "^Para:${0}$";
        String CC_REGEX_SKELETON = "^CC:${0}$";
        String DATE_REGEX = "^Fecha:.*$";
        String DATE_AT_TOP_REGEX = "^Fecha:.*(De|Responder a|Para|Cc|Asunto)[\\s]*:.*$";
        String SUBJECT_REGEX = "^Asunto:.*$";
        String SUBJECT_AT_TOP_REGEX = "^Asunto:.*(De|Responder a|Para|Cc|Fecha)[\\s]*:.*$";

        markerDetector = new MarkerDetector(
                FROM_REGEX_SKELETON,
                REPLY_TO_REGEX_SKELETON,
                TO_REGEX_SKELETON,
                CC_REGEX_SKELETON,
                DATE_REGEX,
                DATE_AT_TOP_REGEX,
                SUBJECT_REGEX,
                SUBJECT_AT_TOP_REGEX);
    }

    @Override
    protected MarkerDetector getMarkerDetector()
    {
        return markerDetector;
    }
}
