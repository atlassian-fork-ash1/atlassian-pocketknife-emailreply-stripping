package com.atlassian.pocketknife.internal.emailreply.matcher.basic;

import com.atlassian.pocketknife.internal.emailreply.matcher.RegexList;
import com.atlassian.pocketknife.internal.emailreply.util.LineMatchingUtil;
import com.atlassian.pocketknife.internal.emailreply.util.TextBlockUtil;

import javax.annotation.Nonnull;
import java.util.List;

/**
 * Detect quoted email using following pattern:
 * <pre>
 *     On Wed, Jun 10, 2015 at 9:59 AM, wrote Joe Doe <jd@gmail.com>:
 *     > quoted text
 * </pre>
 * Supported language: German
 */
public class OnDateWroteSmbBlockMatcher extends StatelessQuotedEmailMatcher
{
    @Override
    public boolean isQuotedEmail(@Nonnull List<String> textBlock)
    {
        if (textBlock.isEmpty())
        {
            return false;
        }
        final String first2Lines = TextBlockUtil.getFirst2Lines(textBlock);
        final String thirdLine = TextBlockUtil.getLineOrEmptyString(textBlock, 2);
        final String fourthLIne = TextBlockUtil.getLineOrEmptyString(textBlock, 3);

        return RegexList.ON_DATE_WROTE_SMB_PATTERN.matcher(first2Lines).find()
                && LineMatchingUtil.isBlankOrStartWithQuoteLineIndicator(thirdLine)
                && LineMatchingUtil.isBlankOrStartWithQuoteLineIndicator(fourthLIne);
    }
}
