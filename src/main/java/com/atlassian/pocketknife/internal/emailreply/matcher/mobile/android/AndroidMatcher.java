package com.atlassian.pocketknife.internal.emailreply.matcher.mobile.android;

import com.atlassian.pocketknife.internal.emailreply.matcher.ProxyQuotedEmailMatcher;
import com.atlassian.pocketknife.internal.emailreply.matcher.mobile.android.signature.DefaultSignatureMatcher;
import com.atlassian.pocketknife.internal.emailreply.matcher.mobile.android.v5.ForwardMatcher;
import com.atlassian.pocketknife.internal.emailreply.matcher.mobile.android.v5.ReplyMatcher;
import com.atlassian.pocketknife.spi.emailreply.matcher.QuotedEmailMatcher;
import com.google.common.collect.ImmutableList;

import java.util.List;

public class AndroidMatcher extends ProxyQuotedEmailMatcher {
    private final List<QuotedEmailMatcher> delegators = ImmutableList.of(
            new ReplyMatcher(), new ForwardMatcher(), new DefaultSignatureMatcher());

    @Override
    protected List<QuotedEmailMatcher> getDelegators() {
        return delegators;
    }
}
