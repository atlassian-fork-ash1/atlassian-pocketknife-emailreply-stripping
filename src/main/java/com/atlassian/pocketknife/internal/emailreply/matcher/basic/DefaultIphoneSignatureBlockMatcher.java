package com.atlassian.pocketknife.internal.emailreply.matcher.basic;

import com.atlassian.pocketknife.internal.emailreply.matcher.RegexList;
import com.atlassian.pocketknife.internal.emailreply.util.LineMatchingUtil;
import com.atlassian.pocketknife.internal.emailreply.util.TextBlockUtil;
import org.apache.commons.lang.StringUtils;

import javax.annotation.Nonnull;
import java.util.List;

/**
 * Detect quoted email by these patterns:
 * <br/>
 * Pattern 1
 * <pre>
 *     Sent from my iphone
 *     (blank line)
 *     > On ... someone wrote:
 *     > quoted text
 * </pre>
 *
 * Pattern 2
 * <pre>
 *     Sent from my iphone
 *     > On ... someone wrote:
 *     > quoted text
 * </pre>
 * Supports English, French, German, Spanish
 */
public class DefaultIphoneSignatureBlockMatcher extends StatelessQuotedEmailMatcher
{
    @Override
    public boolean isQuotedEmail(@Nonnull final List<String> textBlock)
    {
        if (textBlock.size() == 4)
        {
            return isPattern1(textBlock);
        }
        else if (textBlock.size() == 3)
        {
            return isPattern2(textBlock);
        }
        else
        {
            return false;
        }
    }

    /**
     * Check for
     * <pre>
     *     Sent from my iphone
     *     (blank line)
     *     > On ... someone wrote:
     *     > quoted text
     * </pre>
     * @param textBlock
     * @return
     */
    private boolean isPattern1(final List<String> textBlock)
    {
        final String line1 = TextBlockUtil.getLineOrEmptyString(textBlock, 0);
        final String line2 = TextBlockUtil.getLineOrEmptyString(textBlock, 1);
        final String line3 = TextBlockUtil.getLineOrEmptyString(textBlock, 2);
        final String line4 = TextBlockUtil.getLineOrEmptyString(textBlock, 3);

        return RegexList.DEFAULT_IPHONE_SIGNATURE_PATTERN.matcher(line1).find()
                && StringUtils.isBlank(line2)
                && isQuotedAttributionLine(line3)
                && LineMatchingUtil.isStartWithQuoteLineIndicator(line4);
    }

    /**
     * Check for
     * <pre>
     *     Sent from my iphone
     *     > On ... someone wrote:
     *     > quoted text
     * </pre>
     * @param textBlock
     * @return
     */
    private boolean isPattern2(final List<String> textBlock)
    {
        final String line1 = TextBlockUtil.getLineOrEmptyString(textBlock, 0);
        final String line2 = TextBlockUtil.getLineOrEmptyString(textBlock, 1);
        final String line3 = TextBlockUtil.getLineOrEmptyString(textBlock, 2);

        return RegexList.DEFAULT_IPHONE_SIGNATURE_PATTERN.matcher(line1).find()
                && isQuotedAttributionLine(line2)
                && LineMatchingUtil.isStartWithQuoteLineIndicator(line3);
    }

    private boolean isQuotedAttributionLine(final String line)
    {
        return RegexList.QUOTED_ON_DATE_SMB_WROTE_PATTERN.matcher(line).find()
                || RegexList.QUOTED_ON_DATE_WROTE_SMB_PATTERN.matcher(line).find();
    }
}
