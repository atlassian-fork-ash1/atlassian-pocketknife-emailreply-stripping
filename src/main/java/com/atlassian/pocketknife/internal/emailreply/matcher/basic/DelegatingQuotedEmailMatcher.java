package com.atlassian.pocketknife.internal.emailreply.matcher.basic;

import com.atlassian.pocketknife.spi.emailreply.matcher.QuotedEmailMatcher;

import javax.annotation.Nonnull;
import java.util.List;

public abstract class DelegatingQuotedEmailMatcher extends StatelessQuotedEmailMatcher
{
    @Override
    public boolean isQuotedEmail(@Nonnull final List<String> textBlock)
    {
        for (final QuotedEmailMatcher matcher : createDelegators())
        {
            if (matcher.isQuotedEmail(textBlock))
            {
                return true;
            }
        }
        return false;
    }

    protected abstract List<QuotedEmailMatcher> createDelegators();
}
