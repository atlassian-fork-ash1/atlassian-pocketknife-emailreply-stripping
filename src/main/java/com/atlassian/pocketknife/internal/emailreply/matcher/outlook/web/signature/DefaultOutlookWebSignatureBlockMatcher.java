package com.atlassian.pocketknife.internal.emailreply.matcher.outlook.web.signature;

import com.atlassian.pocketknife.internal.emailreply.matcher.RegexList;
import com.atlassian.pocketknife.internal.emailreply.matcher.basic.StatelessQuotedEmailMatcher;
import com.atlassian.pocketknife.internal.emailreply.util.TextBlockUtil;
import org.apache.commons.lang.StringUtils;

import javax.annotation.Nonnull;
import java.util.List;

import static com.atlassian.pocketknife.internal.emailreply.matcher.outlook.web.BaseMatcher.OUTLOOK_WEB_HORIZONTAL_RULE;
import static org.apache.commons.lang.StringUtils.EMPTY;
import static org.apache.commons.lang.StringUtils.isBlank;

/**
 * Detect quoted email using this pattern:
 * <pre>
 *     Sent from Outlook<http://aka.ms/weboutlook>
 *     ________________________________
 * </pre>
 * Supports English, French, German, Japanese, Spanish
 */
public class DefaultOutlookWebSignatureBlockMatcher extends StatelessQuotedEmailMatcher {

    @Override
    public boolean isQuotedEmail(@Nonnull final List<String> textBlock) {
        final boolean match = RegexList.DEFAULT_OUTLOOK_WEB_SIGNATURE_PATTERN.matcher(TextBlockUtil.getLineOrEmptyString(textBlock, 0)).find();

        if (!match) {
            return false;
        }

        String nextNonBlankLine = EMPTY;
        for (int index = 1; index < textBlock.size(); index++) {
            final String lineOrEmptyString = TextBlockUtil.getLineOrEmptyString(textBlock, index);

            if (isBlank(lineOrEmptyString)) {
                continue;
            }

            nextNonBlankLine = lineOrEmptyString;
            break;
        }

        return StringUtils.equals(OUTLOOK_WEB_HORIZONTAL_RULE, nextNonBlankLine);
    }
}
