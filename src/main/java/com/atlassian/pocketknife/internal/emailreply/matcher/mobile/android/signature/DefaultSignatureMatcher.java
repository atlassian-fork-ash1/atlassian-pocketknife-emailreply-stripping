package com.atlassian.pocketknife.internal.emailreply.matcher.mobile.android.signature;

import com.atlassian.pocketknife.internal.emailreply.matcher.RegexList;
import com.atlassian.pocketknife.internal.emailreply.matcher.util.RegexUtils;
import com.atlassian.pocketknife.spi.emailreply.matcher.QuotedEmailMatcher;
import com.google.common.collect.ImmutableList;

import java.util.List;
import java.util.regex.Pattern;

import static com.atlassian.pocketknife.internal.emailreply.util.LineMatchingUtil.isBlankOrStartWithQuoteLineIndicator;
import static com.atlassian.pocketknife.internal.emailreply.util.LineMatchingUtil.stripStartOfQuoteLineIndicators;
import static com.atlassian.pocketknife.internal.emailreply.util.TextBlockUtil.getLineOrEmptyString;
import static org.apache.commons.lang.StringUtils.EMPTY;

/**
 * will match, for example
 * <pre>
 *      Sent from my Samsung Galaxy smartphone.-------- Original message --------
 * </pre>
 * <pre>
 *      Sent from my HTC
 *
 *      ----- Reply message -----
 * </pre>
 * <pre>
 *      Sent from my HTC
 *
 *      ----- Forwarded message -----
 * </pre>
 * <pre>
 *      Sent from my Samsung Galaxy smartphone.-------- Original message --------
 * </pre>
 * <p>
 * The actual device is not defined in the regex, so will match any text (ie. Samsung Galaxy smartphone, HTC, Nexus 5, etc).
 */
public class DefaultSignatureMatcher implements QuotedEmailMatcher {

    final List<Pattern> DEFAULT_SIGNATURE_PATTERNS =
            ImmutableList.of(
                    RegexList.DEFAULT_ANDROID_SIGNATURE_PATTERN
            );

    final List<Pattern> ATTRIBUTION_PATTERNS =
            ImmutableList.of(
                    RegexList.FORWARDED_MESSAGE_PATTERN,
                    RegexList.INLINED_FORWARDED_MESSAGE_PATTERN,
                    RegexList.REPLY_MESSAGE_PATTERN,
                    RegexList.INLINED_REPLY_MESSAGE_PATTERN,
                    RegexList.ORIGINAL_MESSAGE_PATTERN,
                    RegexList.INLINED_ORIGINAL_MESSAGE_PATTERN
            );

    @Override
    public boolean isQuotedEmail(List<String> textBlock) {
        if (textBlock == null || textBlock.isEmpty()) {
            return false;
        }

        String firstLine = getLineOrEmptyString(textBlock, 0);

        if (isBlankOrStartWithQuoteLineIndicator(firstLine)) {
            firstLine = stripStartOfQuoteLineIndicators(firstLine);
        }

        if (matchAnyDefaultSignature(firstLine)) {
            // could now be a blank line, no blank line or on same line
            if (matchAnyAttribution(firstLine)) {
                return true;
            }

            int index = 1;

            // see if end of message
            if (index >= textBlock.size()) {
                return true;
            }

            // check if there is an attribution following
            String nextLine = EMPTY;
            while (isBlankOrStartWithQuoteLineIndicator(nextLine) && index < textBlock.size()) {
                nextLine = getLineOrEmptyString(textBlock, index++);
                if (matchAnyAttribution(nextLine)) {
                    return true;
                }
            }
        }

        return false;
    }

    private boolean matchAnyAttribution(final String line) {
        return ATTRIBUTION_PATTERNS.stream().anyMatch(attribution -> RegexUtils.match(attribution, line));
    }

    private boolean matchAnyDefaultSignature(final String line) {
        return DEFAULT_SIGNATURE_PATTERNS.stream().anyMatch(signature -> RegexUtils.match(signature, line));
    }
}
