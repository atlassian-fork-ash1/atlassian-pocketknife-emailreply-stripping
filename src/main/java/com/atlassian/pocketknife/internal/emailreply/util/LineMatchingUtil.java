package com.atlassian.pocketknife.internal.emailreply.util;

import com.google.common.collect.ImmutableList;
import org.apache.commons.lang.StringUtils;

import java.util.List;
import java.util.regex.Pattern;

import static org.apache.commons.lang.StringUtils.join;

public class LineMatchingUtil {
    private static final String BRACKET_QUOTED_LINE_INDICATOR = ">";
    private static final String PIPE_QUOTED_LINE_INDICATOR = "|";

    private static final List<String> QUOTE_LINE_INDICATORS = ImmutableList.of(BRACKET_QUOTED_LINE_INDICATOR, PIPE_QUOTED_LINE_INDICATOR);
    private static final String QUOTE_LINE_INDICATOR_STRING = join(QUOTE_LINE_INDICATORS, "");

    public static boolean isBlankOrStartWithQuoteLineIndicator(final String line) {
        return StringUtils.isBlank(line) || isStartWithQuoteLineIndicator(line);
    }

    public static boolean isStartWithQuoteLineIndicator(final String line) {
        for (String indicator : QUOTE_LINE_INDICATORS) {
            if (StringUtils.startsWith(line, indicator)) {
                return true;
            }
        }
        return false;
    }

    public static boolean isBlankOrMatchPattern(final String line, final Pattern patternToMatch) {
        return StringUtils.isBlank(line) || patternToMatch.matcher(line).find();
    }

    public static boolean isBlankOrStartWithQuoteLineIndicatorOrMatchPattern(final String line, final Pattern patternToMatch) {
        return LineMatchingUtil.isBlankOrStartWithQuoteLineIndicator(line) || patternToMatch.matcher(line).find();
    }

    public static String stripEndOfQuoteLineIndicators(final String line) {
        return StringUtils.stripEnd(line, QUOTE_LINE_INDICATOR_STRING);
    }

    public static String stripStartOfQuoteLineIndicators(final String line) {
        return StringUtils.stripStart(line, QUOTE_LINE_INDICATOR_STRING);
    }

}
