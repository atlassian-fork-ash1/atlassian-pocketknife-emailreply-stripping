package com.atlassian.pocketknife.internal.emailreply.matcher.basic;

import com.atlassian.pocketknife.internal.emailreply.matcher.RegexList;

import javax.annotation.Nonnull;
import java.util.List;

/**
 * Detect quoted email using this pattern
 * <pre>
 *     ---- Forwarded Message ----
 * </pre>
 * Supported languages: English, French, German, Spanish, Japanese
 */
public class ForwardedMessageBlockMatcher extends StatelessQuotedEmailMatcher
{
    @Override
    public boolean isQuotedEmail(@Nonnull List<String> textBlock)
    {
        if (textBlock.isEmpty())
        {
            return false;
        }

        return RegexList.FORWARDED_MESSAGE_PATTERN.matcher(textBlock.get(0)).find();
    }
}
