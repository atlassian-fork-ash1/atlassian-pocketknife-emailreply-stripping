package com.atlassian.pocketknife.internal.emailreply.matcher;

import com.atlassian.pocketknife.spi.emailreply.matcher.QuotedEmailMatcher;

import java.util.List;

/**
 * Matcher that delegates matching for other matchers.
 */
public abstract class ProxyQuotedEmailMatcher implements QuotedEmailMatcher
{

    protected ProxyQuotedEmailMatcher()
    {
    }

    @Override
    public boolean isQuotedEmail(List<String> textBlock)
    {
        List<QuotedEmailMatcher> delegators = getDelegators();
        if (delegators == null)
        {
            return false;
        }

        for (QuotedEmailMatcher delegator : delegators)
        {
            if (delegator.isQuotedEmail(textBlock))
            {
                return true;
            }
        }
        return false;
    }

    protected abstract List<QuotedEmailMatcher> getDelegators();
}
