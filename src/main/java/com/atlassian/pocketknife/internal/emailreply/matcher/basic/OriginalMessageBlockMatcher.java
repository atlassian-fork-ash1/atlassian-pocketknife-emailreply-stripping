package com.atlassian.pocketknife.internal.emailreply.matcher.basic;

import com.atlassian.pocketknife.internal.emailreply.matcher.RegexList;
import com.atlassian.pocketknife.internal.emailreply.util.TextBlockUtil;

import javax.annotation.Nonnull;
import java.util.List;

/**
 * Detect quoted email using this pattern:
 * <pre>
 *     --- Original messsage ---
 * </pre>
 * Supported language: English, French, German, Japanese, Spanish
 */
public class OriginalMessageBlockMatcher extends StatelessQuotedEmailMatcher
{
    @Override
    public boolean isQuotedEmail(@Nonnull List<String> textBlock)
    {
        return RegexList.ORIGINAL_MESSAGE_PATTERN.matcher(TextBlockUtil.getLineOrEmptyString(textBlock, 0)).find();
    }
}
