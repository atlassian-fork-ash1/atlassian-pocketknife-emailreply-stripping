package com.atlassian.pocketknife.internal.emailreply.matcher.outlook;

import com.atlassian.pocketknife.internal.emailreply.matcher.ProxyQuotedEmailMatcher;
import com.atlassian.pocketknife.internal.emailreply.matcher.outlook.android.OnDateSmbWroteOutlookAndroidBlockMatcher;
import com.atlassian.pocketknife.internal.emailreply.matcher.outlook.android.OutlookDefaultSignatureAndroidMatcher;
import com.atlassian.pocketknife.spi.emailreply.matcher.QuotedEmailMatcher;
import com.google.common.collect.ImmutableList;

import java.util.List;

public class OutlookAndroidMatcher extends ProxyQuotedEmailMatcher {

    private final List<QuotedEmailMatcher> delegators =
            ImmutableList.of(
                    new OutlookDefaultSignatureAndroidMatcher(),
                    new OnDateSmbWroteOutlookAndroidBlockMatcher()
            );

    @Override
    protected List<QuotedEmailMatcher> getDelegators() {
        return delegators;
    }
}
