package com.atlassian.pocketknife.internal.emailreply.matcher.basic;

import com.atlassian.pocketknife.internal.emailreply.matcher.RegexList;
import com.atlassian.pocketknife.internal.emailreply.util.LineMatchingUtil;
import com.atlassian.pocketknife.internal.emailreply.util.TextBlockUtil;

import javax.annotation.Nonnull;
import java.util.List;

/**
 * Detect quoted email using this patterns:
 * <pre>
 *     (---) Begin forwarded message (---)
 *     > quoted line
 * </pre>
 * Supports English, French, German, Japanese, Spanish
 */
public class BeginForwardedMessageBlockMatcher extends StatelessQuotedEmailMatcher
{
    @Override
    public boolean isQuotedEmail(@Nonnull final List<String> textBlock)
    {
        return RegexList.BEGIN_FORWARD_MESSAGE_PATTERN.matcher(TextBlockUtil.getLineOrEmptyString(textBlock, 0)).find()
                && LineMatchingUtil.isBlankOrStartWithQuoteLineIndicator(TextBlockUtil.getLineOrEmptyString(textBlock, 1))
                && LineMatchingUtil.isBlankOrStartWithQuoteLineIndicator(TextBlockUtil.getLineOrEmptyString(textBlock, 2))
                && LineMatchingUtil.isBlankOrStartWithQuoteLineIndicator(TextBlockUtil.getLineOrEmptyString(textBlock, 3));
    }
}
