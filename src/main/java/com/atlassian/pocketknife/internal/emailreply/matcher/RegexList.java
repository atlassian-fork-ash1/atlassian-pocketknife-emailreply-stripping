package com.atlassian.pocketknife.internal.emailreply.matcher;

import java.util.StringJoiner;
import java.util.regex.Pattern;

public final class RegexList {
    public static final Pattern ORIGINAL_MESSAGE_PATTERN = Pattern.compile("^[\\s]*[-]+[ ]*(Original message|message d'origine|Ursprüngliche Nachricht|オリジナルメッセージ|Mensaje original)[ ]*[-]+$", Pattern.CASE_INSENSITIVE);

    public static final Pattern INLINED_ORIGINAL_MESSAGE_PATTERN = Pattern.compile("^.*[-]+[ ]*(Original message|message d'origine|Ursprüngliche Nachricht|オリジナルメッセージ|Mensaje original)[ ]*[-]+.*$", Pattern.CASE_INSENSITIVE);

    public static final Pattern BEGIN_FORWARD_MESSAGE_PATTERN = Pattern.compile("^[-]*[ ]*(Begin forwarded message|Début du message transféré|Anfang der weitergeleiteten E‑Mail|Inicio del mensaje reenviado)[ ]*(:)*[ ]*[-]*$", Pattern.CASE_INSENSITIVE);

    public static final Pattern REPLY_MESSAGE_PATTERN = Pattern.compile("^[\\s]*[-]+[ ]*(Reply message|Message de réponse|Antwort senden|メッセージを返信|Mensaje de respuesta)[ ]*[-]+:*$", Pattern.CASE_INSENSITIVE);

    public static final Pattern INLINED_REPLY_MESSAGE_PATTERN = Pattern.compile("^.*[-]+[ ]*(Reply message|Message de réponse|Antwort senden|メッセージを返信|Mensaje de respuesta)[ ]*[-]+.*$", Pattern.CASE_INSENSITIVE);

    public static final Pattern FORWARDED_MESSAGE_PATTERN = Pattern.compile("^[\\s]*[-]+[ ]*(Forwarded message|Message transféré|Weitergeleitete Nachricht|転送メッセージ|Mensaje reenviado)[ ]*[-]+:*$", Pattern.CASE_INSENSITIVE);

    public static final Pattern INLINED_FORWARDED_MESSAGE_PATTERN = Pattern.compile("^.*[-]+[ ]*(Forwarded message|Message transféré|Weitergeleitete Nachricht|転送メッセージ|Mensaje reenviado)[ ]*[-]+.*$", Pattern.CASE_INSENSITIVE);

    public static final Pattern DEFAULT_IPHONE_SIGNATURE_PATTERN = Pattern.compile("^[ ]*(Sent from my iPhone|Envoyé de mon iPhone|Von meinem iPhone gesendet|iPhoneから送信|Enviado desde mi iPhone)[ ]*$", Pattern.CASE_INSENSITIVE);

    public static final Pattern DEFAULT_ANDROID_SIGNATURE_PATTERN = Pattern.compile("^[ ]*(Sent from my\\s+\\S+|Envoyé de mon\\s+\\S+|Von meinem [\\S\\s]+ gesendet|[\\S\\s]+から送信|Enviado desde mi\\s+\\S+).*$", Pattern.CASE_INSENSITIVE);

    public static final Pattern JAPANESE_MESSAGE_OF_SMB_PATTERN = Pattern.compile("[ ]*(のメッセージ)[ ]*:$", Pattern.CASE_INSENSITIVE);

    private static final String fromMatchers = "From|Od|Fra|Von|De|Saatja|Lähettäjä|De|Feladó|Da|差出人|보낸 사람|Fra|Od|От|Från|发件人";
    private static final String toMatchers = "To|Komu|Til|An|Para|Saaja|Vastaanottaja|À|Címzett|A|宛先|받는 사람|Til|Do|Para|Para|Кому|Till|收件人";
    private static final String ccMatchers = "CC|Kopie|c\\.c\\.|Koopia|Kopio|Másolatot kap|참조|Kopi|DW|Копия|Kópia|Kopia|抄送";
    private static final String subjectMatchers = "Subject|Předmět|Emne|Betreff|Asunto|Teema|Aihe|Objet|Tárgy|Oggetto|件名|제목|Emne|Temat|Assunto|Тема|Predmet|Ämne|主题";
    private static final String dateMatchers = "Date|Datum|Dato|Fecha|Kuupäev|Päivämäärä|Dátum|Data|日付|날짜|Dato|Дата|Dátum|日期";

    public static final Pattern FROM_TO_SUBJECT_DATE_CC_PREFIX_PATTERN = Pattern.compile("^>?[ ]*(" + new StringJoiner("|")
            .add(fromMatchers)
            .add(toMatchers)
            .add(ccMatchers)
            .add(subjectMatchers)
            .add(dateMatchers)
            .toString() + "):", Pattern.CASE_INSENSITIVE);

    public static final Pattern ON_DATE_SMB_WROTE_PATTERN = Pattern.compile("^-*>?[ ]?(on|le|el)[ ].*[,](.*\\n){0,2}.*(wrote|sent|a écrit|escribió)[ ]*:?-*$", Pattern.CASE_INSENSITIVE);

    public static final Pattern ON_DATE_WROTE_SMB_PATTERN = Pattern.compile("^-*>?[ ]?(am)[ ].*(.*\\n){0,2}.*(schrieb)[ ].*:$", Pattern.CASE_INSENSITIVE);

    public static final Pattern YAHOO_ON_DATE_SMB_WROTE_PATTERN = Pattern.compile("^-*[ ]{4,}(On|Le|El)[ ].*([,])(.*\\n){0,2}.*(wrote|a écrit|escribió)[ ]*:?-*$", Pattern.CASE_INSENSITIVE);

    public static final Pattern YAHOO_SMB_WROTE_ON_DATE_PATTERN = Pattern.compile("^-*[ ]{4,}.*(.*\\n){0,2}.*(schrieb am).*([,]).*:?-*$", Pattern.CASE_INSENSITIVE);

    public static final Pattern QUOTED_ON_DATE_SMB_WROTE_PATTERN = Pattern.compile("^(>|)-*[ ]?(on|le|el)[ ].*[,](.*\\n){0,2}.*(wrote|sent|a écrit|escribió)[ ]*:?-*$", Pattern.CASE_INSENSITIVE);

    public static final Pattern QUOTED_ON_DATE_WROTE_SMB_PATTERN = Pattern.compile("^(>|)-*[ ]?(am)[ ].*(.*\\n){0,2}.*(schrieb)[ ].*:$", Pattern.CASE_INSENSITIVE);

    public static final Pattern DEFAULT_IPAD_SIGNATURE_PATTERN = Pattern.compile("^[ ]*(Sent from my iPad|Envoyé de mon iPad|Von meinem iPad gesendet|Enviado desde mi iPad|iPadから送信)[ ]*$", Pattern.CASE_INSENSITIVE);

    public static final Pattern DEFAULT_OUTLOOK_WEB_SIGNATURE_PATTERN = Pattern.compile("^[ ]*(Sent from Outlook|Envoyé à partir de Outlook|Gesendet von Outlook|Enviado desde Outlook|差出人 Outlook).*$", Pattern.CASE_INSENSITIVE);

    public static final Pattern DEFAULT_OUTLOOK_ANDROID_SIGNATURE_PATTERN = Pattern.compile("^[ ]*(Get Outlook for Android|Télécharger Outlook pour Android|Outlook for Android herunterladen|Obtener Outlook para Android).*$", Pattern.CASE_INSENSITIVE);

    private RegexList() {

    }
}
