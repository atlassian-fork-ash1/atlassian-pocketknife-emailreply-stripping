package com.atlassian.pocketknife.internal.emailreply.matcher;

import com.atlassian.pocketknife.spi.emailreply.matcher.QuotedEmailMatcher;
import com.atlassian.pocketknife.internal.emailreply.matcher.basic.DelegatingQuotedEmailMatcher;
import com.atlassian.pocketknife.internal.emailreply.matcher.basic.ForwardedMessageBlockMatcher;
import com.atlassian.pocketknife.internal.emailreply.matcher.basic.GenericAttributionBlockMatcher;
import com.atlassian.pocketknife.internal.emailreply.matcher.basic.OnDateSmbWroteBlockMatcher;
import com.atlassian.pocketknife.internal.emailreply.matcher.basic.OnDateWroteSmbBlockMatcher;
import com.google.common.collect.Lists;

import java.util.List;

/**
 * Detector for emails sent by Gmail webclient
 */
public class GmailReplyMatcher extends DelegatingQuotedEmailMatcher
{
    @Override
    protected List<QuotedEmailMatcher> createDelegators()
    {
        return Lists.<QuotedEmailMatcher>newArrayList(
                new OnDateSmbWroteBlockMatcher(),
                new OnDateWroteSmbBlockMatcher(),
                new GenericAttributionBlockMatcher(),
                new ForwardedMessageBlockMatcher()
        );
    }
}
