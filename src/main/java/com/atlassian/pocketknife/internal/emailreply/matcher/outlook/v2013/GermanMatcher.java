package com.atlassian.pocketknife.internal.emailreply.matcher.outlook.v2013;

public class GermanMatcher extends BaseMatcher
{
    private final MarkerDetector markerDetector;

    public GermanMatcher()
    {
        String FROM_REGEX_SKELETON = "^Von[\\s]*:${0}[\\s]*$";
        String TO_REGEX_SKELETON = "^An[\\s]*:${0}$";
        String CC_REGEX_SKELETON = "^Cc[\\s]*:${0}$";
        String DATE_REGEX = "^Gesendet[\\s]*:.*$";
        String DATE_AT_TOP_REGEX = "^Gesendet[\\s]*:.*(Von|An|Cc|Betreff)[\\s]*:.*$";
        String SUBJECT_REGEX = "^Betreff[\\s]*:.*$";
        String SUBJECT_AT_TOP_REGEX = "^Betreff[\\s]*:.*(Von|An|Cc|Gesendet)[\\s]*:.*$";

        markerDetector = new MarkerDetector(
                FROM_REGEX_SKELETON,
                TO_REGEX_SKELETON,
                CC_REGEX_SKELETON,
                DATE_REGEX,
                DATE_AT_TOP_REGEX,
                SUBJECT_REGEX,
                SUBJECT_AT_TOP_REGEX);
    }

    @Override
    protected MarkerDetector getMarkerDetector()
    {
        return markerDetector;
    }
}
