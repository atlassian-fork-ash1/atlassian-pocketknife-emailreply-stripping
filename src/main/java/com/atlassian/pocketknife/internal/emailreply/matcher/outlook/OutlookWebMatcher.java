package com.atlassian.pocketknife.internal.emailreply.matcher.outlook;

import com.atlassian.pocketknife.internal.emailreply.matcher.ProxyQuotedEmailMatcher;
import com.atlassian.pocketknife.internal.emailreply.matcher.basic.FromToSubjectDateBlockMatcher;
import com.atlassian.pocketknife.internal.emailreply.matcher.outlook.web.signature.DefaultOutlookWebSignatureBlockMatcher;
import com.atlassian.pocketknife.spi.emailreply.matcher.QuotedEmailMatcher;
import com.google.common.collect.ImmutableList;

import java.util.List;

public class OutlookWebMatcher extends ProxyQuotedEmailMatcher {

    private final List<QuotedEmailMatcher> delegators =
            ImmutableList.of(
                    new FromToSubjectDateBlockMatcher(),
                    new DefaultOutlookWebSignatureBlockMatcher(),
                    new com.atlassian.pocketknife.internal.emailreply.matcher.outlook.web.Matcher()
            );

    @Override
    protected List<QuotedEmailMatcher> getDelegators() {
        return delegators;
    }
}
