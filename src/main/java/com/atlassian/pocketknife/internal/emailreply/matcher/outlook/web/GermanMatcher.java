package com.atlassian.pocketknife.internal.emailreply.matcher.outlook.web;

public class GermanMatcher extends BaseMatcher {
    private final MarkerDetector markerDetector;

    public GermanMatcher() {

        String FROM_REGEX_SKELETON = "^Von[\\s]*:${0}[\\s]*$";
        String TO_REGEX_SKELETON = "^An[\\s]*:.*$";
        String CC_REGEX_SKELETON = "^Cc[\\s]*:.*$";
        String DATE_REGEX = "^Gesendet[\\s]*:.*$";
        String SUBJECT_REGEX = "^Betreff[\\s]*:.*$";

        markerDetector = new MarkerDetector(
                FROM_REGEX_SKELETON,
                TO_REGEX_SKELETON,
                CC_REGEX_SKELETON,
                DATE_REGEX,
                SUBJECT_REGEX);
    }

    @Override
    protected MarkerDetector getMarkerDetector() {
        return markerDetector;
    }
}
