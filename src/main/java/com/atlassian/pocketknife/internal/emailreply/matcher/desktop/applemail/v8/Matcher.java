package com.atlassian.pocketknife.internal.emailreply.matcher.desktop.applemail.v8;

import com.atlassian.pocketknife.spi.emailreply.matcher.QuotedEmailMatcher;
import com.atlassian.pocketknife.internal.emailreply.matcher.util.RegexUtils;
import com.google.common.collect.Lists;

import java.util.List;
import java.util.regex.Pattern;

/**
 * <p>Sample reply quoted text beginning marker: <br/>
 * > On Jun 9, 2015, at 2:55 PM, customer <chuongnn.atlassian.ad@gmail.com> wrote: <br/>
 * > <br/>
 * > blah blah blah <br/>
 * </p>
 * <p>
 * <p>Sample forward quoted text beginning marker: <br/>
 * > Begin forwarded message: <br/>
 * >  <br/>
 * > From: customer <chuongnn.atlassian.ad@gmail.com>  <br/>
 * </p>
 * <p>
 * <p> Matching logic: <br/>
 * 1. Text block begins with a attribution line (either reply or forward). <br/>
 * 2. Attribution line is followed by all quoted lines. <br/>
 * </p>
 */
public class Matcher implements QuotedEmailMatcher
{
    private final String ATTRIBUTION_LINE_BEGIN_REGEX = "^>[\\s]+";
    private final String TIME_REGEX = "[0-9]+:[0-9]+[\\s]+(PM|AM)";
    private final String MILITARY_TIME_REGEX = "[0-9]+:[0-9]+";
    private final String NAME_REGEX = "[^<]+[\\s]+<[^@]+@[^>]+>";

    private final String ATTRIBUTION_LINE_ENGLISH_REPLY_REGEX_SKELETON = "${0}On[\\s]+[A-Za-z]+[\\s]+[0-9]+,[\\s]+[0-9]+,[\\s]+at[\\s]+${1},[\\s]+${2}[\\s]+wrote:$";
    private final String ATTRIBUTION_LINE_FRENCH_REPLY_REGEX_SKELETON = "${0}Le[\\s]+[0-9]+[^0-9]+[\\s]+[0-9]+[\\s]+à[\\s]+${1},[\\s]+${2}[\\s]+a[\\s]+écrit[\\s]+:$";
    private final String ATTRIBUTION_LINE_GERMAN_REPLY_REGEX_SKELETON = "${0}Am[\\s]+[0-9]+\\.[0-9]+\\.[0-9]+[\\s]+um[\\s]+${1}[\\s]+schrieb[\\s]+${2}:$";
    private final String ATTRIBUTION_LINE_JAPANESE_REPLY_REGEX_SKELETON = "${0}[0-9]+\\/[0-9]+\\/[0-9]+[\\s]+${1}、${2}[\\s]+のメール：$";
    private final String ATTRIBUTION_LINE_SPANISH_REPLY_REGEX_SKELETON = "${0}El[\\s]+[0-9]+\\/[0-9]+\\/[0-9]+,[\\s]+a[\\s]+las[\\s]+${1},[\\s]+${2}[\\s]+escribió:$";
    private final String ATTRIBUTION_LINE_ENGLISH_FORWARD_REGEX = "^>[\\s]+Begin[\\s]+forwarded[\\s]+message:$";
    private final String ATTRIBUTION_LINE_FRENCH_FORWARD_REGEX = "^>[\\s]+Début[\\s]+du[\\s]+message[\\s]+réexpédié[\\s]+:$";
    private final String ATTRIBUTION_LINE_GERMAN_FORWARD_REGEX = "^>[\\s]+Anfang[\\s]+der[\\s]+weitergeleiteten[\\s]+Nachricht:$";
    private final String ATTRIBUTION_LINE_JAPANESE_FORWARD_REGEX = "^>[\\s]+転送されたメッセージ：$";
    private final String ATTRIBUTION_LINE_SPANISH_FORWARD_REGEX = "^>[\\s]+Inicio[\\s]+del[\\s]+mensaje[\\s]+reenviado:$";
    private final String QUOTED_LINE_REGEX = "^>.*$";

    /**
     * package private for testing.
     */
    final Pattern ATTRIBUTION_LINE_ENGLISH_REPLY_PATTERN = Pattern.compile(
            makeReplyRegex(
                    ATTRIBUTION_LINE_ENGLISH_REPLY_REGEX_SKELETON), Pattern.CASE_INSENSITIVE);
    final Pattern ATTRIBUTION_LINE_FRENCH_REPLY_PATTERN = Pattern.compile(
            makeReplyRegex(
                    ATTRIBUTION_LINE_FRENCH_REPLY_REGEX_SKELETON), Pattern.CASE_INSENSITIVE);
    final Pattern ATTRIBUTION_LINE_GERMAN_REPLY_PATTERN = Pattern.compile(
            makeReplyRegex(
                    ATTRIBUTION_LINE_GERMAN_REPLY_REGEX_SKELETON), Pattern.CASE_INSENSITIVE);
    final Pattern ATTRIBUTION_LINE_JAPANESE_REPLY_PATTERN = Pattern.compile(
            makeReplyRegex(
                    ATTRIBUTION_LINE_JAPANESE_REPLY_REGEX_SKELETON), Pattern.CASE_INSENSITIVE);
    final Pattern ATTRIBUTION_LINE_SPANISH_REPLY_PATTERN = Pattern.compile(
            makeReplyRegex(
                    ATTRIBUTION_LINE_SPANISH_REPLY_REGEX_SKELETON), Pattern.CASE_INSENSITIVE);
    final Pattern ATTRIBUTION_LINE_ENGLISH_MILITARY_TIME_REPLY_PATTERN = Pattern.compile(
            makeMilitaryTimeReplyRegex(
                    ATTRIBUTION_LINE_ENGLISH_REPLY_REGEX_SKELETON), Pattern.CASE_INSENSITIVE);
    final Pattern ATTRIBUTION_LINE_FRENCH_MILITARY_TIME_REPLY_PATTERN = Pattern.compile(
            makeMilitaryTimeReplyRegex(
                    ATTRIBUTION_LINE_FRENCH_REPLY_REGEX_SKELETON), Pattern.CASE_INSENSITIVE);
    final Pattern ATTRIBUTION_LINE_GERMAN_MILITARY_TIME_REPLY_PATTERN = Pattern.compile(
            makeMilitaryTimeReplyRegex(
                    ATTRIBUTION_LINE_GERMAN_REPLY_REGEX_SKELETON), Pattern.CASE_INSENSITIVE);
    final Pattern ATTRIBUTION_LINE_JAPANESE_MILITARY_TIME_REPLY_PATTERN = Pattern.compile(
            makeMilitaryTimeReplyRegex(
                    ATTRIBUTION_LINE_JAPANESE_REPLY_REGEX_SKELETON), Pattern.CASE_INSENSITIVE);
    final Pattern ATTRIBUTION_LINE_SPANISH_MILITARY_TIME_REPLY_PATTERN = Pattern.compile(
            makeMilitaryTimeReplyRegex(
                    ATTRIBUTION_LINE_SPANISH_REPLY_REGEX_SKELETON), Pattern.CASE_INSENSITIVE);
    final Pattern ATTRIBUTION_LINE_ENGLISH_FORWARD_PATTERN = Pattern.compile(
            ATTRIBUTION_LINE_ENGLISH_FORWARD_REGEX, Pattern.CASE_INSENSITIVE);
    final Pattern ATTRIBUTION_LINE_FRENCH_FORWARD_PATTERN = Pattern.compile(
            ATTRIBUTION_LINE_FRENCH_FORWARD_REGEX, Pattern.CASE_INSENSITIVE);
    final Pattern ATTRIBUTION_LINE_GERMAN_FORWARD_PATTERN = Pattern.compile(
            ATTRIBUTION_LINE_GERMAN_FORWARD_REGEX, Pattern.CASE_INSENSITIVE);
    final Pattern ATTRIBUTION_LINE_JAPANESE_FORWARD_PATTERN = Pattern.compile(
            ATTRIBUTION_LINE_JAPANESE_FORWARD_REGEX, Pattern.CASE_INSENSITIVE);
    final Pattern ATTRIBUTION_LINE_SPANISH_FORWARD_PATTERN = Pattern.compile(
            ATTRIBUTION_LINE_SPANISH_FORWARD_REGEX, Pattern.CASE_INSENSITIVE);
    final Pattern QUOTED_LINE_PATTERN = Pattern.compile(QUOTED_LINE_REGEX, Pattern.CASE_INSENSITIVE);

    final List<Pattern> ATTRIBUTION_LINE_PATTERNS = Lists.newArrayList(
            ATTRIBUTION_LINE_ENGLISH_REPLY_PATTERN,
            ATTRIBUTION_LINE_ENGLISH_MILITARY_TIME_REPLY_PATTERN,
            ATTRIBUTION_LINE_ENGLISH_FORWARD_PATTERN,
            ATTRIBUTION_LINE_FRENCH_REPLY_PATTERN,
            ATTRIBUTION_LINE_FRENCH_MILITARY_TIME_REPLY_PATTERN,
            ATTRIBUTION_LINE_FRENCH_FORWARD_PATTERN,
            ATTRIBUTION_LINE_GERMAN_REPLY_PATTERN,
            ATTRIBUTION_LINE_GERMAN_MILITARY_TIME_REPLY_PATTERN,
            ATTRIBUTION_LINE_GERMAN_FORWARD_PATTERN,
            ATTRIBUTION_LINE_JAPANESE_REPLY_PATTERN,
            ATTRIBUTION_LINE_JAPANESE_MILITARY_TIME_REPLY_PATTERN,
            ATTRIBUTION_LINE_JAPANESE_FORWARD_PATTERN,
            ATTRIBUTION_LINE_SPANISH_REPLY_PATTERN,
            ATTRIBUTION_LINE_SPANISH_MILITARY_TIME_REPLY_PATTERN,
            ATTRIBUTION_LINE_SPANISH_FORWARD_PATTERN);

    @Override
    public boolean isQuotedEmail(List<String> textBlock)
    {
        if (textBlock == null || textBlock.size() == 0)
        {
            return false;
        }
        boolean attributionLineMatched = isAttributionLine(textBlock.get(0));
        boolean allQuotedLinesMatched = true;
        for (int i = 1; i < textBlock.size(); i++)
        {
            allQuotedLinesMatched &= isQuotedLine(textBlock.get(i));
            if (!allQuotedLinesMatched)
            {
                break;
            }
        }
        return attributionLineMatched && allQuotedLinesMatched;
    }

    boolean isAttributionLine(String line)
    {
        for (Pattern pattern : ATTRIBUTION_LINE_PATTERNS)
        {
            if (RegexUtils.match(pattern, line))
            {
                return true;
            }
        }
        return false;
    }

    boolean isQuotedLine(String line)
    {
        return RegexUtils.match(QUOTED_LINE_PATTERN, line);
    }

    String makeReplyRegex(String skeleton)
    {
        return RegexUtils.buildRegexFromSkeleton(skeleton, ATTRIBUTION_LINE_BEGIN_REGEX, TIME_REGEX, NAME_REGEX);
    }

    String makeMilitaryTimeReplyRegex(String skeleton)
    {
        return RegexUtils.buildRegexFromSkeleton(
                skeleton, ATTRIBUTION_LINE_BEGIN_REGEX, MILITARY_TIME_REGEX, NAME_REGEX);
    }
}
