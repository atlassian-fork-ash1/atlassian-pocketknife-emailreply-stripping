package com.atlassian.pocketknife.internal.emailreply.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static java.util.Collections.unmodifiableList;
import static org.apache.commons.lang.StringUtils.EMPTY;
import static org.apache.commons.lang.StringUtils.defaultIfEmpty;

/**
 * <p>
 * Utility class to stream the text body of an email in a block of up to 5 lines.
 * This class contains a fixed size sliding window that will incrementally advance along the
 * text body. See TestEmailTextReader for more info on this behavior
 * </p>
 * <p>
 * <p>Usage:
 * <blockquote><pre>{@code
 *     final EmailTextReader reader = new EmailTextReader(emailBodyString);
 *     List<String> block = null;
 *     do
 *     {
 *         block = reader.readNextBlock();
 * <p>
 *         if (block.isEmpty())
 *         {
 *              // we've reached the end of the body
 *              break;
 *         }
 * <p>
 *         // do something with block
 *     }
 *     while (block.size() > 0)
 * }</pre></blockquote>
 * </p>
 */
public class EmailTextReader {
    public static final int BLOCK_SIZE = 5;
    private final Scanner scanner;
    private final List<String> currentBlock;

    public EmailTextReader(final String content) {
        this.scanner = new Scanner(defaultIfEmpty(content, EMPTY));
        this.currentBlock = new ArrayList<>(BLOCK_SIZE);
    }

    /**
     * <p>Advance the fixed size sliding window to the next available line.</p>
     * <p>
     * <p>If this is the first time this method is called, then it will
     * try to read up to first 5 lines. Subsequent calls to this method
     * will advance the window to the next available line
     * </p>
     *
     * @return list of up to 5 lines. Empty list if there is nothing left to read
     */
    public List<String> readNextBlock() {
        if (!currentBlock.isEmpty()) {
            currentBlock.remove(0);
        }

        while (scanner.hasNextLine() && currentBlock.size() < BLOCK_SIZE) {
            currentBlock.add(scanner.nextLine());
        }

        return unmodifiableList(currentBlock);
    }
}
