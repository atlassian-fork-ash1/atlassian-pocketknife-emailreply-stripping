package com.atlassian.pocketknife.internal.emailreply.matcher.outlook.web;

public class JapaneseMatcher extends BaseMatcher {
    private final MarkerDetector markerDetector;

    public JapaneseMatcher() {

        String FROM_REGEX_SKELETON = "^差出人[\\s]*:${0}[\\s]*$";
        String TO_REGEX_SKELETON = "^宛先[\\s]*:.*$";
        String CC_REGEX_SKELETON = "^ＣＣ[\\s]*:.*$";
        String DATE_REGEX = "^送信日時[\\s]*:.*$";
        String SUBJECT_REGEX = "^件名[\\s]*:.*$";

        markerDetector = new MarkerDetector(
                FROM_REGEX_SKELETON,
                TO_REGEX_SKELETON,
                CC_REGEX_SKELETON,
                DATE_REGEX,
                SUBJECT_REGEX);
    }

    @Override
    protected MarkerDetector getMarkerDetector() {
        return markerDetector;
    }
}
