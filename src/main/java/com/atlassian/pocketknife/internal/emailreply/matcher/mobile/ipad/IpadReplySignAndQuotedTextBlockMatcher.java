package com.atlassian.pocketknife.internal.emailreply.matcher.mobile.ipad;

import com.atlassian.pocketknife.internal.emailreply.matcher.RegexList;
import com.atlassian.pocketknife.internal.emailreply.matcher.basic.StatelessQuotedEmailMatcher;
import com.atlassian.pocketknife.internal.emailreply.util.LineMatchingUtil;
import com.atlassian.pocketknife.internal.emailreply.util.TextBlockUtil;
import org.apache.commons.lang.StringUtils;

import javax.annotation.Nonnull;
import java.util.List;

/**
 * Supported patterns:
 * <br/>
 * Pattern 1
 * <pre>
 *     Sent from my iPad
 *     (blank line)
 *     > On ... someone wrote:
 *     > quoted text
 * </pre>
 * Pattern 2
 * <pre>
 *     Sent from my iPad
 *     > On ... someone wrote:
 *     > quoted text
 * </pre>
 * Supported languages: English, French, Spanish, Japanese
 */
public class IpadReplySignAndQuotedTextBlockMatcher extends StatelessQuotedEmailMatcher
{
    @Override
    public boolean isQuotedEmail(@Nonnull List<String> textBlock)
    {
        if (textBlock.size() < 3)
        {
            return false;
        }
        else if (textBlock.size() == 3)
        {
            return isPattern2(textBlock);
        }
        else
        {
            return isPattern1(textBlock) || isPattern2(textBlock);
        }
    }

    private boolean isPattern1(final List<String> textBlock)
    {
        final String line1 = TextBlockUtil.getLineOrEmptyString(textBlock, 0);
        final String line2 = TextBlockUtil.getLineOrEmptyString(textBlock, 1);
        final String line3 = TextBlockUtil.getLineOrEmptyString(textBlock, 2);
        final String line4 = TextBlockUtil.getLineOrEmptyString(textBlock, 3);

        return RegexList.DEFAULT_IPAD_SIGNATURE_PATTERN.matcher(line1).find()
                && StringUtils.isBlank(line2)
                && isQuotedAttributionLine(line3)
                && LineMatchingUtil.isStartWithQuoteLineIndicator(line4);
    }

    private boolean isPattern2(final List<String> textBlock)
    {
        final String line1 = TextBlockUtil.getLineOrEmptyString(textBlock, 0);
        final String line2 = TextBlockUtil.getLineOrEmptyString(textBlock, 1);
        final String line3 = TextBlockUtil.getLineOrEmptyString(textBlock, 2);

        return RegexList.DEFAULT_IPAD_SIGNATURE_PATTERN.matcher(line1).find()
                && isQuotedAttributionLine(line2)
                && LineMatchingUtil.isStartWithQuoteLineIndicator(line3);
    }

    private boolean isQuotedAttributionLine(final String line)
    {
        return RegexList.QUOTED_ON_DATE_SMB_WROTE_PATTERN.matcher(line).find()
                || RegexList.QUOTED_ON_DATE_WROTE_SMB_PATTERN.matcher(line).find();
    }
}
