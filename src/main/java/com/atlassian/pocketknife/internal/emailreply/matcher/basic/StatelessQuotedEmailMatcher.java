package com.atlassian.pocketknife.internal.emailreply.matcher.basic;

import com.atlassian.pocketknife.spi.emailreply.matcher.QuotedEmailMatcher;

public abstract class StatelessQuotedEmailMatcher implements QuotedEmailMatcher
{
    @Override
    public int hashCode()
    {
        return this.getClass().getName().hashCode();
    }

    @Override
    public boolean equals(Object obj)
    {
        if (obj == null)
        {
            return false;
        }

        return this.getClass().getName().equals(obj.getClass().getName());
    }
}
