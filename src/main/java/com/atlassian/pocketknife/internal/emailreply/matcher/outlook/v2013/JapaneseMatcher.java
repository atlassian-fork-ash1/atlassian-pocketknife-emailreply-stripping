package com.atlassian.pocketknife.internal.emailreply.matcher.outlook.v2013;

public class JapaneseMatcher extends BaseMatcher
{
    private final MarkerDetector markerDetector;

    public JapaneseMatcher()
    {
        String FROM_REGEX_SKELETON = "^差出人[\\s]*:${0}[\\s]*$";
        String TO_REGEX_SKELETON = "^宛先[\\s]*:${0}$";
        String CC_REGEX_SKELETON = "^ＣＣ[\\s]*:${0}$";
        String DATE_REGEX = "^送信日時[\\s]*:.*$";
        String DATE_AT_TOP_REGEX = "^送信日時[\\s]*:.*(差出人|宛先|ＣＣ|件名)[\\s]*:.*$";
        String SUBJECT_REGEX = "^件名[\\s]*:.*$";
        String SUBJECT_AT_TOP_REGEX = "^件名[\\s]*:.*(差出人|宛先|ＣＣ|送信日時)[\\s]*:.*$";

        markerDetector = new MarkerDetector(
                FROM_REGEX_SKELETON,
                TO_REGEX_SKELETON,
                CC_REGEX_SKELETON,
                DATE_REGEX,
                DATE_AT_TOP_REGEX,
                SUBJECT_REGEX,
                SUBJECT_AT_TOP_REGEX);
    }

    @Override
    protected MarkerDetector getMarkerDetector()
    {
        return markerDetector;
    }
}
