package com.atlassian.pocketknife.internal.emailreply;

import com.atlassian.mail.MailUtils;
import com.atlassian.mail.converters.HtmlConverter;
import com.atlassian.mail.converters.basic.HtmlToTextConverter;
import com.atlassian.mail.options.GetBodyOptions;
import com.atlassian.pocketknife.api.emailreply.EmailReplyCleaner;
import com.atlassian.pocketknife.spi.emailreply.matcher.QuotedEmailMatcher;
import com.google.common.annotations.VisibleForTesting;
import org.apache.commons.lang.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.mail.Message;
import javax.mail.MessagingException;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.atlassian.pocketknife.internal.emailreply.util.LineMatchingUtil.stripEndOfQuoteLineIndicators;
import static org.apache.commons.lang.StringUtils.EMPTY;
import static org.apache.commons.lang.StringUtils.contains;
import static org.apache.commons.lang.StringUtils.defaultString;
import static org.apache.commons.lang.StringUtils.deleteWhitespace;
import static org.apache.commons.lang.StringUtils.indexOf;
import static org.apache.commons.lang.StringUtils.isBlank;
import static org.apache.commons.lang.StringUtils.isEmpty;
import static org.apache.commons.lang.StringUtils.length;
import static org.apache.commons.lang.StringUtils.normalizeSpace;
import static org.apache.commons.lang.StringUtils.removeStart;
import static org.apache.commons.lang.StringUtils.replaceEach;
import static org.apache.commons.lang.StringUtils.split;
import static org.apache.commons.lang.StringUtils.startsWith;
import static org.apache.commons.lang.StringUtils.stripToEmpty;
import static org.apache.commons.lang.StringUtils.substring;
import static org.apache.commons.lang.StringUtils.substringAfterLast;
import static org.apache.commons.lang.StringUtils.substringBeforeLast;
import static org.apache.commons.lang.StringUtils.trimToEmpty;

/**
 * This class will expect html content that it can parse into a DOM.
 * <p>
 * Then it will convert the html to text using {@link HtmlConverter} and then use
 * {@link DefaultEmailReplyCleaner} with provided matchers to clean the email and finally, align
 * the html with remaining text, to return a html string with the unwanted text removed from DOM.
 * <p>
 * This will then run another clean with {@link DefaultEmailReplyCleaner} over the converted content to catch any
 * cases that can't be handled. For example, when aligning the text and html parts of email, if the text content
 * is different, this will not be possible. Therefore if there are any matchers configured to handle this case,
 * they can be picked up in this final step.
 */
public class HtmlEmailReplyCleaner implements EmailReplyCleaner {
    private static final Logger log = LoggerFactory.getLogger(HtmlEmailReplyCleaner.class);
    private static final HtmlConverter HTML_TO_TEXT_CONVERTER = new HtmlToTextConverter();
    private static final String DASH_LINE = "\n----------------------------------------------------------------------------------------\n";

    private final DefaultEmailReplyCleaner defaultEmailReplyCleaner;
    private final HtmlConverter externalHtmlConverter;
    private final HtmlConverter wrappedHtmlConverter;
    private final AtomicBoolean wrappedHtmlConverterIsExecuted;

    public HtmlEmailReplyCleaner(HtmlConverter htmlConverter, List<QuotedEmailMatcher> matchers) {
        this.wrappedHtmlConverterIsExecuted = new AtomicBoolean(false);
        this.defaultEmailReplyCleaner = new DefaultEmailReplyCleaner(matchers);
        this.externalHtmlConverter = htmlConverter;
        this.wrappedHtmlConverter = html -> {
            final String convertedHtml = externalHtmlConverter.convert(html);
            wrappedHtmlConverterIsExecuted.set(true);
            return convertedHtml;
        };
    }

    public HtmlConverter getHtmlConverter() {
        return externalHtmlConverter;
    }

    public DefaultEmailReplyCleaner getDefaultEmailReplyCleaner() {
        return defaultEmailReplyCleaner;
    }

    @Nonnull
    @Override
    public EmailCleanerResult cleanQuotedEmail(@Nonnull Message message) throws MessagingException, IOException {
        final AtomicBoolean emailHasOnlyHtmlPart = new AtomicBoolean(false);
        final String textBody =
                defaultString(
                        MailUtils.getBody(
                                message,
                                GetBodyOptions.builder()
                                        .setPreferHtmlPart(false)
                                        .setStripWhitespace(false)
                                        .setHtmlConverter(html -> {
                                            emailHasOnlyHtmlPart.set(true);
                                            return html;
                                        })
                                        .build()
                        )
                );

        final AtomicBoolean emailHasHtmlPart = new AtomicBoolean(false);
        final String htmlBody =
                defaultString(
                        MailUtils.getBody(
                                message,
                                GetBodyOptions.builder()
                                        .preferHtmlPart()
                                        .stripWhitespace()
                                        .setHtmlConverter(html -> {
                                            emailHasHtmlPart.set(true);
                                            return html;
                                        })
                                        .build()
                        )
                );

        final String body;
        final String originalBody;
        if (emailHasOnlyHtmlPart.get()) {
            originalBody = this.wrappedHtmlConverter.convert(htmlBody);

            // the mail does not contain any text part, it is all html
            // this will likely fail most of the time, but could get lucky some times as well!
            // start by converting Html to Text with simple converter
            final EmailCleanerResult strippedResult = defaultEmailReplyCleaner.cleanQuotedEmailInternal(
                    cleanupLinkCitations(HTML_TO_TEXT_CONVERTER.convert(htmlBody))
            );

            body = processHtmlWithStrippedText(htmlBody, strippedResult);
        } else {
            final EmailCleanerResult strippedResult = defaultEmailReplyCleaner.cleanQuotedEmailInternal(textBody);
            if (emailHasHtmlPart.get()) {
                originalBody = this.wrappedHtmlConverter.convert(htmlBody);
                // it has text and html
                body = processHtmlWithStrippedText(htmlBody, strippedResult);
            } else {
                originalBody = strippedResult.getOriginalBody();
                // it has text, and no html, so whatever the text is, we will have to use
                body = strippedResult.getRawBody();
            }
        }

        final String whitespaceStrippedBody = stripToEmpty(body);

        if (wrappedHtmlConverterIsExecuted.get()) {
            // if the converter was executed, for completeness (and custom strip ability), run stripper over what was converted.
            // this will catch any strippers added to handle html to wiki conversion for example.
            //
            // Perform the check on the whitespace stripped Body, as if custom matchers are in play, the stripped body is what
            // will be getting returned and visible in most cases, so run on that, instead of something with potentially
            // unexpected whitespace
            final EmailCleanerResult emailCleanerResult = defaultEmailReplyCleaner.cleanQuotedEmailInternal(whitespaceStrippedBody);
            return new EmailCleanerResult(stripToEmpty(emailCleanerResult.getRawBody()), originalBody);
        } else {
            return new EmailCleanerResult(whitespaceStrippedBody, originalBody);
        }
    }

    private String processHtmlWithStrippedText(final String htmlBody, final EmailCleanerResult emailCleanerResult) throws IOException {
        final String body;
        if (StringUtils.equals(stripToEmpty(emailCleanerResult.getOriginalBody()), stripToEmpty(emailCleanerResult.getRawBody()))) {
            // nothing was stripped from the text, so assume that all of the email is wanted, and convert the html to wiki
            try {
                return this.wrappedHtmlConverter.convert(htmlBody);
            } catch (IOException e) {
                log.warn("Error while converting html body to text", e);
                throw e;
            }
        } else {
            // stuff was stripped off the text body.
            // try and find what was stripped, as a marker, so that we can remove that before converting to wiki
            final Document document = Jsoup.parse(htmlBody);
            document.outputSettings().prettyPrint(false);

            HtmlCleanerHelper htmlCleanerHelper = new HtmlCleanerHelper(emailCleanerResult.getRawBody());
            htmlCleanerHelper.cleanHtml(document.body());

            try {
                body = this.wrappedHtmlConverter.convert(document.outerHtml());
            } catch (IOException e) {
                log.warn("Error while converting stripped html body to text", e);
                throw e;
            }
        }
        return body;
    }

    /**
     * These values are what the {@link com.atlassian.mail.converters.basic.HtmlToTextConverter} uses to inline reference links at bottom of conversion, for example:
     * <pre>
     *      ----------------------------------------------------------------------------------------
     *      [1] mailto:testing@home.com
     *      [2] http://www.adelaidenow.com.au/
     *      [3] http://www.adelaidenow.com.au/
     *      [4] http://www.adelaidenow.com.au
     *      [5] http://www.google.com
     * </pre>
     */
    @VisibleForTesting
    static String cleanupLinkCitations(final String body) {
        if (!contains(body, DASH_LINE)) {
            // quick win, nothing to do
            return body;
        }

        final String citations = substringAfterLast(body, DASH_LINE);

        if (isBlank(citations)) {
            return body;
        }

        String cleanedBody = substringBeforeLast(body, DASH_LINE);
        int currentSubstringIndex = 0;
        for (String cite : split(citations, "\n")) {
            if (isBlank(cite)) {
                continue;
            }

            final Pattern p = Pattern.compile("^\\[([0-9]+)\\].*");
            final Matcher m = p.matcher(cite);

            if (m.matches()) {
                final String number = m.group(1);

                final String pointer = "[" + number + "]";

                final int index = indexOf(cleanedBody, pointer, currentSubstringIndex);

                if (index >= 0) {
                    cleanedBody = substring(cleanedBody, 0, index) + substring(cleanedBody, index + pointer.length());
                    currentSubstringIndex = index;
                } else {
                    log.debug("Expected to find {} in {}, but did not see anything ....", "[" + number + "]", cleanedBody);
                }
            }
        }

        return cleanedBody;
    }

    private static class HtmlCleanerHelper {

        // \u3000 is the double-byte space character in UTF-8
        // \u00A0 is the non-breaking space character (&nbsp;)
        // \u2007 is the figure space character (&#8199;)
        // \u202F is the narrow non-breaking space character (&#8239;)
        private static final String[] UNICODE_WHITESPACE = new String[]{"\u3000", "\u00A0", "\u2007", "\u202F"};
        private static final String[] UNICODE_WHITESPACE_REPLACE = new String[]{" ", " ", " ", " "};

        private static final String[] STYLING_CHARACTERS = new String[]{"*", "/", "_", "-", ">", "|", "+"};
        private static final String[] STYLING_CHARACTERS_REPLACE = new String[]{EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY};

        private String expectedNormalizedText = EMPTY;
        private boolean finished = false;

        public HtmlCleanerHelper(String expectedStrippedText) {
            this.expectedNormalizedText = stripEndOfQuoteLineIndicators(normalizeText(expectedStrippedText, false));
        }

        public void cleanHtml(final Node startNode) {
            for (Node node : startNode.childNodes()) {
                if (finished) {
                    processUnwantedText(node);
                    continue;
                }

                final String normalizedText = extractNormalizedTextLine(node);
                if (processExpectedText(normalizedText)) {
                    continue;
                }

                if (startsWith(normalizedText, trimToEmpty(this.expectedNormalizedText))
                        || startsWith(trimStylings(normalizedText), trimStylings(this.expectedNormalizedText))
                        || startsWith(trimStylingsAndNumbers(normalizedText), trimStylingsAndNumbers(this.expectedNormalizedText))) {
                    // is there a part of the element that should be kept?
                    for (Node childNode : node.childNodes()) {
                        if (finished) {
                            processUnwantedText(childNode);
                            continue;
                        }

                        // check if a partial text node?
                        processExpectedPartialText(childNode);

                        cleanHtml(childNode);
                    }
                }
            }
        }

        private String normalizeText(final String text) {
            return normalizeText(text, true);
        }

        private String normalizeText(final String text, boolean deleteWhitespace) {
            String normalized = normalizeSpace(text);
            // Java does not remove non-breaking-whitespace, as it is not regarded as whitespace :(
            // So we will do it ourselves to get all types of spaces down to a single space only
            normalized = replaceEach(normalized, UNICODE_WHITESPACE, UNICODE_WHITESPACE_REPLACE).replaceAll("\\s+", " ");
            normalized = trimToEmpty(normalized);
            if (deleteWhitespace) {
                return deleteWhitespace(normalized);
            } else {
                return normalized;
            }
        }

        private String trimStylings(final String text) {
            return trimStylings(text, true);
        }

        private String trimStylings(final String text, boolean deleteWhitespace) {
            String normalized = replaceEach(text, STYLING_CHARACTERS, STYLING_CHARACTERS_REPLACE);
            return normalizeText(normalized, deleteWhitespace);
        }

        private String trimStylingsAndNumbers(final String text) {
            return trimStylingsAndNumbers(text, true);
        }

        private String trimStylingsAndNumbers(final String text, boolean deleteWhitespace) {
            final String normalized;
            if (deleteWhitespace) {
                normalized = text.replaceAll("[\\d]+\\.", EMPTY);
            } else {
                normalized = text.replaceAll("[\\d]+\\. ", EMPTY);
            }
            return trimStylings(normalized, deleteWhitespace);
        }

        private String extractNormalizedTextLine(final Node node) {
            final String text;
            if (node instanceof TextNode) {
                final TextNode textNode = (TextNode) node;
                text = textNode.text();
            } else if (node instanceof Element) {
                final Element element = (Element) node;
                text = element.text();
            } else {
                text = EMPTY;
            }

            if (isBlank(text)) {
                // don't care
                return EMPTY;
            }

            return normalizeText(text);
        }

        private boolean processExpectedText(final String normalizedText) {
            if (StringUtils.equals(normalizeText(this.expectedNormalizedText), normalizedText)
                    || StringUtils.equals(trimStylings(this.expectedNormalizedText), trimStylings(normalizedText))
                    || StringUtils.equals(trimStylingsAndNumbers(this.expectedNormalizedText), trimStylingsAndNumbers(normalizedText))) {
                this.expectedNormalizedText = EMPTY;
                finished = true;
                return true;
            } else if (startsWith(normalizeText(this.expectedNormalizedText), normalizedText)) {
                this.expectedNormalizedText = removeStart(normalizeText(this.expectedNormalizedText), normalizedText);
                return true;
            } else if (startsWith(trimStylings(this.expectedNormalizedText), trimStylings(normalizedText))) {
                this.expectedNormalizedText = removeStart(trimStylings(this.expectedNormalizedText), trimStylings(normalizedText));
                return true;
            } else if (startsWith(trimStylingsAndNumbers(this.expectedNormalizedText), trimStylingsAndNumbers(normalizedText))) {
                this.expectedNormalizedText = removeStart(trimStylingsAndNumbers(this.expectedNormalizedText), trimStylingsAndNumbers(normalizedText));
                return true;
            }

            return false;
        }

        private void processExpectedPartialText(final Node node) {
            // check if a partial text node?
            if (node instanceof TextNode) {
                final String originalNodeText = ((TextNode) node).text();
                final String nodeText = normalizeText(originalNodeText);
                if (processExpectedText(nodeText)) {
                    return;
                }

                if (startsWith(nodeText, normalizeText(this.expectedNormalizedText))) {
                    ((TextNode) node).text(processPartialRemainder(normalizeText(this.expectedNormalizedText), originalNodeText));
                    this.expectedNormalizedText = EMPTY;
                    finished = true;
                } else if (startsWith(trimStylings(nodeText), trimStylings(this.expectedNormalizedText))) {
                    ((TextNode) node).text(processPartialRemainder(trimStylings(this.expectedNormalizedText), originalNodeText));
                    this.expectedNormalizedText = EMPTY;
                    finished = true;
                } else if (startsWith(trimStylingsAndNumbers(nodeText), trimStylingsAndNumbers(this.expectedNormalizedText))) {
                    ((TextNode) node).text(processPartialRemainder(trimStylingsAndNumbers(this.expectedNormalizedText), originalNodeText));
                    this.expectedNormalizedText = EMPTY;
                    finished = true;
                }
            } else if (node instanceof Element && node.childNodeSize() == 0) {
                final String originalNodeText = ((Element) node).text();
                final String elementText = normalizeText(originalNodeText);
                if (processExpectedText(elementText)) {
                    return;
                }

                if (startsWith(elementText, normalizeText(this.expectedNormalizedText))) {
                    ((Element) node).text(processPartialRemainder(normalizeText(this.expectedNormalizedText), originalNodeText));
                    this.expectedNormalizedText = EMPTY;
                    finished = true;
                } else if (startsWith(trimStylings(elementText), trimStylings(this.expectedNormalizedText))) {
                    ((Element) node).text(processPartialRemainder(trimStylings(this.expectedNormalizedText), originalNodeText));
                    this.expectedNormalizedText = EMPTY;
                    finished = true;
                } else if (startsWith(trimStylingsAndNumbers(elementText), trimStylingsAndNumbers(this.expectedNormalizedText))) {
                    ((Element) node).text(processPartialRemainder(trimStylingsAndNumbers(this.expectedNormalizedText), originalNodeText));
                    this.expectedNormalizedText = EMPTY;
                    finished = true;
                }
            }
        }

        private String processPartialRemainder(String expectedText, String originalNodeText) {
            String partialText = EMPTY;
            int increment = 0;
            while (!isEmpty(expectedText) && increment < length(originalNodeText)) {
                final char c = originalNodeText.charAt(increment++);
                partialText += c;
                if (c == expectedText.charAt(0)) {
                    expectedText = substring(expectedText, 1);
                }
            }
            return partialText;
        }

        private void processUnwantedText(final Node node) {
            if (node instanceof TextNode) {
                final TextNode textNode = (TextNode) node;
                textNode.text(EMPTY);
            } else if (node instanceof Element) {
                final Element element = (Element) node;
                if (element.hasText()) {
                    element.text(EMPTY);
                }
            }
        }
    }

}
