package com.atlassian.pocketknife.api.emailreply;

import com.atlassian.fugue.Option;
import com.atlassian.mail.converters.HtmlConverter;
import com.atlassian.mail.converters.basic.HtmlToTextConverter;
import com.atlassian.pocketknife.internal.emailreply.DefaultEmailReplyCleaner;
import com.atlassian.pocketknife.internal.emailreply.HtmlEmailReplyCleaner;
import com.atlassian.pocketknife.internal.emailreply.matcher.GmailReplyMatcher;
import com.atlassian.pocketknife.internal.emailreply.matcher.IPhoneReplyMatcher;
import com.atlassian.pocketknife.internal.emailreply.matcher.YahooReplyMatcher;
import com.atlassian.pocketknife.internal.emailreply.matcher.desktop.applemail.AppleMailMatcher;
import com.atlassian.pocketknife.internal.emailreply.matcher.desktop.ibmnotes.IbmNotesMatcher;
import com.atlassian.pocketknife.internal.emailreply.matcher.desktop.thunderbird.ThunderbirdMatcher;
import com.atlassian.pocketknife.internal.emailreply.matcher.mobile.android.AndroidMatcher;
import com.atlassian.pocketknife.internal.emailreply.matcher.mobile.ipad.IPadReplyMatcher;
import com.atlassian.pocketknife.internal.emailreply.matcher.outlook.OutlookAndroidMatcher;
import com.atlassian.pocketknife.internal.emailreply.matcher.outlook.OutlookDesktopMatcher;
import com.atlassian.pocketknife.internal.emailreply.matcher.outlook.OutlookWebMatcher;
import com.atlassian.pocketknife.spi.emailreply.matcher.QuotedEmailMatcher;
import com.google.common.annotations.VisibleForTesting;

import java.util.List;
import java.util.Set;

import static com.atlassian.fugue.Option.none;
import static com.atlassian.fugue.Option.option;
import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Sets.newLinkedHashSet;

/**
 * Use this builder to obtain a default implementation of {@link com.atlassian.pocketknife.api.emailreply.EmailReplyCleaner}
 * Supported clients:
 * <ul>
 * <li>Gmail web</li>
 * <li>Yahoo web</li>
 * <li>Outlook web</li>
 * <li>Apple iPhone Mail</li>
 * <li>Apple iPad Mail</li>
 * <li>Apple Android GMail</li>
 * <li>Apple Mail desktop</li>
 * <li>Outlook desktop</li>
 * <li>IBM Notes desktop</li>
 * <li>Thunderbird desktop</li>
 * </ul>
 * Usage:
 * <blockquote><pre>{@code
 *      // this cleaner supports the clients listed above
 *      final EmailReplyCleaner cleaner = new EmailReplyCleanerBuilder().build();
 * <p>
 *      // this cleaner supports the default clients AND your custom client
 *      final EmailReplyCleaner cleaner = new EmailReplyCleanerBuilder()
 *                                  .defaultMatchers()
 *                                  .customMatchers(yourCustomMatchers)
 *                                  .build();
 * <p>
 *      // this cleaner supports only your custom matchers
 *      final EmailReplyCleaner cleaner = new EmailReplyCleanerBuilder()
 *                                  .customMatchers(yourCustomMatchers)
 *                                  .build();
 * <p>
 *      // this cleaner supports only Gmail client
 *      final EmailReplyCleaner cleaner = new EmailReplyCleanerBuilder()
 *                                  .gmailMatcher()
 *                                  .build();
 * }</pre></blockquote>
 */
public class EmailReplyCleanerBuilder {
    private static final HtmlConverter HTML_CONVERTER = new HtmlToTextConverter();

    private final Set<QuotedEmailMatcher> matchers = newLinkedHashSet();
    private Option<HtmlConverter> htmlConverter = none();

    /**
     * Add support for gmail.com
     *
     * @return
     */
    public EmailReplyCleanerBuilder gmailMatcher() {
        matchers.add(new GmailReplyMatcher());
        return this;
    }

    /**
     * Add support for outlook.com
     *
     * @return
     */
    public EmailReplyCleanerBuilder outlookWebMatcher() {
        matchers.add(new OutlookWebMatcher());
        return this;
    }

    /**
     * Add support for yahoo.com
     *
     * @return
     */
    public EmailReplyCleanerBuilder yahooWebMatcher() {
        matchers.add(new YahooReplyMatcher());
        return this;
    }

    /**
     * Add support for iPhone Mail
     *
     * @return
     */
    public EmailReplyCleanerBuilder iphoneMatcher() {
        matchers.add(new IPhoneReplyMatcher());
        return this;
    }

    /**
     * Add support for Apple Mail Desktop Client
     *
     * @return
     */
    public EmailReplyCleanerBuilder appleMailDesktopMatcher() {
        matchers.add(new AppleMailMatcher());
        return this;
    }

    /**
     * Add support for Outlook Desktop Clients
     *
     * @return
     */
    public EmailReplyCleanerBuilder outlookDesktopMatcher() {
        matchers.add(new OutlookDesktopMatcher());
        return this;
    }

    /**
     * Add support for IBM Notes Desktop Client
     *
     * @return
     */
    public EmailReplyCleanerBuilder ibmNotesDesktopMatcher() {
        matchers.add(new IbmNotesMatcher());
        return this;
    }

    /**
     * Add support for Thunderbird Desktop Client
     *
     * @return
     */
    public EmailReplyCleanerBuilder thunderbirdDesktopMatcher() {
        matchers.add(new ThunderbirdMatcher());
        return this;
    }

    /**
     * Add support for iPad Mail
     *
     * @return
     */
    public EmailReplyCleanerBuilder ipadMatcher() {
        matchers.add(new IPadReplyMatcher());
        return this;
    }

    /**
     * Add support for Android Gmail
     *
     * @return
     */
    public EmailReplyCleanerBuilder androidMatcher() {
        matchers.add(new AndroidMatcher());
        return this;
    }

    /**
     * Add support for Outlook Android Application
     *
     * @return
     */
    public EmailReplyCleanerBuilder outlookAndroidMatcher() {
        matchers.add(new OutlookAndroidMatcher());
        return this;
    }

    /**
     * Add your custom matchers to the list
     *
     * @param customMatchers
     * @return
     */
    public EmailReplyCleanerBuilder customMatchers(final List<QuotedEmailMatcher> customMatchers) {
        if (customMatchers != null) {
            this.matchers.addAll(customMatchers);
        }
        return this;
    }

    /**
     * Add support for gmail.com, yahoo.com, outlook.com, iPhone Mail
     *
     * @return
     */
    public EmailReplyCleanerBuilder defaultMatchers() {
        gmailMatcher();
        outlookWebMatcher();
        yahooWebMatcher();
        iphoneMatcher();
        ipadMatcher();
        androidMatcher();
        appleMailDesktopMatcher();
        outlookDesktopMatcher();
        ibmNotesDesktopMatcher();
        thunderbirdDesktopMatcher();
        outlookAndroidMatcher();
        return this;
    }

    /**
     * Prefer the HTML version of mail message, if present, using the {@link HtmlToTextConverter} to convert to text
     * if required.
     * <p>
     * Be aware that using this step, you might want atleast 1 matcher (probably custom matcher) to handle the potential
     * case that the text and html parts of an email are different, and therefore can not be aligned. In this case,
     * the stripper that runs on the converted content can come in handy to pick up this case and still strip away
     * some of the converted message.
     * <p>
     * This is because the Html cleaner will work on the text part first, and then try to align what is left with the
     * html text of the message. If the text contents are different, this will not be able to work, and instead will
     * convert the entire message.
     *
     * @return this builder
     */
    public EmailReplyCleanerBuilder preferHtml() {
        return this.preferHtml(HTML_CONVERTER);
    }

    /**
     * Prefer the HTML version of mail message, if present, using the supplied {@link HtmlConverter} to convert to text
     * if required
     * <p>
     * Be aware that using this step, you might want atleast 1 matcher (probably custom matcher) to handle the potential
     * case that the text and html parts of an email are different, and therefore can not be aligned. In this case,
     * the stripper that runs on the converted content can come in handy to pick up this case and still strip away
     * some of the converted message.
     * <p>
     * This is because the Html cleaner will work on the text part first, and then try to align what is left with the
     * html text of the message. If the text contents are different, this will not be able to work, and instead will
     * convert the entire message.
     *
     * @return this builder
     */
    public EmailReplyCleanerBuilder preferHtml(final HtmlConverter htmlConverter) {
        this.htmlConverter = option(htmlConverter);
        return this;
    }

    /**
     * Get a new instance of {@link com.atlassian.pocketknife.api.emailreply.EmailReplyCleaner} with configured matchers
     *
     * @return
     */
    public EmailReplyCleaner build() {
        if (matchers.isEmpty()) {
            defaultMatchers();
        }

        // matchers is a list, so they will get executed in order they are added, meaning someone can add a custom matcher
        // first if they would like that
        return this.htmlConverter.fold(
                () -> new DefaultEmailReplyCleaner(newArrayList(matchers)),
                (converter) -> new HtmlEmailReplyCleaner(converter, newArrayList(matchers))
        );
    }

    @VisibleForTesting
    Set<QuotedEmailMatcher> getMatchers() {
        return matchers;
    }
}
