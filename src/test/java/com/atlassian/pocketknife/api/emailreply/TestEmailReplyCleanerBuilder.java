package com.atlassian.pocketknife.api.emailreply;

import com.atlassian.mail.converters.basic.HtmlToTextConverter;
import com.atlassian.mail.converters.wiki.HtmlToWikiTextConverter;
import com.atlassian.pocketknife.internal.emailreply.DefaultEmailReplyCleaner;
import com.atlassian.pocketknife.internal.emailreply.HtmlEmailReplyCleaner;
import com.atlassian.pocketknife.internal.emailreply.matcher.GmailReplyMatcher;
import com.atlassian.pocketknife.internal.emailreply.matcher.IPhoneReplyMatcher;
import com.atlassian.pocketknife.internal.emailreply.matcher.YahooReplyMatcher;
import com.atlassian.pocketknife.spi.emailreply.matcher.QuotedEmailMatcher;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Sets;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.Set;

import static com.google.common.collect.Sets.newLinkedHashSet;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class TestEmailReplyCleanerBuilder {
    private EmailReplyCleanerBuilder builder;

    @Before
    public void setUp() {
        builder = new EmailReplyCleanerBuilder();
    }

    @Test
    public void testMatchersAreAddedInOrder() {
        final QuotedEmailMatcher mock = mock(QuotedEmailMatcher.class);

        builder.gmailMatcher();
        builder.yahooWebMatcher();
        builder.customMatchers(singletonList(mock));
        builder.iphoneMatcher();

        final Set<QuotedEmailMatcher> expectedMatchers = newLinkedHashSet(ImmutableList.of(new GmailReplyMatcher(), new YahooReplyMatcher(), mock, new IPhoneReplyMatcher()));
        assertThat(
                "matchers are recorded in order",
                builder.getMatchers(),
                equalTo(expectedMatchers));
    }

    @Test
    public void testNoDuplicateMatcherDespiteMultipleCall() {
        builder.gmailMatcher();
        builder.gmailMatcher();

        final Set<QuotedEmailMatcher> expectedMatchers = Sets.<QuotedEmailMatcher>newHashSet(new GmailReplyMatcher());
        assertThat(
                "must contain only 1 instance of Gmail matcher",
                builder.getMatchers(),
                equalTo(expectedMatchers));
    }

    @Test
    public void testAddNullCustomMatchers() {
        builder.customMatchers(null);
        assertTrue("matcher list must be empty", builder.getMatchers().isEmpty());
    }

    @Test
    public void testDefaultMatchersIsAlwaysCalled() {
        builder = Mockito.spy(builder);
        builder.build();

        verify(builder, Mockito.times(1)).defaultMatchers();
    }

    @Test
    public void testDefaultCleaner() {
        final EmailReplyCleaner cleaner = builder.build();

        assertThat("DefaultEmailReplyCleaner is created", cleaner instanceof DefaultEmailReplyCleaner, is(true));
    }

    @Test
    public void testPreferHtml() {
        final EmailReplyCleaner cleaner = builder.preferHtml().build();

        assertThat("HtmlEmailReplyCleaner is created", cleaner, instanceOf(HtmlEmailReplyCleaner.class));
        assertThat("using HtmlToTextConverter", ((HtmlEmailReplyCleaner) cleaner).getHtmlConverter(), instanceOf(HtmlToTextConverter.class));
    }

    @Test
    public void testPreferCustomHtml() {
        final EmailReplyCleaner cleaner = builder.preferHtml(new HtmlToWikiTextConverter(emptyList())).build();

        assertThat("HtmlEmailReplyCleaner is created", cleaner, instanceOf(HtmlEmailReplyCleaner.class));
        assertThat("using HtmlToWikiTextConverter", ((HtmlEmailReplyCleaner) cleaner).getHtmlConverter(), instanceOf(HtmlToWikiTextConverter.class));
    }
}
