package com.atlassian.pocketknife.api.emailreply;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestEmailCleanerResult {

    @Test
    public void testReturnBody() {
        final EmailReplyCleaner.EmailCleanerResult emailCleanerResult = new EmailReplyCleaner.EmailCleanerResult("body", "body original");

        assertEquals("body", emailCleanerResult.getBody());
        assertEquals("body", emailCleanerResult.getRawBody());
        assertEquals("body original", emailCleanerResult.getOriginalBody());
    }

    @Test
    public void testBlankBodyReturnOriginal() {
        final EmailReplyCleaner.EmailCleanerResult emailCleanerResult = new EmailReplyCleaner.EmailCleanerResult(" ", "original");

        assertEquals("original", emailCleanerResult.getBody());
        assertEquals(" ", emailCleanerResult.getRawBody());
        assertEquals("original", emailCleanerResult.getOriginalBody());
    }

    @Test(expected = NullPointerException.class)
    public void testBodyIsNonnull() {
        new EmailReplyCleaner.EmailCleanerResult(null, "body original");
    }

    @Test(expected = NullPointerException.class)
    public void testOriginalBodyIsNonnull() {
        new EmailReplyCleaner.EmailCleanerResult("body", null);
    }

}
