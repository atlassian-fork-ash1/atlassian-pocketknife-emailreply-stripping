package com.atlassian.pocketknife.internal.emailreply;

import com.atlassian.pocketknife.api.emailreply.EmailReplyCleaner;
import com.atlassian.pocketknife.api.emailreply.EmailReplyCleanerBuilder;
import org.junit.Test;

public class TestIPhoneReplyMatcher
{
    private final EmailReplyCleaner iphoneCleaner = new EmailReplyCleanerBuilder().iphoneMatcher().build();

    @Test
    public void testIphoneEnglish1LineText() throws Exception
    {
        EmailTestUtils.validateEmailStripping(iphoneCleaner, EmailCorpus.GENERAL_1_LINE_TEXT);
    }

    @Test
    public void testIphoneEmptyBody() throws Exception
    {
        EmailTestUtils.validateEmailStripping(iphoneCleaner, EmailCorpus.GENERAL_EMPTY_BODY);
    }

    @Test
    public void testIphone4LineText() throws Exception
    {
        EmailTestUtils.validateEmailStripping(iphoneCleaner, EmailCorpus.GENERAL_4_LINE_TEXT);
    }

    @Test
    public void testIphone5LineText() throws Exception
    {
        EmailTestUtils.validateEmailStripping(iphoneCleaner, EmailCorpus.GENERAL_5_LINE_TEXT);
    }

    @Test
    public void testIphoneReplyEnglishText() throws Exception
    {
        EmailTestUtils.validateEmailStripping(iphoneCleaner, EmailCorpus.IPHONE_REPLY_ENGLISH_TEXT);
    }

    @Test
    public void testIphoneReplyEnglishEmptyText() throws Exception
    {
        EmailTestUtils.validateEmailStripping(iphoneCleaner, EmailCorpus.IPHONE_REPLY_ENGLISH_EMPTY_TEXT);
    }

    @Test
    public void testIphoneReplyEnglishQuotedPatternText() throws Exception
    {
        EmailTestUtils.validateEmailStripping(iphoneCleaner, EmailCorpus.IPHONE_REPLY_ENGLISH_QUOTED_PATTERN_TEXT);
    }

    @Test
    public void testIphoneForwardEnglishText() throws Exception
    {
        EmailTestUtils.validateEmailStripping(iphoneCleaner, EmailCorpus.IPHONE_FORWARD_ENGLISH_TEXT);
    }

    @Test
    public void testIphoneForwardEnglishEmptyText() throws Exception
    {
        EmailTestUtils.validateEmailStripping(iphoneCleaner, EmailCorpus.IPHONE_FORWARD_ENGLISH_EMPTY_TEXT);
    }

    @Test
    public void testIphoneReplyFrenchText() throws Exception
    {
        EmailTestUtils.validateEmailStripping(iphoneCleaner, EmailCorpus.IPHONE_REPLY_FRENCH_TEXT);
    }

    @Test
    public void testIphoneReplyFrenchEmptyText() throws Exception
    {
        EmailTestUtils.validateEmailStripping(iphoneCleaner, EmailCorpus.IPHONE_REPLY_FRENCH_EMPTY_TEXT);
    }

    @Test
    public void testIphoneReplyFrenchQuotedPatternText() throws Exception
    {
        EmailTestUtils.validateEmailStripping(iphoneCleaner, EmailCorpus.IPHONE_REPLY_FRENCH_QUOTED_PATTERN_TEXT);
    }

    @Test
    public void testIphoneForwardFrenchText() throws Exception
    {
        EmailTestUtils.validateEmailStripping(iphoneCleaner, EmailCorpus.IPHONE_FORWARD_FRENCH_TEXT);
    }

    @Test
    public void testIphoneForwardFrenchEmptyText() throws Exception
    {
        EmailTestUtils.validateEmailStripping(iphoneCleaner, EmailCorpus.IPHONE_FORWARD_FRENCH_EMPTY_TEXT);
    }

    @Test
    public void testIphoneReplyGermanText() throws Exception
    {
        EmailTestUtils.validateEmailStripping(iphoneCleaner, EmailCorpus.IPHONE_REPLY_GERMAN_TEXT);
    }

    @Test
    public void testIphoneReplyGermanEmptyText() throws Exception
    {
        EmailTestUtils.validateEmailStripping(iphoneCleaner, EmailCorpus.IPHONE_REPLY_GERMAN_EMPTY_TEXT);
    }

    @Test
    public void testIphoneReplyGermanQuotedPatternText() throws Exception
    {
        EmailTestUtils.validateEmailStripping(iphoneCleaner, EmailCorpus.IPHONE_REPLY_GERMAN_QUOTED_PATTERN_TEXT);
    }

    @Test
    public void testIphoneForwardGermanText() throws Exception
    {
        EmailTestUtils.validateEmailStripping(iphoneCleaner, EmailCorpus.IPHONE_FORWARD_GERMAN_TEXT);
    }

    @Test
    public void testIphoneForwardGermanEmptyText() throws Exception
    {
        EmailTestUtils.validateEmailStripping(iphoneCleaner, EmailCorpus.IPHONE_FORWARD_GERMAN_EMPTY_TEXT);
    }

    @Test
    public void testIphoneReplyJapText() throws Exception
    {
        EmailTestUtils.validateEmailStripping(iphoneCleaner, EmailCorpus.IPHONE_REPLY_JAP_TEXT);
    }

    @Test
    public void testIphoneReplyJapEmptyText() throws Exception
    {
        EmailTestUtils.validateEmailStripping(iphoneCleaner, EmailCorpus.IPHONE_REPLY_JAP_EMPTY_TEXT);
    }

    @Test
    public void testIphoneReplyJapQuotedPatternText() throws Exception
    {
        EmailTestUtils.validateEmailStripping(iphoneCleaner, EmailCorpus.IPHONE_REPLY_JAP_QUOTED_PATTERN_TEXT);
    }

    @Test
    public void testIphoneForwardJapText() throws Exception
    {
        EmailTestUtils.validateEmailStripping(iphoneCleaner, EmailCorpus.IPHONE_FORWARD_JAP_TEXT);
    }

    @Test
    public void testIphoneForwardJapEmptyText() throws Exception
    {
        EmailTestUtils.validateEmailStripping(iphoneCleaner, EmailCorpus.IPHONE_FORWARD_JAP_EMPTY_TEXT);
    }

    @Test
    public void testIphoneReplySpanishText() throws Exception
    {
        EmailTestUtils.validateEmailStripping(iphoneCleaner, EmailCorpus.IPHONE_REPLY_SPANISH_TEXT);
    }

    @Test
    public void testIphoneReplySpanishEmptyText() throws Exception
    {
        EmailTestUtils.validateEmailStripping(iphoneCleaner, EmailCorpus.IPHONE_REPLY_SPANISH_EMPTY_TEXT);
    }

    @Test
    public void testIphoneReplySpanishQuotedPatternText() throws Exception
    {
        EmailTestUtils.validateEmailStripping(iphoneCleaner, EmailCorpus.IPHONE_REPLY_SPANISH_QUOTED_PATTERN_TEXT);
    }

    @Test
    public void testIphoneForwardSpanishText() throws Exception
    {
        EmailTestUtils.validateEmailStripping(iphoneCleaner, EmailCorpus.IPHONE_FORWARD_SPANISH_TEXT);
    }

    @Test
    public void testIphoneForwardSpanishEmptyText() throws Exception
    {
        EmailTestUtils.validateEmailStripping(iphoneCleaner, EmailCorpus.IPHONE_FORWARD_SPANISH_EMPTY_TEXT);
    }

    @Test
    public void testIphoneReplyWithoutSentFromDeviceBlockText() throws Exception
    {
        EmailTestUtils.validateEmailStripping(iphoneCleaner, EmailCorpus.IPHONE_REPLY_WITHOUT_SENT_FROM_DEVICE_BLOCK_TEXT);
    }

    @Test
    public void testIphoneForwardWithoutSentFromDeviceBlockText() throws Exception
    {
        EmailTestUtils.validateEmailStripping(iphoneCleaner, EmailCorpus.IPHONE_FORWARD_WITHOUT_SENT_FROM_DEVICE_BLOCK_TEXT);
    }
}
