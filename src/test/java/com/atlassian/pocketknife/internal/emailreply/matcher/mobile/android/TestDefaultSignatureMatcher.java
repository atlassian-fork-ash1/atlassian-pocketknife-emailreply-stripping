package com.atlassian.pocketknife.internal.emailreply.matcher.mobile.android;

import com.atlassian.pocketknife.internal.emailreply.DefaultEmailReplyCleaner;
import com.atlassian.pocketknife.internal.emailreply.matcher.RegexList;
import com.atlassian.pocketknife.internal.emailreply.matcher.TestBase;
import com.atlassian.pocketknife.internal.emailreply.matcher.mobile.android.signature.DefaultSignatureMatcher;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Test;

public class TestDefaultSignatureMatcher extends TestBase {

    private final String corpusFolder = getDefaultCorpusFolder() + "/mobile/android/signature";

    private final String ForwardFromHtcEnglishFile = corpusFolder + "/ForwardFromHtcEnglish.eml";
    private final String StrippedForwardFromHtcEnglishFile = corpusFolder + "/stripped/ForwardFromHtcEnglish.eml";

    private final String ForwardFromSamsungEnglishFile = corpusFolder + "/ForwardFromSamsungEnglish.eml";
    private final String StrippedForwardFromSamsungEnglishFile = corpusFolder + "/stripped/ForwardFromSamsungEnglish.eml";

    private final String ReplyFromHtcEnglishFile = corpusFolder + "/ReplyFromHtcEnglish.eml";
    private final String StrippedReplyFromHtcEnglishFile = corpusFolder + "/stripped/ReplyFromHtcEnglish.eml";

    private final String ReplyFromSamsungnglishFile = corpusFolder + "/ReplyFromSamsungEnglish.eml";
    private final String StrippedReplyFromSamsungEnglish = corpusFolder + "/stripped/ReplyFromSamsungEnglish.eml";

    private final String TextOnlyHtcNoAttributionWithSignatureFile = corpusFolder + "/TextOnlyHtcNoAttributionWithSignature.eml";
    private final String StrippedTextOnlyHtcNoAttributionWithSignatureFile = corpusFolder + "/stripped/TextOnlyHtcNoAttributionWithSignature.eml";

    private final String TextOnlyHtcQuoteWithAttributionFile = corpusFolder + "/TextOnlyHtcQuoteWithAttribution.eml";
    private final String StrippedTextOnlyHtcQuoteWithAttributionFile = corpusFolder + "/stripped/TextOnlyHtcQuoteWithAttribution.eml";

    @Before
    public void setUp() throws Exception {
        matcher = new DefaultSignatureMatcher();
        cleaner = new DefaultEmailReplyCleaner(Lists.newArrayList(matcher));
    }

    @Test
    public void testPatterns() throws Exception {
        String EnglishLine = " ---------- Forwarded message ----------";
        String FrenchLine = " ---------- Message transféré ----------";
        String GermanLine = "---------- Weitergeleitete Nachricht ----------";
        String JapaneseLine = " ---------- 転送メッセージ ----------";
        String SpanishLine = "---------- Mensaje reenviado ----------";
        String InlinedEnglishLine = "From my phone. ---------- Forwarded message ----------";

        patternMatchedTest(RegexList.FORWARDED_MESSAGE_PATTERN, EnglishLine);
        patternMatchedTest(RegexList.INLINED_FORWARDED_MESSAGE_PATTERN, EnglishLine);
        patternMatchedTest(RegexList.FORWARDED_MESSAGE_PATTERN, FrenchLine);
        patternMatchedTest(RegexList.INLINED_FORWARDED_MESSAGE_PATTERN, FrenchLine);
        patternMatchedTest(RegexList.FORWARDED_MESSAGE_PATTERN, GermanLine);
        patternMatchedTest(RegexList.INLINED_FORWARDED_MESSAGE_PATTERN, GermanLine);
        patternMatchedTest(RegexList.FORWARDED_MESSAGE_PATTERN, JapaneseLine);
        patternMatchedTest(RegexList.INLINED_FORWARDED_MESSAGE_PATTERN, JapaneseLine);
        patternMatchedTest(RegexList.FORWARDED_MESSAGE_PATTERN, SpanishLine);
        patternMatchedTest(RegexList.INLINED_FORWARDED_MESSAGE_PATTERN, SpanishLine);
        patternUnMatchedTest(RegexList.FORWARDED_MESSAGE_PATTERN, InlinedEnglishLine);
        patternMatchedTest(RegexList.INLINED_FORWARDED_MESSAGE_PATTERN, InlinedEnglishLine);

        EnglishLine = " ---------- Original message ----------";
        FrenchLine = " ---------- message d'origine ----------";
        GermanLine = " ---------- Ursprüngliche Nachricht ----------";
        JapaneseLine = " ---------- オリジナルメッセージ ----------";
        SpanishLine = "---------- Mensaje original ----------";
        InlinedEnglishLine = "From my phone.---------- Original message ----------";

        patternMatchedTest(RegexList.ORIGINAL_MESSAGE_PATTERN, EnglishLine);
        patternMatchedTest(RegexList.INLINED_ORIGINAL_MESSAGE_PATTERN, EnglishLine);
        patternMatchedTest(RegexList.ORIGINAL_MESSAGE_PATTERN, FrenchLine);
        patternMatchedTest(RegexList.INLINED_ORIGINAL_MESSAGE_PATTERN, FrenchLine);
        patternMatchedTest(RegexList.ORIGINAL_MESSAGE_PATTERN, GermanLine);
        patternMatchedTest(RegexList.INLINED_ORIGINAL_MESSAGE_PATTERN, GermanLine);
        patternMatchedTest(RegexList.ORIGINAL_MESSAGE_PATTERN, JapaneseLine);
        patternMatchedTest(RegexList.INLINED_ORIGINAL_MESSAGE_PATTERN, JapaneseLine);
        patternMatchedTest(RegexList.ORIGINAL_MESSAGE_PATTERN, SpanishLine);
        patternMatchedTest(RegexList.INLINED_ORIGINAL_MESSAGE_PATTERN, SpanishLine);
        patternUnMatchedTest(RegexList.ORIGINAL_MESSAGE_PATTERN, InlinedEnglishLine);
        patternMatchedTest(RegexList.INLINED_ORIGINAL_MESSAGE_PATTERN, InlinedEnglishLine);

        EnglishLine = " ---------- Reply message ----------";
        FrenchLine = " ---------- Message de réponse ----------";
        GermanLine = "---------- Antwort senden ----------";
        JapaneseLine = " ---------- メッセージを返信 ----------";
        SpanishLine = "---------- Mensaje de respuesta ----------";
        InlinedEnglishLine = "From my phone. ---------- Reply message ----------";

        patternMatchedTest(RegexList.REPLY_MESSAGE_PATTERN, EnglishLine);
        patternMatchedTest(RegexList.INLINED_REPLY_MESSAGE_PATTERN, EnglishLine);
        patternMatchedTest(RegexList.REPLY_MESSAGE_PATTERN, FrenchLine);
        patternMatchedTest(RegexList.INLINED_REPLY_MESSAGE_PATTERN, FrenchLine);
        patternMatchedTest(RegexList.REPLY_MESSAGE_PATTERN, GermanLine);
        patternMatchedTest(RegexList.INLINED_REPLY_MESSAGE_PATTERN, GermanLine);
        patternMatchedTest(RegexList.REPLY_MESSAGE_PATTERN, JapaneseLine);
        patternMatchedTest(RegexList.INLINED_REPLY_MESSAGE_PATTERN, JapaneseLine);
        patternMatchedTest(RegexList.REPLY_MESSAGE_PATTERN, SpanishLine);
        patternMatchedTest(RegexList.INLINED_REPLY_MESSAGE_PATTERN, SpanishLine);
        patternUnMatchedTest(RegexList.REPLY_MESSAGE_PATTERN, InlinedEnglishLine);
        patternMatchedTest(RegexList.INLINED_REPLY_MESSAGE_PATTERN, InlinedEnglishLine);
    }

    @Test
    public void testSignaturePatterns() throws Exception {
        String EnglishLine = "Sent from my Samsung Galaxy smartphone.";
        String EnglishLine2 = "Sent from my Samsung Galaxy smartphone using the app";
        String FrenchLine = " Envoyé de mon HTC";
        String GermanLine = "Von meinem Nexus 5 gesendet";
        String GermanLine2 = "Von meinem Nexus 5 gesendet using the app";
        String JapaneseLine = "  HTCから送信 ";
        String SpanishLine = "Enviado desde mi Samsung Note";

        patternMatchedTest(RegexList.DEFAULT_ANDROID_SIGNATURE_PATTERN, EnglishLine);
        patternMatchedTest(RegexList.DEFAULT_ANDROID_SIGNATURE_PATTERN, EnglishLine2);
        patternMatchedTest(RegexList.DEFAULT_ANDROID_SIGNATURE_PATTERN, FrenchLine);
        patternMatchedTest(RegexList.DEFAULT_ANDROID_SIGNATURE_PATTERN, GermanLine);
        patternMatchedTest(RegexList.DEFAULT_ANDROID_SIGNATURE_PATTERN, GermanLine2);
        patternMatchedTest(RegexList.DEFAULT_ANDROID_SIGNATURE_PATTERN, JapaneseLine);
        patternMatchedTest(RegexList.DEFAULT_ANDROID_SIGNATURE_PATTERN, SpanishLine);

        patternUnMatchedTest(RegexList.DEFAULT_ANDROID_SIGNATURE_PATTERN, "Sent from mySamsung Galaxy smartphone.");
        patternUnMatchedTest(RegexList.DEFAULT_ANDROID_SIGNATURE_PATTERN, " Envoyéde mon HTC");
        patternUnMatchedTest(RegexList.DEFAULT_ANDROID_SIGNATURE_PATTERN, "Von meinem Nexus 5gesendet");
        patternUnMatchedTest(RegexList.DEFAULT_ANDROID_SIGNATURE_PATTERN, "から送信 ");
        patternUnMatchedTest(RegexList.DEFAULT_ANDROID_SIGNATURE_PATTERN, "Enviado desde mi    ");
    }

    @Test
    public void testForwardFromHtcEnglishMatch() throws Exception {
        strippingTest(StrippedForwardFromHtcEnglishFile, ForwardFromHtcEnglishFile);
    }

    @Test
    public void testForwardFromSamsungEnglishMatch() throws Exception {
        strippingTest(StrippedForwardFromSamsungEnglishFile, ForwardFromSamsungEnglishFile);
    }

    @Test
    public void testReplyFromHtcEnglishMatch() throws Exception {
        strippingTest(StrippedReplyFromHtcEnglishFile, ReplyFromHtcEnglishFile);
    }

    @Test
    public void testReplyFromSamsungEnglishMatch() throws Exception {
        strippingTest(StrippedReplyFromSamsungEnglish, ReplyFromSamsungnglishFile);
    }

    @Test
    public void testTextOnlyHtcNoAttributionWIthSignatureFileMatch() throws Exception {
        strippingTest(StrippedTextOnlyHtcNoAttributionWithSignatureFile, TextOnlyHtcNoAttributionWithSignatureFile);
    }

    @Test
    public void testTextOnlyHtcQuoteWithAttributionFileFileMatch() throws Exception {
        strippingTest(StrippedTextOnlyHtcQuoteWithAttributionFile, TextOnlyHtcQuoteWithAttributionFile);
    }

}
