package com.atlassian.pocketknife.internal.emailreply;

import com.atlassian.pocketknife.api.emailreply.EmailReplyCleaner;
import com.atlassian.pocketknife.api.emailreply.EmailReplyCleanerBuilder;
import org.junit.Test;

public class TestYahooReplyCleaner
{
    private final EmailReplyCleaner yahooCleaner = new EmailReplyCleanerBuilder().yahooWebMatcher().build();

    @Test
    public void testYahooEnglish1LineText() throws Exception
    {
        EmailTestUtils.validateEmailStripping(yahooCleaner, EmailCorpus.GENERAL_1_LINE_TEXT);
    }

    @Test
    public void testYahooEmptyBody() throws Exception
    {
        EmailTestUtils.validateEmailStripping(yahooCleaner, EmailCorpus.GENERAL_EMPTY_BODY);
    }

    @Test
    public void testYahoo4LineText() throws Exception
    {
        EmailTestUtils.validateEmailStripping(yahooCleaner, EmailCorpus.GENERAL_4_LINE_TEXT);
    }

    @Test
    public void testYahoo5LineText() throws Exception
    {
        EmailTestUtils.validateEmailStripping(yahooCleaner, EmailCorpus.GENERAL_5_LINE_TEXT);
    }

    @Test
    public void testYahooReplyEnglishText() throws Exception
    {
        EmailTestUtils.validateEmailStripping(yahooCleaner, EmailCorpus.YAHOO_REPLY_ENGLISH_TEXT);
    }

    @Test
    public void testYahooReplyEnglishEmptyText() throws Exception
    {
        EmailTestUtils.validateEmailStripping(yahooCleaner, EmailCorpus.YAHOO_REPLY_ENGLISH_EMPTY_TEXT);
    }

    @Test
    public void testYahooForwardEnglishText() throws Exception
    {
        EmailTestUtils.validateEmailStripping(yahooCleaner, EmailCorpus.YAHOO_FORWARD_ENGLISH_TEXT);
    }

    @Test
    public void testYahooForwardEnglishEmptyText() throws Exception
    {
        EmailTestUtils.validateEmailStripping(yahooCleaner, EmailCorpus.YAHOO_FORWARD_ENGLISH_EMPTY_TEXT);
    }

    @Test
    public void testYahooReplyEnglishQuotedPatternText() throws Exception
    {
        EmailTestUtils.validateEmailStripping(yahooCleaner, EmailCorpus.YAHOO_REPLY_ENGLISH_QUOTED_PATTERN_TEXT);
    }

    @Test
    public void testYahooReplyJapText() throws Exception
    {
        EmailTestUtils.validateEmailStripping(yahooCleaner, EmailCorpus.YAHOO_REPLY_JAP_TEXT);
    }

    @Test
    public void testYahooReplyJapEmptyText() throws Exception
    {
        EmailTestUtils.validateEmailStripping(yahooCleaner, EmailCorpus.YAHOO_REPLY_JAP_EMPTY_TEXT);
    }

    @Test
    public void testYahooForwardJapText() throws Exception
    {
        EmailTestUtils.validateEmailStripping(yahooCleaner, EmailCorpus.YAHOO_FORWARD_JAP_TEXT);
    }

    @Test
    public void testYahooForwardJapEmptyText() throws Exception
    {
        EmailTestUtils.validateEmailStripping(yahooCleaner, EmailCorpus.YAHOO_FORWARD_JAP_EMPTY_TEXT);
    }

    @Test
    public void testYahooReplyGermanText() throws Exception
    {
        EmailTestUtils.validateEmailStripping(yahooCleaner, EmailCorpus.YAHOO_REPLY_GERMAN_TEXT);
    }

    @Test
    public void testYahooReplyGermanEmtpyText() throws Exception
    {
        EmailTestUtils.validateEmailStripping(yahooCleaner, EmailCorpus.YAHOO_REPLY_GERMAN_EMPTY_TEXT);
    }

    @Test
    public void testYahooReplyGermanQuotedPatternText() throws Exception
    {
        EmailTestUtils.validateEmailStripping(yahooCleaner, EmailCorpus.YAHOO_REPLY_GERMAN_QUOTED_PATTERN_TEXT);
    }

    @Test
    public void testYahooForwardGermanText() throws Exception
    {
        EmailTestUtils.validateEmailStripping(yahooCleaner, EmailCorpus.YAHOO_FORWARD_GERMAN_TEXT);
    }

    @Test
    public void testYahooForwardGermanEmptyText() throws Exception
    {
        EmailTestUtils.validateEmailStripping(yahooCleaner, EmailCorpus.YAHOO_FORWARD_GERMAN_EMPTY_TEXT);
    }

    @Test
    public void testYahooReplyFrenchText() throws Exception
    {
        EmailTestUtils.validateEmailStripping(yahooCleaner, EmailCorpus.YAHOO_REPLY_FRENCH_TEXT);
    }

    @Test
    public void testYahooReplyFrenchEmptyText() throws Exception
    {
        EmailTestUtils.validateEmailStripping(yahooCleaner, EmailCorpus.YAHOO_REPLY_FRENCH_EMPTY_TEXT);
    }

    @Test
    public void testYahooReplyFrenchQuotedPatternText() throws Exception
    {
        EmailTestUtils.validateEmailStripping(yahooCleaner, EmailCorpus.YAHOO_REPLY_FRENCH_QUOTED_PATTERN_TEXT);
    }

    @Test
    public void testYahooForwardFrenchText() throws Exception
    {
        EmailTestUtils.validateEmailStripping(yahooCleaner, EmailCorpus.YAHOO_FORWARD_FRENCH_TEXT);
    }

    @Test
    public void testYahooForwardFrenchEmptyText() throws Exception
    {
        EmailTestUtils.validateEmailStripping(yahooCleaner, EmailCorpus.YAHOO_FORWARD_FRENCH_EMPTY_TEXT);
    }

    @Test
    public void testYahooReplySpanishText() throws Exception
    {
        EmailTestUtils.validateEmailStripping(yahooCleaner, EmailCorpus.YAHOO_REPLY_SPANISH_TEXT);
    }

    @Test
    public void testYahooReplySpanishEmptyText() throws Exception
    {
        EmailTestUtils.validateEmailStripping(yahooCleaner, EmailCorpus.YAHOO_REPLY_SPANISH_EMPTY_TEXT);
    }

    @Test
    public void testYahooReplySpanishQuotedPatternText() throws Exception
    {
        EmailTestUtils.validateEmailStripping(yahooCleaner, EmailCorpus.YAHOO_REPLY_SPANISH_QUOTED_PATTERN_TEXT);
    }

    @Test
    public void testYahooForwardSpanishText() throws Exception
    {
        EmailTestUtils.validateEmailStripping(yahooCleaner, EmailCorpus.YAHOO_FORWARD_SPANISH_TEXT);
    }

    @Test
    public void testYahooForwardSpanishEmptyText() throws Exception
    {
        EmailTestUtils.validateEmailStripping(yahooCleaner, EmailCorpus.YAHOO_FORWARD_SPANISH_EMPTY_TEXT);
    }


    @Test
    public void testYahooReplyStyling() throws Exception
    {
        EmailTestUtils.validateEmailStripping(yahooCleaner, EmailCorpus.YAHOO_REPLY_STYLING);
    }


    @Test
    public void testYahooReplyStylingLists() throws Exception
    {
        EmailTestUtils.validateEmailStripping(yahooCleaner, EmailCorpus.YAHOO_REPLY_STYLING_LISTS);
    }
}
