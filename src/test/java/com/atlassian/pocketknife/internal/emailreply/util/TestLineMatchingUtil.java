package com.atlassian.pocketknife.internal.emailreply.util;

import org.junit.Test;

import java.util.regex.Pattern;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TestLineMatchingUtil
{
    @Test
    public void testIsBlankOrStartWithQuoteLineIndicator()
    {
        assertTrue(LineMatchingUtil.isBlankOrStartWithQuoteLineIndicator(null));
        assertTrue(LineMatchingUtil.isBlankOrStartWithQuoteLineIndicator(""));
        assertTrue(LineMatchingUtil.isBlankOrStartWithQuoteLineIndicator(" "));
        assertTrue(LineMatchingUtil.isBlankOrStartWithQuoteLineIndicator("\n"));

        assertTrue(LineMatchingUtil.isBlankOrStartWithQuoteLineIndicator(">"));
        assertTrue(LineMatchingUtil.isBlankOrStartWithQuoteLineIndicator("> some texts"));

        assertTrue(LineMatchingUtil.isBlankOrStartWithQuoteLineIndicator("|"));
        assertTrue(LineMatchingUtil.isBlankOrStartWithQuoteLineIndicator("| some texts"));

        assertFalse(LineMatchingUtil.isBlankOrStartWithQuoteLineIndicator("some texts"));
    }

    @Test
    public void testIsStartWithQuoteLineIndicator()
    {
        assertFalse(LineMatchingUtil.isStartWithQuoteLineIndicator(null));
        assertFalse(LineMatchingUtil.isStartWithQuoteLineIndicator(""));
        assertFalse(LineMatchingUtil.isStartWithQuoteLineIndicator("some texts"));

        assertTrue(LineMatchingUtil.isStartWithQuoteLineIndicator(">"));
        assertTrue(LineMatchingUtil.isStartWithQuoteLineIndicator("> some texts"));

        assertTrue(LineMatchingUtil.isStartWithQuoteLineIndicator("|"));
        assertTrue(LineMatchingUtil.isStartWithQuoteLineIndicator("| some texts"));
    }

    @Test
    public void testIsBlankOrMatchPattern()
    {
        final Pattern pattern = Pattern.compile("some texts");

        assertTrue(LineMatchingUtil.isBlankOrMatchPattern(null, pattern));
        assertTrue(LineMatchingUtil.isBlankOrMatchPattern("", pattern));
        assertTrue(LineMatchingUtil.isBlankOrMatchPattern(" ", pattern));
        assertTrue(LineMatchingUtil.isBlankOrMatchPattern("\n", pattern));
        assertTrue(LineMatchingUtil.isBlankOrMatchPattern("some texts here and there", pattern));

        assertFalse(LineMatchingUtil.isBlankOrMatchPattern("nothing here", pattern));
    }

    @Test
    public void testIsBlankOrStartWithQuoteLineIndicatorOrMatchPattern()
    {
        final Pattern pattern = Pattern.compile("some text");

        assertTrue(LineMatchingUtil.isBlankOrStartWithQuoteLineIndicatorOrMatchPattern(null, pattern));
        assertTrue(LineMatchingUtil.isBlankOrStartWithQuoteLineIndicatorOrMatchPattern("", pattern));
        assertTrue(LineMatchingUtil.isBlankOrStartWithQuoteLineIndicatorOrMatchPattern(" ", pattern));
        assertTrue(LineMatchingUtil.isBlankOrStartWithQuoteLineIndicatorOrMatchPattern("\n", pattern));

        assertTrue(LineMatchingUtil.isBlankOrStartWithQuoteLineIndicatorOrMatchPattern(">", pattern));
        assertTrue(LineMatchingUtil.isBlankOrStartWithQuoteLineIndicatorOrMatchPattern("> some texts", pattern));
        assertTrue(LineMatchingUtil.isBlankOrStartWithQuoteLineIndicatorOrMatchPattern("|", pattern));
        assertTrue(LineMatchingUtil.isBlankOrStartWithQuoteLineIndicatorOrMatchPattern("| some texts", pattern));

        assertTrue(LineMatchingUtil.isBlankOrStartWithQuoteLineIndicatorOrMatchPattern("some texts here and there", pattern));
        assertFalse(LineMatchingUtil.isBlankOrStartWithQuoteLineIndicatorOrMatchPattern("nothing here", pattern));
    }
}
