package com.atlassian.pocketknife.internal.emailreply.matcher.desktop.ibmnotes.v9;

import com.atlassian.pocketknife.internal.emailreply.DefaultEmailReplyCleaner;
import com.atlassian.pocketknife.internal.emailreply.matcher.TestBase;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Test;

public class TestMatcherPatternMatching extends TestBase
{
    @Before
    public void setUp() throws Exception
    {
        matcher = new Matcher();
        cleaner = new DefaultEmailReplyCleaner(Lists.newArrayList(matcher));
    }

    @Test
    public void testPatterns() throws Exception
    {
        String text = "To:\t\tchuongnn.atlassian.cus1@gmail.com\n" +
                "\n" +
                "cc:\t\t\n" +
                "\n" +
                "Subject:\t\tRe: IBM Notes 9 hello English empire 1\n";

        patternMatchedTest(((Matcher) matcher).PATTERN, text);
    }
}