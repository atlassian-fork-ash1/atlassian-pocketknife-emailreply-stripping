package com.atlassian.pocketknife.internal.emailreply;

import com.atlassian.mail.converters.basic.HtmlToTextConverter;
import com.atlassian.mail.MailUtils;
import com.atlassian.pocketknife.internal.emailreply.matcher.GmailReplyMatcher;
import com.atlassian.pocketknife.internal.emailreply.matcher.IPhoneReplyMatcher;
import com.atlassian.pocketknife.internal.emailreply.matcher.TestBase;
import com.atlassian.pocketknife.internal.emailreply.matcher.YahooReplyMatcher;
import com.atlassian.pocketknife.internal.emailreply.matcher.desktop.thunderbird.v31.ForwardMatcher;
import com.atlassian.pocketknife.spi.emailreply.matcher.QuotedEmailMatcher;
import com.google.common.collect.ImmutableList;
import org.junit.Test;

import javax.mail.Message;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;

public class TestHtmlEmailReplyCleaner extends TestBase {

    @Test
    public void testCleanLinkCitations() throws Exception {
        final Message mimeMessage = createMIMEMessage(getDefaultHtmlFolder() + "/GmailMultipleLinksTextAndHtml.eml");
        final String body = MailUtils.getBody(mimeMessage, new HtmlToTextConverter(), true);

        assertThat(body, is("Email as [1]testing@home.com\n" +
                "My phone is 0400 456 000 or 0400456000 or +61400456000\n" +
                "[2]http://www.adelaidenow.com.au/\n" +
                "Maybe in here as [3]http://www.adelaidenow.com.au/ or there[4]http://www.adelaidenow.com.au now.\n" +
                "This is [5]www.google.com and I'm done\n" +
                "This is not actually me :)\n" +
                "----------------------------------------------------------------------------------------\n" +
                "[1] mailto:testing@home.com\n" +
                "[2] http://www.adelaidenow.com.au/\n" +
                "[3] http://www.adelaidenow.com.au/\n" +
                "[4] http://www.adelaidenow.com.au\n" +
                "[5] http://www.google.com"));

        final String s = HtmlEmailReplyCleaner.cleanupLinkCitations(body);

        assertThat(s, is("Email as testing@home.com\n" +
                "My phone is 0400 456 000 or 0400456000 or +61400456000\n" +
                "http://www.adelaidenow.com.au/\n" +
                "Maybe in here as http://www.adelaidenow.com.au/ or therehttp://www.adelaidenow.com.au now.\n" +
                "This is www.google.com and I'm done\n" +
                "This is not actually me :)"));
    }

    @Test
    public void testNoDuplicateMatcherDespiteMultipleCall() {
        HtmlEmailReplyCleaner htmlEmailReplyCleaner = new HtmlEmailReplyCleaner(new HtmlToTextConverter(), ImmutableList.of(new GmailReplyMatcher(), new GmailReplyMatcher()));

        final List<QuotedEmailMatcher> expectedMatchers;
        expectedMatchers = ImmutableList.of(new GmailReplyMatcher());

        assertThat(
                "must contain only 1 instance of Gmail matcher is in use",
                htmlEmailReplyCleaner.getDefaultEmailReplyCleaner().getMatchers(),
                equalTo(expectedMatchers));
    }

    @Test
    public void testMatchersAreAddedInOrder() {
        final QuotedEmailMatcher mock = mock(QuotedEmailMatcher.class);
        final QuotedEmailMatcher forward = new ForwardMatcher();

        HtmlEmailReplyCleaner htmlEmailReplyCleaner = new HtmlEmailReplyCleaner(new HtmlToTextConverter(), ImmutableList.of(new GmailReplyMatcher(), new YahooReplyMatcher(), mock, new IPhoneReplyMatcher(), new GmailReplyMatcher(), forward));

        final List<QuotedEmailMatcher> expectedMatchers = ImmutableList.of(new GmailReplyMatcher(), new YahooReplyMatcher(), mock, new IPhoneReplyMatcher(), forward);
        assertThat(
                "matchers are recorded in order",
                htmlEmailReplyCleaner.getDefaultEmailReplyCleaner().getMatchers(),
                equalTo(expectedMatchers));
    }


}
