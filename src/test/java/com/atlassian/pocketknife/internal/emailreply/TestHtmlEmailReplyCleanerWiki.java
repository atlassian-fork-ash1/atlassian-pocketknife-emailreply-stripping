package com.atlassian.pocketknife.internal.emailreply;

import com.atlassian.mail.converters.wiki.HtmlToWikiTextConverter;
import com.atlassian.pocketknife.api.emailreply.EmailReplyCleaner;
import com.atlassian.pocketknife.api.emailreply.EmailReplyCleanerBuilder;
import com.atlassian.pocketknife.internal.emailreply.matcher.TestBase;
import com.atlassian.pocketknife.spi.emailreply.matcher.QuotedEmailMatcher;
import org.hamcrest.core.IsInstanceOf;
import org.junit.Before;
import org.junit.Test;

import javax.mail.Message;

import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.apache.commons.lang.StringUtils.startsWith;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.core.IsCollectionContaining.hasItem;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

public class TestHtmlEmailReplyCleanerWiki extends TestBase {

    private EmailReplyCleaner htmlEmailReplyCleaner;

    @Before
    public void setup() {
        this.htmlEmailReplyCleaner = new EmailReplyCleanerBuilder().defaultMatchers().preferHtml(new HtmlToWikiTextConverter(emptyList())).build();
        assertThat(this.htmlEmailReplyCleaner, IsInstanceOf.instanceOf(HtmlEmailReplyCleaner.class));
    }

    @Test
    public void testHtmlOnlyForwardStripping() throws Exception {

        final Message mimeMessage = createMIMEMessage(getDefaultHtmlFolder() + "/HackedHtmlOnlyForward.eml");

        final EmailReplyCleaner.EmailCleanerResult result = this.htmlEmailReplyCleaner.cleanQuotedEmail(mimeMessage);

        assertNotNull(result);
        // for this, we can detect that a forwarded message can be stripped
        assertEquals("HTML ONLY\n" +
                "\n" +
                "\n" +
                "The end", result.getBody());
    }

    @Test
    public void testHtmlOnlyForwardInBlockQuoteStripping() throws Exception {

        final Message mimeMessage = createMIMEMessage(getDefaultHtmlFolder() + "/HackedHtmlOnlyForwardInBlockQuote.eml");

        final EmailReplyCleaner.EmailCleanerResult result = this.htmlEmailReplyCleaner.cleanQuotedEmail(mimeMessage);

        assertNotNull(result);

        assertEquals("Test\n" +
                "\n" +
                "\n" +
                "{quote}Quote this\n" +
                "{quote}", result.getBody());
    }

    @Test
    public void testGmailHackedTextOnlyReplyStripping() throws Exception {

        final Message mimeMessage = createMIMEMessage(getDefaultHtmlFolder() + "/GmailHackedTextOnlyReply.eml");

        final EmailReplyCleaner.EmailCleanerResult result = this.htmlEmailReplyCleaner.cleanQuotedEmail(mimeMessage);

        assertNotNull(result);

        assertEquals("HTML ONLY\n" +
                "\n" +
                "The end", result.getBody());
    }

    @Test
    public void testGmailHackedTextOnlyReplyInBlockQuoteStripping() throws Exception {
        final Message mimeMessage = createMIMEMessage(getDefaultHtmlFolder() + "/GmailHackedTextOnlyReplyInBlockQuote.eml");

        final EmailReplyCleaner.EmailCleanerResult result = this.htmlEmailReplyCleaner.cleanQuotedEmail(mimeMessage);

        assertNotNull(result);

        assertEquals("Test\n" +
                "\n" +
                "Quote this\n" +
                ">", result.getBody());
    }

    @Test
    public void testGmailTableAndExternalImageStripping() throws Exception {
        final Message mimeMessage = createMIMEMessage(getDefaultHtmlFolder() + "/GmailTableAndExternalImage.eml");

        final EmailReplyCleaner defaultCleaner = new EmailReplyCleanerBuilder().build();
        final EmailReplyCleaner.EmailCleanerResult emailCleanerResult = defaultCleaner.cleanQuotedEmail(mimeMessage);

        assertNotNull(emailCleanerResult);

        final EmailReplyCleaner.EmailCleanerResult result = this.htmlEmailReplyCleaner.cleanQuotedEmail(mimeMessage);

        assertNotNull(result);

        assertEquals("Some text\n" +
                "\n" +
                "\n" +
                "Test\n" +
                "\n" +
                "Col Col2 Col3\n" +
                "A B C\n" +
                "D E F\n" +
                "The end", emailCleanerResult.getBody());

        assertEquals("Some text!https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcS3_fwJM1motN06hu3CU4SqagZJRqmOkhv53Q_62jfouXFY3r7g!\n" +
                "\n" +
                "\n" +
                "Test\n" +
                "|Col|Col2|Col3|\n" +
                "|A|B|C|\n" +
                "|D|E|F|\n" +
                "\n" +
                "\n" +
                "The end", result.getBody());
    }

    @Test
    public void testGmailFromOutlook2010HackedTextReplyPartialStripping() throws Exception {
        final Message mimeMessage = createMIMEMessage(getDefaultHtmlFolder() + "/GmailFromOutlook2010HackedTextReply.eml");

        final EmailReplyCleaner.EmailCleanerResult result = this.htmlEmailReplyCleaner.cleanQuotedEmail(mimeMessage);

        assertEquals("{color:#1F497D}This is Outlook 2010{color}\n" +
                "h1. {color:#1F497D}Another sentence{color}{color:#1F497D}{color}\n" +
                "\n" +
                "\n" +
                "{color:#1F497D}{color} \n" +
                "\n" +
                "{color:#1F497D}The e{color}", result.getBody());
    }

    @Test
    public void testGmailHtmlAndTextReplyInBlockQuoteStripping() throws Exception {
        final Message mimeMessage = createMIMEMessage(getDefaultHtmlFolder() + "/GmailHtmlAndTextReplyInBlockQuote.eml");

        final EmailReplyCleaner.EmailCleanerResult result = this.htmlEmailReplyCleaner.cleanQuotedEmail(mimeMessage);

        assertNotNull(result);

        assertThat(result.getBody(), is("Test\n" +
                "\n" +
                "\n" +
                "{quote}Quote this\n" +
                "{quote}"));
        assertThat(result.getRawBody(), is("Test\n" +
                "\n" +
                "\n" +
                "{quote}Quote this\n" +
                "{quote}"));
        assertOriginalBodyForGmailHtmlAndTextReplyInBlockQuote(result);
    }

    @Test
    public void testGmailHtmlAndTextReplyInBlockQuoteStrippingWithStrippingOfConvertedResult() throws Exception {
        QuotedEmailMatcher custom = textBlock -> startsWith(textBlock.get(0), "{quote}");

        EmailReplyCleaner htmlEmailReplyCleaner = new EmailReplyCleanerBuilder()
                .customMatchers(singletonList(custom))
                .defaultMatchers()
                .preferHtml(new HtmlToWikiTextConverter(emptyList()))
                .build();

        assertThat(htmlEmailReplyCleaner, IsInstanceOf.instanceOf(HtmlEmailReplyCleaner.class));
        assertThat(((HtmlEmailReplyCleaner) htmlEmailReplyCleaner).getDefaultEmailReplyCleaner().getMatchers(), hasItem(custom));

        final Message mimeMessage = createMIMEMessage(getDefaultHtmlFolder() + "/GmailHtmlAndTextReplyInBlockQuote.eml");

        final EmailReplyCleaner.EmailCleanerResult result = htmlEmailReplyCleaner.cleanQuotedEmail(mimeMessage);

        assertNotNull(result);

        assertThat(result.getBody(), is("Test"));
        assertThat(result.getRawBody(), is("Test"));
        assertOriginalBodyForGmailHtmlAndTextReplyInBlockQuote(result);
    }

    @Test
    public void testHtmlFormattingReply() throws Exception {
        final Message mimeMessage = createMIMEMessage(getDefaultHtmlFolder() + "/HtmlFormattingReply.eml");

        EmailReplyCleaner.EmailCleanerResult result = this.htmlEmailReplyCleaner.cleanQuotedEmail(mimeMessage);

        assertNotNull(result);

        // for this, we can detect that a forwarded message can be stripped
        assertEquals("{color:#ff9900}eragfaegfsefdg{color}\n" +
                "{color:#ff9900}easgvaesf;dgofjbkcw;asgfd{color}\n" +
                "* wadosfjbleaisgbvaegreagagdfadbf\n" +
                "* adfbaebd\n" +
                "# adfgbeabdf\n" +
                "\n" +
                " _adbaefgsaegfs +aefbdsdfbsdfb *dfbsgbdzbdb*+_", result.getBody());

        final EmailReplyCleaner defaultEmailReplyCleaner = new EmailReplyCleanerBuilder().defaultMatchers().build();

        result = defaultEmailReplyCleaner.cleanQuotedEmail(mimeMessage);

        assertNotNull(result);
        // for this, we can detect that a forwarded message can be stripped
        assertEquals("eragfaegfsefdg\n" +
                "easgvaesf;dgofjbkcw;asgfd\n" +
                "\n" +
                "   - wadosfjbleaisgbvaegreagagdfadbf\n" +
                "   - adfbaebd\n" +
                "\n" +
                "\n" +
                "   1. adfgbeabdf\n" +
                "\n" +
                "*adbaefgsaegfsaefbdsdfbsdfbdfbsgbdzbdb*", result.getBody());
    }

    private void assertOriginalBodyForGmailHtmlAndTextReplyInBlockQuote(final EmailReplyCleaner.EmailCleanerResult result) {
        assertThat(result.getOriginalBody(), is("Test\n" +
                "\n" +
                "\n" +
                "{quote}Quote this\n" +
                "\n" +
                "On 27 January 2016 at 01:43, Matthew McMahon <[mmcmahon@atlassian.com|mailto:mmcmahon@atlassian.com]> wrote:\n" +
                " *+_This_+* is a very  +_simple_+  *formatting _test_*\n" +
                "*  *_sdfsdf_*\n" +
                "*  *_sdfsdfsdf_*\n" +
                "# sdfsdf\n" +
                "#  *+sdfsdf+* sdfsdf  _sdf sd_ \n" +
                "That will do!{quote}\n" +
                "\n"));
    }

    @Test
    public void testHtmlReplyWithBigImage() throws Exception {
        final Message mimeMessage = createMIMEMessage(getDefaultHtmlFolder() + "/ReplyWithBigEmbeddedImage.eml");
        final EmailReplyCleaner.EmailCleanerResult result = htmlEmailReplyCleaner.cleanQuotedEmail(mimeMessage);

        assertNotNull(result);
        assertThat("Embedded image still there", result.getBody(), containsString("!data:image"));
    }

}
