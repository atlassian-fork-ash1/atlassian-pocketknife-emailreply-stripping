package com.atlassian.pocketknife.internal.emailreply;

import com.atlassian.fugue.Option;
import com.atlassian.fugue.Pair;
import com.atlassian.mail.converters.HtmlConverter;
import com.atlassian.mail.converters.wiki.HtmlToWikiTextConverter;
import com.atlassian.pocketknife.internal.emailreply.matcher.TestBase;
import org.apache.commons.mail.util.MimeMessageUtils;
import org.jsoup.Jsoup;

import javax.mail.Message;
import javax.mail.Session;
import java.io.File;
import java.util.Properties;

import static com.atlassian.fugue.Option.some;
import static com.atlassian.fugue.Pair.pair;
import static java.util.Collections.emptyList;

public enum EmailCorpus {
    GENERAL_EMPTY_BODY(
            "general/GeneralEmptyBody",
            ""
    ),
    GENERAL_1_LINE_TEXT(
            "general/General1LineText",
            "hello:"
    ),
    GENERAL_4_LINE_TEXT(
            "general/General4LineText",
            "The Big Oxmox advised her not to do so, because there were thousands of bad:\n" +
                    "Commas, wild Question Marks and devious Semikoli, but the Little Blind Text\n" +
                    "didn’t listen. She packed her seven versalia, put her initial into the belt\n" +
                    "and made herself on the way."
    ),
    GENERAL_5_LINE_TEXT(
            "general/General5LineText",
            "She packed her seven versalia, put her initial into the belt and made\n" +
                    "herself on the way. When she reached the first hills of the Italic\n" +
                    "Mountains, she had a last view back on the skyline of her hometown\n" +
                    "Bookmarksgrove, the headline of Alphabet Village and the subline of her own\n" +
                    "road, the Line Lane. Pityful a rethoric question ran over her cheek, then\n" +
                    "she continued her way."
    ),
    GMAIL_REPLY_ENGLISH_TEXT(
            "gmail/GmailReplyEnglishText",
            "The Big Oxmox advised her not to do so, because there were thousands of bad\n" +
                    "Commas, wild Question Marks and devious Semikoli, but the Little Blind Text\n" +
                    "didn’t listen. She packed her seven versalia, put her initial into the belt\n" +
                    "and made herself on the way."
    ),
    GMAIL_REPLY_ENGLISH_EMPTY_TEXT(
            "gmail/GmailReplyEnglishEmptyText",
            ""
    ),
    GMAIL_REPLY_ENGLISH_QUOTED_PATTERN_TEXT(
            "gmail/GmailReplyEnglishQuotedPatternText",
            "Hello there\n" +
                    "On Wed, Jun 10, 2015 at 11:56 AM, Darth Hanter <darthhanter@gmail.com>\n" +
                    "wrote:\n" +
                    "I'll be late"
    ),
    GMAIL_REPLY_ENGLISH_2_LINE_ATTRIBUTION_TEXT(
            "gmail/GmailReplyEnglish2LineAttributionText",
            "Hello there"
    ),
    GMAIL_REPLY_FRENCH_TEXT(
            "gmail/GmailReplyFrenchText",
            "Even the all-powerful Pointing has no control about the blind texts it is\n" +
                    "an almost unorthographic life One day however a small line of blind text by\n" +
                    "the name of Lorem Ipsum decided to leave for the far World of Grammar. The\n" +
                    "Big Oxmox advised her not to do so, because there were thousands of bad\n" +
                    "Commas, wild Question Marks and devious Semikoli, but the Little Blind Text\n" +
                    "didn’t listen."
    ),
    GMAIL_REPLY_FRENCH_EMPTY_TEXT(
            "gmail/GmailReplyFrenchEmptyText",
            ""
    ),
    GMAIL_REPLY_FRENCH_QUOTED_PATTERN_TEXT(
            "gmail/GmailReplyFrenchQuotedPatternText",
            "bonjour\n" +
                    "Le 10 juin 2015 17:25, Darth Hanter <darthhanter@gmail.com> a écrit :\n" +
                    "bonjour"
    ),
    GMAIL_REPLY_FRENCH_2_LINE_ATTRIBUTION_TEXT(
            "gmail/GmailReplyFrench2LineAttributionText",
            "bonjour"
    ),
    GMAIL_REPLY_GERMAN_TEXT(
            "gmail/GmailReplyGermanText",
            "Weit hinten, hinter den Wortbergen, fern der Länder Vokalien und\n" +
                    "Konsonantien leben die Blindtexte. Abgeschieden wohnen Sie in\n" +
                    "Buchstabhausen an der Küste des Semantik, eines großen Sprachozeans. Ein\n" +
                    "kleines Bächlein namens Duden fließt durch ihren Ort und versorgt sie mit\n" +
                    "den nötigen Regelialien."
    ),
    GMAIL_REPLY_GERMAN_QUOTED_PATTERN_TEXT(
            "gmail/GmailReplyGermanQuotedPatternText",
            "hallo\n" +
                    "Am 11. Juni 2015 um 10:23 schrieb Darth Hanter <darthhanter@gmail.com>:\n" +
                    "hallo"
    ),
    GMAIL_REPLY_GERMAN_2_LINE_ATTRIBUTION_TEXT(
            "gmail/GmailReplyGerman2LineAttributionText",
            "hallo"
    ),
    GMAIL_REPLY_GERMAN_EMPTY_TEXT(
            "gmail/GmailReplyGermanEmptyText",
            ""
    ),
    GMAIL_REPLY_JAP_TEXT(
            "gmail/GmailReplyJapText",
            "悪いカンマ、野生のクエスチョンマークとよこしまSemikoli何千ものがあったので、ビッグOxmoxは、そうしないように彼女をお勧めしますが、リトルブラインドのテキストは聞いていませんでした。彼女は、彼女の7\n" +
                    "versaliaを詰めベルトに彼女の最初のを入れて、途中で自分自身を作りました。"
    ),
    GMAIL_REPLY_JAP_EMPTY_TEXT(
            "gmail/GmailReplyJapEmptyText",
            ""
    ),
    GMAIL_REPLY_JAP_QUOTED_PATTERN_TEXT(
            "gmail/GmailReplyJapQuotedPatternText",
            "こんにちは\n" +
                    "2015年6月11日 11:23 Darth Hanter <darthhanter@gmail.com>:\n" +
                    "こんにちは"
    ),
    GMAIL_REPLY_JAP_2_LINE_ATTRIBUTION_TEXT(
            "gmail/GmailReplyJap2LineAttributionText",
            "こんにちは"
    ),
    GMAIL_REPLY_SPANISH_TEXT(
            "gmail/GmailReplySpanishText",
            "Un pequeño río llamado Duden fluye por su lugar y la suministra con el\n" +
                    "regelialia necesario. Es un país paradisematic, en el que las piezas asadas\n" +
                    "de frases volar en tu boca."
    ),
    GMAIL_REPLY_SPANISH_EMPTY_TEXT(
            "gmail/GmailReplySpanishEmptyText",
            ""
    ),
    GMAIL_REPLY_SPANISH_QUOTED_PATTERN_TEXT(
            "gmail/GmailReplySpanishQuotedPatternText",
            "hola\n" +
                    "El 11 de junio de 2015, 13:30, Darth Hanter <darthhanter@gmail.com>\n" +
                    " escribió:\n" +
                    "hola"
    ),
    GMAIL_REPLY_SPANISH_2_LINE_ATTRIBUTION_TEXT(
            "gmail/GmailReplySpanish2LineAttributionText",
            "Un pequeño río llamado Duden fluye por su lugar y la suministra con el\n" +
                    "regelialia necesario. Es un país paradisematic, en el que las piezas asadas\n" +
                    "de frases volar en tu boca."
    ),
    GMAIL_FORWARD_SPANISH_TEXT(
            "gmail/GmailForwardSpanishText",
            "Un pequeño río llamado Duden fluye por su lugar y la suministra con el\n" +
                    "regelialia necesario. Es un país paradisematic, en el que las piezas asadas\n" +
                    "de frases volar en tu boca."
    ),
    GMAIL_FORWARD_SPANISH_EMPTY_TEXT(
            "gmail/GmailForwardSpanishEmptyText",
            ""
    ),
    GMAIL_FORWARD_JAP_TEXT(
            "gmail/GmailForwardJapText",
            "こんにちは"
    ),
    GMAIL_FORWARD_JAP_EMPTY_TEXT(
            "gmail/GmailForwardJapEmptyText",
            ""
    ),
    GMAIL_FORWARD_GERMAN_TEXT(
            "gmail/GmailForwardGermanText",
            "Weit hinten, hinter den Wortbergen, fern der Länder Vokalien und\n" +
                    "Konsonantien leben die Blindtexte. Abgeschieden wohnen Sie in\n" +
                    "Buchstabhausen an der Küste des Semantik, eines großen Sprachozeans. Ein\n" +
                    "kleines Bächlein namens Duden fließt durch ihren Ort und versorgt sie mit\n" +
                    "den nötigen Regelialien."
    ),
    GMAIL_FORWARD_GERMAN_EMPTY_TEXT(
            "gmail/GmailForwardGermanEmptyText",
            ""
    ),
    GMAIL_FORWARD_ENGLISH_TEXT(
            "gmail/GmailForwardEnglishText",
            "Even the all-powerful Pointing has no control about the blind texts it is\n" +
                    "an almost unorthographic life One day however a small line of blind text by\n" +
                    "the name of Lorem Ipsum decided to leave for the far World of Grammar."
    ),
    GMAIL_FORWARD_ENGLISH_EMPTY_TEXT(
            "gmail/GmailForwardEnglishEmptyText",
            ""
    ),
    GMAIL_FORWARD_FRENCH_TEXT(
            "gmail/GmailForwardFrenchText",
            "The Big Oxmox lui a conseillé de ne pas le faire , parce qu'il y avait\n" +
                    "des milliers de mauvaise virgules , points d'interrogation sauvages et\n" +
                    "sournois Semikoli , mais le texte de Little aveugles n'a pas écouté.\n" +
                    "Elle fit ses sept Versalia , a mis sa première dans la ceinture et se\n" +
                    "fait sur ​​le chemin ."
    ),
    GMAIL_FORWARD_FRENCH_EMPTY_TEXT(
            "gmail/GmailForwardFrenchEmptyText",
            ""
    ),
    GMAIL_REPLY_GENERIC_TEXT(
            "gmail/GmailReplyGenericText",
            "hello"
    ),
    GMAIL_REPLY_GENERIC_EMPTY_TEXT(
            "gmail/GmailReplyGenericEmptyText",
            ""
    ),
    GMAIL_REPLY_GENERIC_2_LINE_ATTRIBUTION_TEXT(
            "gmail/GmailReplyGeneric2LineAttributionText",
            "2 line attribution"
    ),
    GMAIL_REPLY_GENERIC_QUOTED_PATTERN_TEXT(
            "gmail/GmailReplyGenericQuotedPatternText",
            "hello\n" +
                    "2015-06-11 14:04 GMT+07:00 Darth Hanter <darthhanter@gmail.com>:\n" +
                    "hello"
    ),
    GMAIL_REPLY_STYLING(
            "gmail/GmailReplyStyling",
            "This is the google reply\n\nTry *this*\n\n*And* *this with **this*\n\nEnd",
            some(pair(new HtmlToWikiTextConverter(emptyList()), "This is the google reply\n\nTry  *this*\n\n\n +And+  _this with_  *this*\n\n\nEnd"))
    ),
    GMAIL_REPLY_STYLING_LISTS(
            "gmail/GmailReplyStylingLists",
            "Add in a list\n\n\n   1. Ordered One\n   2. Ordered Two\n\n*Style*\n\n*Another **LIST*\n\n\n   - Unordered One\n   - Unordered Two\n\nEnd",
            some(pair(new HtmlToWikiTextConverter(emptyList()), "Add in a list\n# Ordered One\n# Ordered Two\n\n *_Style_*\n\n\n _Another_  +LIST+\n* Unordered One\n* Unordered Two\n\nEnd"))
    ),
    OUTLOOK_REPLY_ENGLISH_TEXT(
            "outlook.com/OutlookReplyEnglishText",
            "A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth."
    ),
    OUTLOOK_REPLY_PLAIN_ENGLISH_TEXT(
            "outlook.com/OutlookReplyPlainEnglishText",
            "Reply here\nThen end it"
    ),
    OUTLOOK_REPLY_CC_ENGLISH_TEXT(
            "outlook.com/OutlookReplyCCEnglighText",
            "Test a CC reply"
    ),
    OUTLOOK_REPLY_ENGLISH_EMPTY_TEXT(
            "outlook.com/OutlookReplyEnglishEmptyText",
            ""
    ),
    OUTLOOK_FORWARD_ENGLISH_TEXT(
            "outlook.com/OutlookForwardEnglishText",
            "hey a forwarded message"
    ),
    OUTLOOK_FORWARD_ENGLISH_EMPTY_TEXT(
            "outlook.com/OutlookForwardEnglishEmptyText",
            ""
    ),
    OUTLOOK_REPLY_ENGLISH_QUOTED_PATTERN_TEXT(
            "outlook.com/OutlookReplyEnglishQuotedPatternText",
            "hey\n" +
                    "From: Kha\n" +
                    "To: somebody\n" +
                    "how are you"
    ),
    OUTLOOK_REPLY_STYLING(
            "outlook.com/OutlookReplyStylings",
            "From Outlook\nStyle\nNow this\nand THIS",
            some(pair(new HtmlToWikiTextConverter(emptyList()), "From Outlook\n\n *Style*\n\n\nNow  _+this+_\n\n\nand  *THIS*"))
    ),
    OUTLOOK_REPLY_STYLING_LISTS(
            "outlook.com/OutlookReplyStylingLists",
            "List\nOrdered One from OutlookOrdered Two from Outlook\nNext\nUnordered One from OutlookUnordered Two from Outlook\nEnd",
            some(pair(new HtmlToWikiTextConverter(emptyList()), "List\n# Ordered One from Outlook\n# Ordered Two from Outlook\n\n\nNext\n* Unordered One from Outlook\n* Unordered Two from Outlook\n\n\nEnd"))
    ),
    OUTLOOK_APP_REPLY_SIGNATURE(
            "outlook.com/OutlookAppReplySignature",
            "TEST this now"
    ),
    OUTLOOK_APP_REPLY_NO_SIGNATURE(
            "outlook.com/OutlookAppReplyNoSignature",
            "Djdjd\nFff\nFf\nOoo99"
    ),
    OUTLOOK_WEB_NO_DEFAULT_SIGNATURE(
            "outlook.com/OutlookWebNoDefaultSignature",
            "Hi, I'm the customer and Strip Quotes and HTML Parsing is currently enabled"
    ),
    OUTLOOK_WEB_DEFAULT_SIGNATURE(
            "outlook.com/OutlookDefaultWebSignature",
            "I'm the kliou_test@outlook.com account, leaving a comment with Strip Quotes and HTML Parsing on."
    ),
    OUTLOOK_WEB_DEFAULT_SIGNATURE_WITH_SPACE_AT_END(
            "outlook.com/OutlookDefaultWebSignaureWithSpaceAtEnd",
            "erewwwreewr"
    ),
    YAHOO_REPLY_ENGLISH_TEXT(
            "yahoo/YahooReplyEnglishText",
            "hellooooo"
    ),
    YAHOO_REPLY_ENGLISH_EMPTY_TEXT(
            "yahoo/YahooReplyEnglishEmptyText",
            ""
    ),
    YAHOO_REPLY_ENGLISH_QUOTED_PATTERN_TEXT(
            "yahoo/YahooReplyEnglishQuotedPatternText",
            "hello\n" +
                    "On Thursday, June 11, 2015 5:09 PM, hanter darth <hanterdarth@yahoo.com> wrote:\n" +
                    "fine"
    ),
    YAHOO_FORWARD_ENGLISH_TEXT(
            "yahoo/YahooForwardEnglishText",
            "aye aye sir"
    ),
    YAHOO_FORWARD_ENGLISH_EMPTY_TEXT(
            "yahoo/YahooForwardEnglishEmptyText",
            ""
    ),
    YAHOO_REPLY_JAP_TEXT(
            "yahoo/YahooReplyJapText",
            "お元気ですか"
    ),
    YAHOO_REPLY_JAP_EMPTY_TEXT(
            "yahoo/YahooReplyJapEmptyText",
            ""
    ),
    YAHOO_FORWARD_JAP_TEXT(
            "yahoo/YahooForwardJapText",
            "お元気ですか"
    ),
    YAHOO_FORWARD_JAP_EMPTY_TEXT(
            "yahoo/YahooForwardJapEmptyText",
            ""
    ),
    YAHOO_REPLY_GERMAN_TEXT(
            "yahoo/YahooReplyGermanText",
            "Ein kleines Bächlein namens Duden fließt durch ihren Ort und versorgt sie mit den nötigen Regelialien. Es ist ein paradiesmatisches Land, in dem einem gebratene Satzteile in den Mund fliegen."
    ),
    YAHOO_REPLY_GERMAN_EMPTY_TEXT(
            "yahoo/YahooReplyGermanEmptyText",
            ""
    ),
    YAHOO_REPLY_GERMAN_QUOTED_PATTERN_TEXT(
            "yahoo/YahooReplyGermanQuotedPatternText",
            "hallo hanter darth <hanterdarth@yahoo.com> schrieb am 11:28 Freitag, 12.Juni 2015:\n" +
                    " hallo"
    ),
    YAHOO_FORWARD_GERMAN_TEXT(
            "yahoo/YahooForwardGermanText",
            "Ein kleines Bächlein namens Duden fließt durch ihren Ort und versorgt sie mit den nötigen Regelialien. Es ist ein paradiesmatisches Land, in dem einem gebratene Satzteile in den Mund fliegen."
    ),
    YAHOO_FORWARD_GERMAN_EMPTY_TEXT(
            "yahoo/YahooForwardGermanEmptyText",
            ""
    ),
    YAHOO_REPLY_FRENCH_TEXT(
            "yahoo/YahooReplyFrenchText",
            "Une petite rivière nommée Duden coule par leur place et lui fournit l'regelialia nécessaire. Il est un pays paradisematic, dans lequel des parties de phrases grillées volent dans votre bouche."
    ),
    YAHOO_REPLY_FRENCH_EMPTY_TEXT(
            "yahoo/YahooReplyFrenchEmptyText",
            ""
    ),
    YAHOO_REPLY_FRENCH_QUOTED_PATTERN_TEXT(
            "yahoo/YahooReplyFrenchQuotedPatternText",
            "bonjour\n" +
                    "Le Vendredi 12 juin 2015 11h57, hanter darth <hanterdarth@yahoo.com> a écrit :\n" +
                    "bonjour"
    ),
    YAHOO_FORWARD_FRENCH_TEXT(
            "yahoo/YahooForwardFrenchText",
            "Une petite rivière nommée Duden coule par leur place et lui fournit l'regelialia nécessaire. Il est un pays paradisematic, dans lequel des parties de phrases grillées volent dans votre bouche."
    ),
    YAHOO_FORWARD_FRENCH_EMPTY_TEXT(
            "yahoo/YahooForwardFrenchEmptyText",
            ""
    ),
    YAHOO_REPLY_SPANISH_TEXT(
            "yahoo/YahooReplySpanishText",
            "hola"
    ),
    YAHOO_REPLY_SPANISH_EMPTY_TEXT(
            "yahoo/YahooReplySpanishEmptyText",
            ""
    ),
    YAHOO_REPLY_SPANISH_QUOTED_PATTERN_TEXT(
            "yahoo/YahooReplySpanishQuotedPatternText",
            "hola\n" +
                    "El Viernes 12 de junio de 2015 14:06, hanter darth <hanterdarth@yahoo.com> escribió:\n" +
                    "hola"
    ),
    YAHOO_FORWARD_SPANISH_TEXT(
            "yahoo/YahooForwardSpanishText",
            "hola"
    ),
    YAHOO_REPLY_STYLING(
            "yahoo/YahooReplyStyling",
            "From Yahoo\nStyling\nMore Styling\nEnd",
            some(pair(new HtmlToWikiTextConverter(emptyList()), "{color:#000}From Yahoo\n\n\n *+_Styling_+*\n\n\n *More*  _Styling_\n\n\nEnd{color}"))
    ),
    YAHOO_REPLY_STYLING_LISTS(
            "yahoo/YahooReplyStylingLists",
            "List\n   \n   - Ordered One from yahoo\n   - Ordered Two from yahoo\n\nNext\n   \n   - Unordered One from yahoo\n   - Unordered Two from yahoo\n\nEnd",
            some(pair(new HtmlToWikiTextConverter(emptyList()), "{color:#000}List\n{color}\n# {color:#000}Ordered One from yahoo\n{color}\n# {color:#000}Ordered Two from yahoo\n\n\nNext\n{color}\n* {color:#000}Unordered One from yahoo\n{color}\n* {color:#000}Unordered Two from yahoo\n\n\nEnd{color}"))
    ),
    YAHOO_FORWARD_SPANISH_EMPTY_TEXT(
            "yahoo/YahooForwardSpanishEmptyText",
            ""
    ),
    IPHONE_REPLY_ENGLISH_TEXT(
            "ios7/AppleReplyEnglishText",
            "yolo"
    ),
    IPHONE_REPLY_ENGLISH_EMPTY_TEXT(
            "ios7/AppleReplyEnglishEmptyText",
            ""
    ),
    IPHONE_REPLY_ENGLISH_QUOTED_PATTERN_TEXT(
            "ios7/AppleReplyEnglishQuotedPatternText",
            "yolo\n" +
                    "sent from my iphone\n" +
                    "hey there"
    ),
    IPHONE_FORWARD_ENGLISH_TEXT(
            "ios7/AppleForwardEnglishText",
            "yoyoyoyo"
    ),
    IPHONE_FORWARD_ENGLISH_EMPTY_TEXT(
            "ios7/AppleForwardEnglishEmptyText",
            ""
    ),
    IPHONE_REPLY_FRENCH_TEXT(
            "ios7/AppleReplyFrenchText",
            "merci"
    ),
    IPHONE_REPLY_FRENCH_EMPTY_TEXT(
            "ios7/AppleReplyFrenchEmptyText",
            ""
    ),
    IPHONE_REPLY_FRENCH_QUOTED_PATTERN_TEXT(
            "ios7/AppleReplyFrenchQuotedPatternText",
            "merci\n" +
                    "Envoyé de mon iPhone\n" +
                    "merci"
    ),
    IPHONE_FORWARD_FRENCH_TEXT(
            "ios7/AppleForwardFrenchText",
            "merci"
    ),
    IPHONE_FORWARD_FRENCH_EMPTY_TEXT(
            "ios7/AppleForwardFrenchEmptyText",
            ""
    ),
    IPHONE_REPLY_GERMAN_TEXT(
            "ios7/AppleReplyGermanText",
            "dank"
    ),
    IPHONE_REPLY_GERMAN_EMPTY_TEXT(
            "ios7/AppleReplyGermanEmptyText",
            ""
    ),
    IPHONE_REPLY_GERMAN_QUOTED_PATTERN_TEXT(
            "ios7/AppleReplyGermanQuotedPatternText",
            "dank\n" +
                    "Von meinem iPhone gesendet\n" +
                    "dank"
    ),
    IPHONE_FORWARD_GERMAN_TEXT(
            "ios7/AppleForwardGermanText",
            "dank"
    ),
    IPHONE_FORWARD_GERMAN_EMPTY_TEXT(
            "ios7/AppleForwardGermanEmptyText",
            ""
    ),
    IPHONE_REPLY_JAP_TEXT(
            "ios7/AppleReplyJapText",
            "あかさ"
    ),
    IPHONE_REPLY_JAP_EMPTY_TEXT(
            "ios7/AppleReplyJapEmptyText",
            ""
    ),
    IPHONE_REPLY_JAP_QUOTED_PATTERN_TEXT(
            "ios7/AppleReplyJapQuotedPatternText",
            "あかさ\n" +
                    "iPhoneから送信\n" +
                    "あかさ",
            some(pair(new HtmlToWikiTextConverter(emptyList()), "あかさ\niPhoneから送信\nあかさ\n\n\n{quote}{quote}"))
    ),
    IPHONE_FORWARD_JAP_TEXT(
            "ios7/AppleForwardJapText",
            "あかさ"
    ),
    IPHONE_FORWARD_JAP_EMPTY_TEXT(
            "ios7/AppleForwardJapEmptyText",
            ""
    ),
    IPHONE_REPLY_SPANISH_TEXT(
            "ios7/AppleReplySpanishText",
            "gracias"
    ),
    IPHONE_REPLY_SPANISH_EMPTY_TEXT(
            "ios7/AppleReplySpanishEmptyText",
            ""
    ),
    IPHONE_REPLY_SPANISH_QUOTED_PATTERN_TEXT(
            "ios7/AppleReplySpanishQuotedPatternText",
            "gracias\n" +
                    "Enviado desde mi iPhone\n" +
                    "gracias"
    ),
    IPHONE_FORWARD_SPANISH_TEXT(
            "ios7/AppleForwardSpanishText",
            "gracias"
    ),
    IPHONE_FORWARD_SPANISH_EMPTY_TEXT(
            "ios7/AppleForwardSpanishEmptyText",
            ""
    ),
    IPHONE_REPLY_WITHOUT_SENT_FROM_DEVICE_BLOCK_TEXT(
            "ios7/AppleReplyWithoutSentFromDeviceBlock",
            "yolo"
    ),
    IPHONE_FORWARD_WITHOUT_SENT_FROM_DEVICE_BLOCK_TEXT(
            "ios7/AppleForwardWithoutSentFromDeviceBlock",
            "yoyoyoyo"
    ),
    IPAD_REPLY_ENGLISH_TEXT(
            "ios7/ipad/IpadReplyEnglishText",
            "How are you."
    ),
    IPAD_REPLY_ENGLISH_TEXT_NO_BLANK_LINE(
            "ios7/ipad/IpadReplyEnglishTextNoBlankLine",
            "How are you."
    ),
    IPAD_REPLY_ENGLISH_EMPTY_TEXT(
            "ios7/ipad/IpadReplyEnglishEmptyText",
            ""
    ),
    IPAD_REPLY_ENGLISH_QUOTED_PATTERN_TEXT(
            "ios7/ipad/IpadReplyEnglishQuotedPatternText",
            "Hello\n" +
                    "Sent from my iPad\n" +
                    "Hey"
    ),
    IPAD_REPLY_ENGLISH_WITHOUT_SIGNATURE(
            "ios7/ipad/IpadReplyEnglishWithoutSignature",
            "Hey"
    ),
    IPAD_FORWARD_ENGLISH_TEXT(
            "ios7/ipad/IpadForwardEnglishText",
            "Hello"
    ),
    IPAD_FORWARD_ENGLISH_EMPTY_TEXT(
            "ios7/ipad/IpadForwardEnglishEmptyText",
            ""
    ),
    IPAD_FORWARD_ENGLISH_NO_BLANK_LINE(
            "ios7/ipad/IpadForwardEnglishNoBlankLine",
            ""
    ),
    IPAD_FORWARD_ENGLISH_WITHOUT_SIGNATURE(
            "ios7/ipad/IpadForwardEnglishWithoutSignature",
            "Hey"
    ),
    IPAD_REPLY_FRENCH_TEXT(
            "ios7/ipad/IpadReplyFrenchText",
            "Merci"
    ),
    IPAD_REPLY_FRENCH_EMPTY_TEXT(
            "ios7/ipad/IpadReplyFrenchEmptyText",
            ""
    ),
    IPAD_REPLY_FRENCH_QUOTED_PATTERN_TEXT(
            "ios7/ipad/IpadReplyFrenchQuotedPatternText",
            "Bonjour\n" +
                    "Envoyé de mon iPad\n" +
                    "Bonjour"
    ),
    IPAD_REPLY_FRENCH_WITHOUT_SIGNATURE(
            "ios7/ipad/IpadReplyFrenchWithoutSignature",
            "Merci"
    ),
    IPAD_FORWARD_FRENCH_TEXT(
            "ios7/ipad/IpadForwardFrenchText",
            "Merci"
    ),
    IPAD_FORWARD_FRENCH_EMPTY_TEXT(
            "ios7/ipad/IpadForwardFrenchEmptyText",
            ""
    ),
    IPAD_FORWARD_FRENCH_WITHOUT_SIGNATURE(
            "ios7/ipad/IpadForwardFrenchWithoutSignature",
            "Merci"
    ),
    IPAD_REPLY_GERMAN_TEXT(
            "ios7/ipad/IpadReplyGermanText",
            "Hallo"
    ),
    IPAD_REPLY_GERMAN_EMPTY_TEXT(
            "ios7/ipad/IpadReplyGermanEmptyText",
            ""
    ),
    IPAD_REPLY_GERMAN_QUOTED_PATTERN_TEXT(
            "ios7/ipad/IpadReplyGermanQuotedPatternText",
            "Hallo\n" +
                    "Von meinem iPad gesendet\n" +
                    "Hallo"
    ),
    IPAD_REPLY_GERMAN_WITHOUT_SIGNATURE(
            "ios7/ipad/IpadReplyGermanWithoutSignature",
            "Hallo"
    ),
    IPAD_FORWARD_GERMAN_TEXT(
            "ios7/ipad/IpadForwardGermanText",
            "Hallo"
    ),
    IPAD_FORWARD_GERMAN_EMPTY_TEXT(
            "ios7/ipad/IpadForwardGermanEmptyText",
            ""
    ),
    IPAD_FORWARD_GERMAN_WITHOUT_SIGNATURE(
            "ios7/ipad/IpadForwardGermanWithoutSignature",
            "Hallo"
    ),
    IPAD_REPLY_SPANISH_TEXT(
            "ios7/ipad/IpadReplySpanishText",
            "Hola"
    ),
    IPAD_REPLY_SPANISH_EMPTY_TEXT(
            "ios7/ipad/IpadReplySpanishEmptyText",
            ""
    ),
    IPAD_REPLY_SPANISH_QUOTED_PATTERN_TEXT(
            "ios7/ipad/IpadReplySpanishQuotedPatternText",
            "Hola\n" +
                    "Enviado desde mi iPad\n" +
                    "Hola"
    ),
    IPAD_REPLY_SPANISH_WITHOUT_SIGNATURE(
            "ios7/ipad/IpadReplySpanishWithoutSignature",
            "Hola"
    ),
    IPAD_FORWARD_SPANISH_TEXT(
            "ios7/ipad/IpadForwardSpanishText",
            "Hola"
    ),
    IPAD_FORWARD_SPANISH_EMPTY_TEXT(
            "ios7/ipad/IpadForwardSpanishEmptyText",
            ""
    ),
    IPAD_FORWARD_SPANISH_WITHOUT_SIGNATURE(
            "ios7/ipad/IpadForwardSpanishWithoutSignature",
            "Hola"
    ),
    IPAD_REPLY_JAP_TEXT(
            "ios7/ipad/IpadReplyJapText",
            "あああ"
    ),
    IPAD_REPLY_JAP_QUOTED_PATTERN_TEXT(
            "ios7/ipad/IpadReplyJapQuotedPatternText",
            "あ\n" +
                    "24-06-2015 10:27、Darth Hanter <darthhanter@gmail.com> のメッセージ:\n" +
                    "あ"
    ),
    IPAD_REPLY_JAP_EMPTY_TEXT(
            "ios7/ipad/IpadReplyJapEmptyText",
            ""
    ),
    IPAD_REPLY_JAP_WITHOUT_SIGNATURE(
            "ios7/ipad/IpadReplyJapWithoutSignature",
            "あああ"
    ),
    IPAD_REPLY_JAP_WITHOUT_SIGNATURE_COMPACT(
            "ios7/ipad/IpadReplyJapNoSignCompact",
            "あああ"
    ),
    IPAD_FORWARD_JAP_TEXT(
            "ios7/ipad/IpadForwardJapText",
            "あああ"
    ),
    IPAD_FORWARD_JAP_EMPTY_TEXT(
            "ios7/ipad/IpadForwardJapEmptyText",
            ""
    ),
    IPAD_FORWARD_JAP_WITHOUT_SIGNATURE(
            "ios7/ipad/IpadForwardJapWithoutSignature",
            "あああ"
    ),
    THUNDERBIRD_FORWARD_STYLINGS(
            "desktop/thunderbird/v31/ThunderbirdForwardStylings",
            "/*Styling*/\n\nMore _styling\n\n_List\n\n 1. Ordered one\n 2. Ordered two\n\nAnother\n\n  * Unordered one\n  * Unordered two\n\nEnd",
            some(pair(new HtmlToWikiTextConverter(emptyList()), "_*Styling*_\n \n More  +styling+ \n \n List \n# Ordered one \n# Ordered two \n \n\nAnother \n* Unordered one \n* Unordered two \n \n\nEnd"))
    );

    private static final String PATH_PREFIX = "./src/test/resources/com/atlassian/pocketknife/internal/emailreply/corpus/";
    private static final String FILE_EXTENSION = ".eml";

    private final String fileName;
    private final String expectedContent;
    private final Option<Pair<HtmlConverter, String>> htmlTestableExpectedContent;

    EmailCorpus(String fileName, String expectedContent) {
        this(fileName, expectedContent, some(pair((html) -> {
            final String text = Jsoup.parse(html).body().text();
            return TestBase.normalize(text);
        }, TestBase.normalize(expectedContent))));
    }

    EmailCorpus(String fileName, String expectedContent, Option<Pair<HtmlConverter, String>> htmlTestableExpectedContent) {
        this.fileName = fileName;
        this.expectedContent = expectedContent;
        this.htmlTestableExpectedContent = htmlTestableExpectedContent;
    }

    public Message toMimeMessage() throws Exception {
        final Session session = Session.getDefaultInstance(new Properties());
        return MimeMessageUtils.createMimeMessage(session, new File(this.buildFilePath()));
    }

    public String getExpectedContent() {
        return expectedContent;
    }

    public Option<Pair<HtmlConverter, String>> getHtmlTestableExpectedContent() {
        return htmlTestableExpectedContent;
    }

    private String buildFilePath() {
        return PATH_PREFIX + this.fileName + FILE_EXTENSION;
    }
}