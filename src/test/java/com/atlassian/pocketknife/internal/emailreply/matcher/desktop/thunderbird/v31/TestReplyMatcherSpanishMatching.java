package com.atlassian.pocketknife.internal.emailreply.matcher.desktop.thunderbird.v31;

import com.atlassian.pocketknife.internal.emailreply.DefaultEmailReplyCleaner;
import com.atlassian.pocketknife.internal.emailreply.matcher.TestBase;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Test;

/**
 * <p> Test cases: <br/>
 * 1. Language based normal reply. <br/>
 * 2. Language based with long fullname reply. <br/>
 * 3. Language based with malicious content reply. <br/>
 * </p>
 */
public class TestReplyMatcherSpanishMatching extends TestBase
{
    private final String corpusFolder = getDefaultCorpusFolder() + "/desktop/thunderbird/v31";

    private final String SpanishReplyEmailFile = corpusFolder + "/SpanishReplyEmail.eml";
    private final String StrippedSpanishReplyEmailFile = corpusFolder + "/stripped/SpanishReplyEmail.eml";

    private final String SpanishLongFullnameReplyEmailFile = corpusFolder + "/SpanishLongFullnameReplyEmail.eml";
    private final String StrippedSpanishLongFullnameReplyEmailFile =
            corpusFolder + "/stripped/SpanishLongFullnameReplyEmail.eml";

    private final String SpanishMaliciousContentReplyEmailFile =
            corpusFolder + "/SpanishMaliciousContentReplyEmail.eml";
    private final String StrippedSpanishMaliciousContentReplyEmailFile =
            corpusFolder + "/stripped/SpanishMaliciousContentReplyEmail.eml";

    @Before
    public void setUp() throws Exception
    {
        matcher = new ReplyMatcher();
        cleaner = new DefaultEmailReplyCleaner(Lists.newArrayList(matcher));
    }

    @Test
    public void testSpanishReplyEmailMatch() throws Exception
    {
        strippingTest(StrippedSpanishReplyEmailFile, SpanishReplyEmailFile);
    }

    @Test
    public void testSpanishLongFullnameReplyEmailMatch() throws Exception
    {
        strippingTest(StrippedSpanishLongFullnameReplyEmailFile, SpanishLongFullnameReplyEmailFile);
    }

    @Test
    public void testSpanishMaliciousContentReplyEmailMatch() throws Exception
    {
        strippingTest(StrippedSpanishMaliciousContentReplyEmailFile, SpanishMaliciousContentReplyEmailFile);
    }
}
