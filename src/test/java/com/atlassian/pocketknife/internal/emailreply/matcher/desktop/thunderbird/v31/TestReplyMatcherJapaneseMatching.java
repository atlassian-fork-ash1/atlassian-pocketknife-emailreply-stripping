package com.atlassian.pocketknife.internal.emailreply.matcher.desktop.thunderbird.v31;

import com.atlassian.pocketknife.internal.emailreply.DefaultEmailReplyCleaner;
import com.atlassian.pocketknife.internal.emailreply.matcher.TestBase;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Test;

import java.nio.charset.Charset;

/**
 * <p> Test cases: <br/>
 * 1. Language based normal reply. <br/>
 * 2. Language based with long fullname reply. <br/>
 * 3. Language based with malicious content reply. <br/>
 * </p>
 */
public class TestReplyMatcherJapaneseMatching extends TestBase
{
    private final String corpusFolder = getDefaultCorpusFolder() + "/desktop/thunderbird/v31";

    private final String JapaneseReplyEmailFile = corpusFolder + "/JapaneseReplyEmail.eml";
    private final String StrippedJapaneseReplyEmailFile = corpusFolder + "/stripped/JapaneseReplyEmail.eml";

    private final String JapaneseLongFullnameReplyEmailFile = corpusFolder + "/JapaneseLongFullnameReplyEmail.eml";
    private final String StrippedJapaneseLongFullnameReplyEmailFile =
            corpusFolder + "/stripped/JapaneseLongFullnameReplyEmail.eml";

    private final String JapaneseMaliciousContentReplyEmailFile =
            corpusFolder + "/JapaneseMaliciousContentReplyEmail.eml";
    private final String StrippedJapaneseMaliciousContentReplyEmailFile =
            corpusFolder + "/stripped/JapaneseMaliciousContentReplyEmail.eml";

    @Before
    public void setUp() throws Exception
    {
        matcher = new ReplyMatcher();
        cleaner = new DefaultEmailReplyCleaner(Lists.newArrayList(matcher));
    }

    @Test
    public void testJapaneseReplyEmailMatch() throws Exception
    {
        strippingTest(StrippedJapaneseReplyEmailFile, JapaneseReplyEmailFile, Charset.forName("iso-2022-jp"));
    }

    @Test
    public void testJapaneseLongFullnameReplyEmailMatch() throws Exception
    {
        strippingTest(
                StrippedJapaneseLongFullnameReplyEmailFile,
                JapaneseLongFullnameReplyEmailFile,
                Charset.forName("iso-2022-jp"));
    }

    @Test
    public void testJapaneseMaliciousContentReplyEmailMatch() throws Exception
    {
        strippingTest(
                StrippedJapaneseMaliciousContentReplyEmailFile,
                JapaneseMaliciousContentReplyEmailFile,
                Charset.forName("iso-2022-jp"));
    }
}
