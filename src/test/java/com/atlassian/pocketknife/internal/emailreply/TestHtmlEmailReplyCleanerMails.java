package com.atlassian.pocketknife.internal.emailreply;

import com.atlassian.fugue.Pair;
import com.atlassian.pocketknife.api.emailreply.EmailReplyCleaner;
import com.atlassian.pocketknife.api.emailreply.EmailReplyCleanerBuilder;
import com.atlassian.pocketknife.internal.emailreply.matcher.TestBase;
import org.hamcrest.core.IsInstanceOf;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import javax.mail.Message;
import java.io.IOException;
import java.util.Collection;
import java.util.List;

import static com.google.common.collect.Lists.newArrayList;
import static org.apache.commons.lang.StringUtils.EMPTY;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

@RunWith(Parameterized.class)
public class TestHtmlEmailReplyCleanerMails extends TestBase {

    @Parameterized.Parameters(name = "{index}: {0}")
    public static Collection<Object[]> data() {
        final List<Object[]> list = newArrayList();

        for (EmailCorpus corpus : EmailCorpus.values()) {
            if (corpus.getHtmlTestableExpectedContent().isDefined()) {
                list.add(new Object[]{corpus});
            }
        }

        return list;
    }

    private EmailCorpus emailCorpus;
    private EmailReplyCleaner htmlEmailReplyCleaner;
    private boolean textOnly = true;

    public TestHtmlEmailReplyCleanerMails(EmailCorpus emailCorpus) {
        this.emailCorpus = emailCorpus;
    }

    @Before
    public void setup() {
        this.htmlEmailReplyCleaner =
                new EmailReplyCleanerBuilder()
                        .defaultMatchers()
                        .preferHtml(
                                html -> emailCorpus.getHtmlTestableExpectedContent()
                                        .map(pair -> {
                                            textOnly = false;
                                            try {
                                                return pair.left().convert(html);
                                            } catch (IOException e) {
                                                throw new RuntimeException(e);
                                            }
                                        }).getOrThrow(() -> new IllegalStateException("No HTML Converter provided ... this was not expected to be called"))
                        )
                        .build();

        assertThat(this.htmlEmailReplyCleaner, IsInstanceOf.instanceOf(HtmlEmailReplyCleaner.class));
    }

    @Test
    public void testStripping() throws Exception {
        final Message message = emailCorpus.toMimeMessage();

        final EmailReplyCleaner.EmailCleanerResult result = this.htmlEmailReplyCleaner.cleanQuotedEmail(message);

        final String expected;
        if (textOnly) {
            expected = emailCorpus.getExpectedContent();
        } else {
            expected = emailCorpus.getHtmlTestableExpectedContent().map(Pair::right).getOrElse(EMPTY);
        }

        assertThat("stripped result is not null", result, notNullValue());
        assertThat("Stripped body is incorrect", result.getRawBody(), is(expected));
    }

}
