package com.atlassian.pocketknife.internal.emailreply.matcher.desktop.applemail.v8;

import com.atlassian.pocketknife.internal.emailreply.DefaultEmailReplyCleaner;
import com.atlassian.pocketknife.internal.emailreply.matcher.TestBase;
import com.google.common.collect.Lists;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Test;

/**
 * <p> Test cases: <br/>
 * 1. Language based normal reply. <br/>
 * 2. Language based with long fullname reply. <br/>
 * 3. Language based with malicious content reply. <br/>
 * 4. Language based normal forward. <br/>
 * 5. Language based with malicious forward. <br/>
 * </p>
 */
public class TestMatcherEnglishMatching extends TestBase
{
    private final String corpusFolder = getDefaultCorpusFolder() + "/desktop/applemail/v8";

    private final String EnglishReplyEmailFile = corpusFolder + "/EnglishReplyEmail.eml";
    private final String StrippedEnglishReplyEmailFile = corpusFolder + "/stripped/EnglishReplyEmail.eml";

    private final String EnglishLongFullnameReplyEmailFile = corpusFolder + "/EnglishLongFullnameReplyEmail.eml";
    private final String StrippedEnglishLongFullnameReplyEmailFile =
            corpusFolder + "/stripped/EnglishLongFullnameReplyEmail.eml";

    private final String EnglishMaliciousContentReplyEmailFile =
            corpusFolder + "/EnglishMaliciousContentReplyEmail.eml";
    private final String StrippedEnglishMaliciousContentReplyEmailFile =
            corpusFolder + "/stripped/EnglishMaliciousContentReplyEmail.eml";

    private final String EnglishForwardEmailFile = corpusFolder + "/EnglishForwardEmail.eml";
    private final String StrippedEnglishForwardEmailFile = corpusFolder + "/stripped/EnglishForwardEmail.eml";

    private final String EnglishMaliciousContentForwardEmailFile =
            corpusFolder + "/EnglishMaliciousContentForwardEmail.eml";
    private final String StrippedEnglishMaliciousContentForwardEmailFile =
            corpusFolder + "/stripped/EnglishMaliciousContentForwardEmail.eml";

    @Before
    public void setUp() throws Exception
    {
        matcher = new Matcher();
        cleaner = new DefaultEmailReplyCleaner(Lists.newArrayList(matcher));
    }

    @Test
    public void testEnglishReplyEmailMatch() throws Exception
    {
        strippingTest(StrippedEnglishReplyEmailFile, EnglishReplyEmailFile);
    }

    @Test
    public void testEnglishLongFullnameReplyEmailMatch() throws Exception
    {
        strippingTest(StrippedEnglishLongFullnameReplyEmailFile, EnglishLongFullnameReplyEmailFile);
    }

    @Test
    public void testEnglishMaliciousContentReplyEmailMatch() throws Exception
    {
        strippingTest(StrippedEnglishMaliciousContentReplyEmailFile, EnglishMaliciousContentReplyEmailFile);
    }

    @Test
    public void testEnglishForwardEmailMatch() throws Exception
    {
        strippingTest(StrippedEnglishForwardEmailFile, EnglishForwardEmailFile);
    }

    @Test
    public void testEnglishMaliciousContentForwardEmailMatch() throws Exception
    {
        strippingTest(StrippedEnglishMaliciousContentForwardEmailFile, EnglishMaliciousContentForwardEmailFile);
    }

}
