package com.atlassian.pocketknife.internal.emailreply;

import com.atlassian.pocketknife.api.emailreply.EmailReplyCleaner;
import com.atlassian.pocketknife.internal.emailreply.matcher.outlook.OutlookAndroidMatcher;
import com.atlassian.pocketknife.internal.emailreply.matcher.outlook.OutlookWebMatcher;
import com.google.common.collect.Lists;
import org.junit.Test;

public class TestOutlookApplicationReplyCleaner {
    private final EmailReplyCleaner outlookCleaner = new DefaultEmailReplyCleaner(
            Lists.newArrayList(
                    new OutlookAndroidMatcher()
            )
    );

    @Test
    public void testOutlookAppReplySignature() throws Exception {
        EmailTestUtils.validateEmailStripping(outlookCleaner, EmailCorpus.OUTLOOK_APP_REPLY_SIGNATURE);
    }

    @Test
    public void testOutlookAppReplyNoSignature() throws Exception {
        EmailTestUtils.validateEmailStripping(outlookCleaner, EmailCorpus.OUTLOOK_APP_REPLY_NO_SIGNATURE);
    }
}
