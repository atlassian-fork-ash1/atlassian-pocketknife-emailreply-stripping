package com.atlassian.pocketknife.internal.emailreply.matcher.desktop.thunderbird.v31;

import com.atlassian.pocketknife.internal.emailreply.DefaultEmailReplyCleaner;
import com.atlassian.pocketknife.internal.emailreply.matcher.TestBase;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Test;

/**
 * <p> Test cases: <br/>
 * 1. Language based normal reply. <br/>
 * 2. Language based with long fullname reply. <br/>
 * 3. Language based with malicious content reply. <br/>
 * </p>
 */
public class TestReplyMatcherGermanMatching extends TestBase
{
    private final String corpusFolder = getDefaultCorpusFolder() + "/desktop/thunderbird/v31";

    private final String GermanReplyEmailFile = corpusFolder + "/GermanReplyEmail.eml";
    private final String StrippedGermanReplyEmailFile = corpusFolder + "/stripped/GermanReplyEmail.eml";

    private final String GermanLongFullnameReplyEmailFile = corpusFolder + "/GermanLongFullnameReplyEmail.eml";
    private final String StrippedGermanLongFullnameReplyEmailFile =
            corpusFolder + "/stripped/GermanLongFullnameReplyEmail.eml";

    private final String GermanMaliciousContentReplyEmailFile = corpusFolder + "/GermanMaliciousContentReplyEmail.eml";
    private final String StrippedGermanMaliciousContentReplyEmailFile =
            corpusFolder + "/stripped/GermanMaliciousContentReplyEmail.eml";

    @Before
    public void setUp() throws Exception
    {
        matcher = new ReplyMatcher();
        cleaner = new DefaultEmailReplyCleaner(Lists.newArrayList(matcher));
    }

    @Test
    public void testGermanReplyEmailMatch() throws Exception
    {
        strippingTest(StrippedGermanReplyEmailFile, GermanReplyEmailFile);
    }

    @Test
    public void testGermanLongFullnameReplyEmailMatch() throws Exception
    {
        strippingTest(StrippedGermanLongFullnameReplyEmailFile, GermanLongFullnameReplyEmailFile);
    }

    @Test
    public void testGermanMaliciousContentReplyEmailMatch() throws Exception
    {
        strippingTest(StrippedGermanMaliciousContentReplyEmailFile, GermanMaliciousContentReplyEmailFile);
    }
}
