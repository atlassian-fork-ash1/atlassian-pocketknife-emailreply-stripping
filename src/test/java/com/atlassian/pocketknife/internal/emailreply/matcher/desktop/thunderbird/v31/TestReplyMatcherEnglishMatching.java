package com.atlassian.pocketknife.internal.emailreply.matcher.desktop.thunderbird.v31;

import com.atlassian.pocketknife.internal.emailreply.DefaultEmailReplyCleaner;
import com.atlassian.pocketknife.internal.emailreply.matcher.TestBase;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Test;

/**
 * <p> Test cases: <br/>
 * 1. Language based normal reply. <br/>
 * 2. Language based with long fullname reply. <br/>
 * 3. Language based with malicious content reply. <br/>
 * </p>
 */
public class TestReplyMatcherEnglishMatching extends TestBase
{
    private final String corpusFolder = getDefaultCorpusFolder() + "/desktop/thunderbird/v31";

    private final String EnglishReplyEmailFile = corpusFolder + "/EnglishReplyEmail.eml";
    private final String StrippedEnglishReplyEmailFile = corpusFolder + "/stripped/EnglishReplyEmail.eml";

    private final String EnglishLongFullnameReplyEmailFile = corpusFolder + "/EnglishLongFullnameReplyEmail.eml";
    private final String StrippedEnglishLongFullnameReplyEmailFile =
            corpusFolder + "/stripped/EnglishLongFullnameReplyEmail.eml";

    private final String EnglishMaliciousContentReplyEmailFile =
            corpusFolder + "/EnglishMaliciousContentReplyEmail.eml";
    private final String StrippedEnglishMaliciousContentReplyEmailFile =
            corpusFolder + "/stripped/EnglishMaliciousContentReplyEmail.eml";

    @Before
    public void setUp() throws Exception
    {
        matcher = new ReplyMatcher();
        cleaner = new DefaultEmailReplyCleaner(Lists.newArrayList(matcher));
    }

    @Test
    public void testEnglishReplyEmailMatch() throws Exception
    {
        strippingTest(StrippedEnglishReplyEmailFile, EnglishReplyEmailFile);
    }

    @Test
    public void testEnglishLongFullnameReplyEmailMatch() throws Exception
    {
        strippingTest(StrippedEnglishLongFullnameReplyEmailFile, EnglishLongFullnameReplyEmailFile);
    }

    @Test
    public void testEnglishMaliciousContentReplyEmailMatch() throws Exception
    {
        strippingTest(StrippedEnglishMaliciousContentReplyEmailFile, EnglishMaliciousContentReplyEmailFile);
    }
}
