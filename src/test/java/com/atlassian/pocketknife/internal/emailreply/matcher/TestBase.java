package com.atlassian.pocketknife.internal.emailreply.matcher;

import com.atlassian.mail.MailUtils;
import com.atlassian.pocketknife.api.emailreply.EmailReplyCleaner;
import com.atlassian.pocketknife.internal.emailreply.DefaultEmailReplyCleaner;
import com.atlassian.pocketknife.internal.emailreply.matcher.util.RegexUtils;
import com.atlassian.pocketknife.spi.emailreply.matcher.QuotedEmailMatcher;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.mail.util.MimeMessageUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Properties;
import java.util.regex.Pattern;

import static org.apache.commons.lang.StringUtils.replaceEach;
import static org.apache.commons.lang.StringUtils.trimToEmpty;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class TestBase {
    Logger logger = LoggerFactory.getLogger(this.getClass());

    protected QuotedEmailMatcher matcher;
    protected DefaultEmailReplyCleaner cleaner;

    private final static String HTML_FOLDER = "./src/test/resources/com/atlassian/pocketknife/internal/emailreply/html";
    private final static String CORPUS_FOLDER = "./src/test/resources/com/atlassian/pocketknife/internal/emailreply/corpus";

    private static final String[] UNICODE_WHITESPACE = new String[]{"\u3000", "\u00A0", "\u2007", "\u202F"};
    private static final String[] UNICODE_WHITESPACE_REPLACE = new String[]{" ", " ", " ", " "};

    public static String getDefaultCorpusFolder() {
        return CORPUS_FOLDER;
    }

    public static String getDefaultHtmlFolder() {
        return HTML_FOLDER;
    }

    public static String normalize(final String text) {
        String expected = StringUtils.normalizeSpace(text);
        expected = replaceEach(expected, UNICODE_WHITESPACE, UNICODE_WHITESPACE_REPLACE).replaceAll("\\s+", " ");
        return trimToEmpty(expected);
    }

    protected Message createMIMEMessage(String emlFilePath) throws IOException, MessagingException {
        Session session = Session.getDefaultInstance(new Properties());

        return MimeMessageUtils.createMimeMessage(session, new File(emlFilePath));
    }

    protected String extractBodyString(Message message) throws MessagingException {
        return MailUtils.getBody(message);
    }

    protected void strippingTest(String resultFile, String rawFile, Charset resultFileCharset) throws
            IOException,
            MessagingException {
        String expectedStrippedBody = new String(Files.readAllBytes(Paths.get(resultFile)), resultFileCharset);
        EmailReplyCleaner.EmailCleanerResult strippedResult = cleaner.cleanQuotedEmail(createMIMEMessage(rawFile));

        logger.debug(String.format(
                "%s%n%s",
                "--------------originalBody--------------",
                strippedResult.getOriginalBody()));
        logger.debug(String.format(
                "%s%n%s",
                "--------------strippedBody--------------",
                strippedResult.getBody()));
        logger.debug(String.format(
                "%s%n%s",
                "--------------expectedStrippedBody--------------",
                expectedStrippedBody));
        logger.debug(
                String.format(
                        "%s%n%s",
                        "--------------messageBody--------------",
                        extractBodyString(createMIMEMessage(rawFile))));
        logger.debug("--------------End--------------");

        assertEquals(expectedStrippedBody, strippedResult.getBody());
        assertThat(strippedResult.getOriginalBody().startsWith(strippedResult.getBody()), is(true));
    }

    protected void strippingTest(String resultFile, String rawFile) throws IOException, MessagingException {
        strippingTest(resultFile, rawFile, StandardCharsets.UTF_8);
    }

    protected void patternMatchedTest(Pattern pattern, String text) {
        assertTrue(RegexUtils.match(pattern, text));
    }

    protected void patternUnMatchedTest(Pattern pattern, String text) {
        assertFalse(RegexUtils.match(pattern, text));
    }

}
