package com.atlassian.pocketknife.internal.emailreply.util;

import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;
import org.junit.Assert;
import org.junit.Test;

public class TestTextBlockUtil
{
    @Test
    public void testGetLineOrEmptyString()
    {
        Assert.assertEquals(StringUtils.EMPTY, TextBlockUtil.getLineOrEmptyString(null, 0));
        Assert.assertEquals(StringUtils.EMPTY, TextBlockUtil.getLineOrEmptyString(Lists.<String>newArrayList(), -1));
        Assert.assertEquals(StringUtils.EMPTY, TextBlockUtil.getLineOrEmptyString(Lists.<String>newArrayList(), 0));
        Assert.assertEquals(StringUtils.EMPTY, TextBlockUtil.getLineOrEmptyString(Lists.<String>newArrayList(), 1));
        Assert.assertEquals("two", TextBlockUtil.getLineOrEmptyString(Lists.newArrayList("one", "two"), 1));
    }

    @Test
    public void testGetFirst2Lines()
    {
        Assert.assertEquals(StringUtils.EMPTY, TextBlockUtil.getFirst2Lines(null));
        Assert.assertEquals(StringUtils.EMPTY, TextBlockUtil.getFirst2Lines(Lists.<String>newArrayList()));
        Assert.assertEquals("one", TextBlockUtil.getFirst2Lines(Lists.newArrayList("one")));
        Assert.assertEquals("one\ntwo", TextBlockUtil.getFirst2Lines(Lists.newArrayList("one", "two")));
        Assert.assertEquals("one\ntwo", TextBlockUtil.getFirst2Lines(Lists.newArrayList("one", "two", "three")));
    }
}
