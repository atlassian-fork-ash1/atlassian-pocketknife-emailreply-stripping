package com.atlassian.pocketknife.internal.emailreply.matcher.mobile.android.v5;

import com.atlassian.pocketknife.internal.emailreply.DefaultEmailReplyCleaner;
import com.atlassian.pocketknife.internal.emailreply.matcher.TestBase;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Test;

public class TestReplyMatcherPatternMatching extends TestBase
{
    @Before
    public void setUp() throws Exception
    {
        matcher = new ReplyMatcher();
        cleaner = new DefaultEmailReplyCleaner(Lists.newArrayList(matcher));
    }

    @Test
    public void testPatterns() throws Exception
    {
        String EnglishReplyAttributionLine = " On Jun 19, 2015 10:08 AM, \"Chuong Nam Nguyen\" <cnguyen@atlassian.com> wrote:";
        String FrenchReplyAttributionLine = " Le 19 juin 2015 11:29 AM, \"Chuong Nam Nguyen\" <cnguyen@atlassian.com> a écrit :";
        String GermanReplyAttributionLine = "Am 19.06.2015 11:35 AM schrieb \"Chuong Nam Nguyen\" <cnguyen@atlassian.com>:";
        String JapaneseReplyAttributionLine = " 2015/06/19 11:57 AM \"Chuong Nam Nguyen\" <cnguyen@atlassian.com>:";
        String SpanishReplyAttributionLine = " El 19/6/2015 2:09 AM, \"Chuong Nam Nguyen\" <cnguyen@atlassian.com> escribió:";
        String EnglishMilitaryTimeReplyAttributionLine = " On Jun 19, 2015 10:08, \"Chuong Nam Nguyen\" <cnguyen@atlassian.com> wrote:";
        String FrenchMilitaryTimeReplyAttributionLine = " Le 19 juin 2015 11:29, \"Chuong Nam Nguyen\" <cnguyen@atlassian.com> a écrit :";
        String GermanMilitaryTimeReplyAttributionLine = " Am 19.06.2015 11:35 vorm. schrieb \"Chuong Nam Nguyen\" <cnguyen@atlassian.com>:";
        String JapaneseMilitaryTimeReplyAttributionLine = " 2015/06/19 午前11:57 \"Chuong Nam Nguyen\" <cnguyen@atlassian.com>:";
        String SpanishMilitaryTimeReplyAttributionLine = " El 19/6/2015 2:09 p. m., \"Chuong Nam Nguyen\" <cnguyen@atlassian.com> escribió:";

        ReplyMatcher matcherInstance = (ReplyMatcher) matcher;

        patternMatchedTest(matcherInstance.ATTRIBUTION_LINE_ENGLISH_PATTERN, EnglishReplyAttributionLine);
        patternMatchedTest(matcherInstance.ATTRIBUTION_LINE_FRENCH_PATTERN, FrenchReplyAttributionLine);
        patternMatchedTest(matcherInstance.ATTRIBUTION_LINE_GERMAN_PATTERN, GermanReplyAttributionLine);
        patternMatchedTest(matcherInstance.ATTRIBUTION_LINE_JAPANESE_PATTERN, JapaneseReplyAttributionLine);
        patternMatchedTest(matcherInstance.ATTRIBUTION_LINE_SPANISH_PATTERN, SpanishReplyAttributionLine);
        patternMatchedTest(
                matcherInstance.ATTRIBUTION_LINE_ENGLISH_MILITARY_TIME_PATTERN,
                EnglishMilitaryTimeReplyAttributionLine);
        patternMatchedTest(
                matcherInstance.ATTRIBUTION_LINE_FRENCH_MILITARY_TIME_PATTERN, FrenchMilitaryTimeReplyAttributionLine);
        patternMatchedTest(
                matcherInstance.ATTRIBUTION_LINE_GERMAN_MILITARY_TIME_PATTERN, GermanMilitaryTimeReplyAttributionLine);
        patternMatchedTest(
                matcherInstance.ATTRIBUTION_LINE_JAPANESE_MILITARY_TIME_PATTERN,
                JapaneseMilitaryTimeReplyAttributionLine);
        patternMatchedTest(
                matcherInstance.ATTRIBUTION_LINE_SPANISH_MILITARY_TIME_PATTERN,
                SpanishMilitaryTimeReplyAttributionLine);
    }
}