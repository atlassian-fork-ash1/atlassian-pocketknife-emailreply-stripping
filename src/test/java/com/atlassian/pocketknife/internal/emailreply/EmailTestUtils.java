package com.atlassian.pocketknife.internal.emailreply;

import com.atlassian.pocketknife.api.emailreply.EmailReplyCleaner;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

public class EmailTestUtils {
    public static void validateEmailStripping(final EmailReplyCleaner cleaner, final EmailCorpus email) throws Exception {
        final String expectedStrippedBody = email.getExpectedContent();
        final EmailReplyCleaner.EmailCleanerResult stripped = cleaner.cleanQuotedEmail(email.toMimeMessage());

        assertThat("stripped result is not null", stripped, notNullValue());
        assertThat("Stripped body is incorrect", stripped.getRawBody(), equalTo(expectedStrippedBody));

        assertThat("Stripped body starts with the Original body", stripped.getOriginalBody().trim().startsWith(stripped.getRawBody()), is(true));
    }

}
