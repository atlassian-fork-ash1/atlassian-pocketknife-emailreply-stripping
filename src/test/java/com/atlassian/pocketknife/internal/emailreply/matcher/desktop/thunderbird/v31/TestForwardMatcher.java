package com.atlassian.pocketknife.internal.emailreply.matcher.desktop.thunderbird.v31;

import com.atlassian.pocketknife.internal.emailreply.DefaultEmailReplyCleaner;
import com.atlassian.pocketknife.internal.emailreply.EmailCorpus;
import com.atlassian.pocketknife.internal.emailreply.EmailTestUtils;
import com.atlassian.pocketknife.internal.emailreply.matcher.TestBase;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Test;

import java.nio.charset.Charset;

/**
 * <p> Test cases: <br/>
 * 1. Unit tests for each regex. <br/>
 * 2. Language based normal forward. <br/>
 * </p>
 */
public class TestForwardMatcher extends TestBase {
    private final String corpusFolder = getDefaultCorpusFolder() + "/desktop/thunderbird/v31";

    private final String EnglishForwardEmailFile = corpusFolder + "/EnglishForwardEmail.eml";
    private final String StrippedEnglishForwardEmailFile = corpusFolder + "/stripped/EnglishForwardEmail.eml";

    private final String GermanForwardEmailFile = corpusFolder + "/GermanForwardEmail.eml";
    private final String StrippedGermanForwardEmailFile = corpusFolder + "/stripped/GermanForwardEmail.eml";

    private final String SpanishForwardEmailFile = corpusFolder + "/SpanishForwardEmail.eml";
    private final String StrippedSpanishForwardEmailFile = corpusFolder + "/stripped/SpanishForwardEmail.eml";

    private final String FrenchForwardEmailFile = corpusFolder + "/FrenchForwardEmail.eml";
    private final String StrippedFrenchForwardEmailFile = corpusFolder + "/stripped/FrenchForwardEmail.eml";

    private final String JapaneseForwardEmailFile = corpusFolder + "/JapaneseForwardEmail.eml";
    private final String StrippedJapaneseForwardEmailFile = corpusFolder + "/stripped/JapaneseForwardEmail.eml";

    @Before
    public void setUp() throws Exception {
        matcher = new ForwardMatcher();
        cleaner = new DefaultEmailReplyCleaner(Lists.newArrayList(matcher));
    }

    @Test
    public void testPatterns() throws Exception {
        String EnglishLine = "-------- Forwarded Message --------";
        String FrenchLine = "-------- Message transfÃ©rÃ© --------";
        String GermanLine = "-------- Weitergeleitete Nachricht --------";
        String JapaneseLine = "-------- Forwarded Message --------";
        String SpanishLine = "-------- Mensaje reenviado --------";

        patternMatchedTest(((ForwardMatcher) matcher).ATTRIBUTION_LINE_ENGLISH_AND_JAPANESE_PATTERN, EnglishLine);
        patternMatchedTest(((ForwardMatcher) matcher).ATTRIBUTION_LINE_FRENCH_PATTERN, FrenchLine);
        patternMatchedTest(((ForwardMatcher) matcher).ATTRIBUTION_LINE_GERMAN_PATTERN, GermanLine);
        patternMatchedTest(((ForwardMatcher) matcher).ATTRIBUTION_LINE_ENGLISH_AND_JAPANESE_PATTERN, JapaneseLine);
        patternMatchedTest(((ForwardMatcher) matcher).ATTRIBUTION_LINE_SPANISH_PATTERN, SpanishLine);
    }

    @Test
    public void testEnglishForwardEmailMatch() throws Exception {
        strippingTest(StrippedEnglishForwardEmailFile, EnglishForwardEmailFile);
    }

    @Test
    public void testGermanForwardEmailMatch() throws Exception {
        strippingTest(StrippedGermanForwardEmailFile, GermanForwardEmailFile);
    }

    @Test
    public void testSpanishForwardEmailMatch() throws Exception {
        strippingTest(StrippedSpanishForwardEmailFile, SpanishForwardEmailFile);
    }

    @Test
    public void testFrenchForwardEmailMatch() throws Exception {
        strippingTest(StrippedFrenchForwardEmailFile, FrenchForwardEmailFile);
    }

    @Test
    public void testJapaneseForwardEmailMatch() throws Exception {
        strippingTest(StrippedJapaneseForwardEmailFile, JapaneseForwardEmailFile, Charset.forName("iso-2022-jp"));
    }

    @Test
    public void testThunderbirdForwardStylings() throws Exception {
        EmailTestUtils.validateEmailStripping(cleaner, EmailCorpus.THUNDERBIRD_FORWARD_STYLINGS);
    }
}