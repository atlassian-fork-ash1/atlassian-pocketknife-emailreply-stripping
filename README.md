atlassian-pocketknife-emailreplystripping was forked into the jira cloud monolith in commit: bfb801c4fd to jira cloud. This library is no longer in use.

# Summary
This library is used to detect quoted message in email replies and forwards. It supports popular email clients and multiple languages.

**Supported email clients**

* Desktop clients
    1. Thunderbird
    2. IBM Lotus Notes
    3. Apple Mail
    4. Microsoft Outlook
* Web clients
    1. Gmail.com
    2. Yahoo.com
    3. Outlook.com
* Mobile clients
    1. iPhone Mail
    2. iPad Mail
    3. Android Gmail

**Supported languages:** English, French, German, Spanish, Japanese

# Dependency
atlassian-mail (provided scope)

# How to use
The entry point to use the API is the class `com.atlassian.pocketknife.api.emailreply.EmailReplyCleanerBuilder`. Use this class to configure what email clients that you support. Refer to its Javadoc for more usage info
Example:

    final EmailReplyCleaner cleaner = new EmailReplyCleanerBuilder().build();
    final String cleanedBody = cleaner.cleanQuotedEmail(mimeMessage);
    System.out.println(cleanedBody);

# How it works
The main interface is `com.atlassian.pocketknife.api.emailreply.QuotedEmailCleaner`. Implementation of this interface will receive a MIME message, remove any quoted email from the body, and return the cleaned body as a string. 

The default implementation detects quoted email by incrementally scanning the body through blocks of 5 lines each. It then passes the block to a list of pattern matcher to decide if the block is the start of a quoted email. If it is, then scanning will stop and everything before that block will be returned as the cleaned body.

# How to extend
You can support new mail client and new language by implementing the interface `com.atlassian.pocketknife.api.emailreply.matcher.QuotedEmailMatcher`. 

Then to use your custom implementation, add it to your builder:

    final EmailReplyCleaner cleaner = new EmailReplyCleanerBuilder();
    cleaner.customMatchers(yourCustomMatchers);
    
# Bamboo plans
- CI: https://servicedesk-cloud-bamboo.internal.atlassian.com/browse/PKERS-PKERS
- Release: https://servicedesk-cloud-bamboo.internal.atlassian.com/browse/PKERS-PKERSRLS